// TODO remove
#![allow(deprecated)]

pub(crate) mod adversary;
pub(crate) mod assets;
pub mod backend;
pub(crate) mod cache_response;
pub(crate) mod character;
pub(crate) mod common;
pub mod config;
pub(crate) mod connection_handler;
pub(crate) mod convert_to_sql;
pub(crate) mod entity;
pub(crate) mod error;
pub(crate) mod error_fairing;
pub(crate) mod fellowship;
pub(crate) mod message;
pub(crate) mod name_generator;
pub(crate) mod name_or_id;
pub(crate) mod new_character;
pub(crate) mod random_features;
pub(crate) mod roll;
pub(crate) mod roll_statistics;
pub(crate) mod routes;
pub(crate) mod users;
pub(crate) mod utils;
pub(crate) mod version;

// android
#[cfg(target_os = "android")]
use tauri;

#[cfg(feature = "tauri")]
pub fn start_with_tauri() {
    eprintln!("Starting app");

    use config::CONFIG;
    use rocket::tokio::runtime::Runtime;
    use tauri::Manager;

    // Start rocket
    let rt = Runtime::new().expect("Failed to create new runtime");
    rt.spawn(async {
        let rocket = backend::get_rocket().await;
        if rocket.is_err() {
            eprintln!("Error while starting app: {rocket:?}");
            std::process::exit(1);
        }
        rocket.unwrap().launch().await.expect("Rocket panicked")
    });
    let mut config = rt.block_on(CONFIG.lock());

    // Start tauri
    tauri::Builder::default()
        .plugin(tauri_plugin_store::Builder::default().build())
        .plugin(tauri_plugin_http::init())
        .plugin(tauri_plugin_clipboard_manager::init())
        .setup(move |app| {
            let config_dir = app.path().document_dir().expect("Failed to get config dir");

            config.configure(&config_dir);
            drop(config);

            Ok(())
        })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

#[cfg(target_os = "android")]
#[tauri::mobile_entry_point]
fn android_start() {
    start_with_tauri();
}
