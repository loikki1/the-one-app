use anyhow::{anyhow, bail, Error, Result};
use rocket::State;
use rocket::{http::Status, outcome::Outcome, request, request::FromRequest, request::Request};
use sea_orm::{
    ActiveModelTrait as _, ColumnTrait as _, DatabaseConnection, EntityTrait as _,
    QueryFilter as _, Set,
};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

use crate::entity::user as entity;
use crate::utils::convert_option_to_set;

const NO_USERNAME: &str = "";

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct User {
    pub id: Option<i32>,
    pub username: String,
    pub password: u64,
}

#[derive(Serialize, Deserialize, Debug, Clone, TS)]
#[ts(export)]
pub struct UserInClear {
    pub username: String,
    pub password: String,
}

impl User {
    #[cfg(not(feature = "cloud"))]
    pub async fn create_empty_user(db: &DatabaseConnection) -> Result<()> {
        if Self::get_user(db, "").await.is_err() {
            let user = Self {
                id: None,
                username: NO_USERNAME.to_owned(),
                password: 0,
            };
            let user: entity::ActiveModel = user.into();
            user.save(db).await?;
        };
        Ok(())
    }

    pub const fn get_username(&self) -> &String {
        &self.username
    }

    pub fn hash(password: &str) -> u64 {
        use std::hash::{DefaultHasher, Hash as _, Hasher as _};
        let mut hash = DefaultHasher::new();
        password.hash(&mut hash);
        hash.finish()
    }

    #[deprecated(since = "3.0.0", note = "Should be in the DB")]
    pub async fn import_user(&self, db: &DatabaseConnection) -> Result<()> {
        let user: entity::ActiveModel = self.clone().into();
        user.insert(db).await?;
        Ok(())
    }

    pub async fn create_user(
        db: &DatabaseConnection,
        username: &str,
        password: &str,
    ) -> Result<()> {
        if cfg!(not(feature = "cloud")) {
            return Ok(());
        }

        if username.is_empty() || password.is_empty() {
            bail!("Cannot provide an empty username or password");
        }

        let user = User {
            id: None,
            username: username.to_owned(),
            password: Self::hash(password),
        };

        let user: entity::ActiveModel = user.into();

        user.insert(db).await?;

        Ok(())
    }

    pub async fn get_user(db: &DatabaseConnection, username: &str) -> Result<Self> {
        let user = entity::Entity::find()
            .filter(entity::Column::Name.eq(username))
            .one(db)
            .await?;
        if let Some(user) = user {
            Ok(user.into())
        } else {
            bail!("Username does not exist in the DB")
        }
    }

    pub async fn check_auth(db: &DatabaseConnection, test_user: UserInClear) -> Result<User> {
        if cfg!(not(feature = "cloud")) {
            return Self::get_user(db, NO_USERNAME).await;
        }

        // Hash password
        let password = Self::hash(&test_user.password);
        let user_from_db = Self::get_user(db, &test_user.username).await?;
        if password == user_from_db.password {
            Ok(user_from_db)
        } else {
            bail!("Authentication failed")
        }
    }

    pub async fn get_user_from_id(db: &DatabaseConnection, id: i32) -> Result<Self> {
        let user = entity::Entity::find_by_id(id).one(db).await?;
        if let Some(user) = user {
            Ok(user.into())
        } else {
            bail!("User id does not exist in the DB")
        }
    }

    async fn check_request(db: &DatabaseConnection, req: &Request<'_>) -> Result<Self> {
        if cfg!(not(feature = "cloud")) {
            return Self::get_user(db, NO_USERNAME).await;
        }
        // Get the headers
        let headers = req.headers();

        // Username
        let username = headers
            .get_one("username")
            .ok_or(anyhow!("Missing username"))?;

        // Password
        let password = headers
            .get_one("password")
            .ok_or(anyhow!("Missing password"))?;

        Self::check_auth(
            db,
            UserInClear {
                username: username.into(),
                password: password.into(),
            },
        )
        .await
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for User {
    type Error = Error;

    async fn from_request(request: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        if let Outcome::Success(db) = request.guard::<&State<DatabaseConnection>>().await {
            match Self::check_request(db, request).await {
                Ok(x) => Outcome::Success(x),
                Err(x) => Outcome::Error((Status::Unauthorized, x)),
            }
        } else {
            Outcome::Error((Status::Unauthorized, anyhow!("Failed to reach DB")))
        }
    }
}

impl From<entity::Model> for User {
    fn from(value: entity::Model) -> Self {
        Self {
            id: Some(value.id),
            username: value.name,
            #[expect(clippy::cast_sign_loss)]
            password: value.hashed_password as u64,
        }
    }
}

impl From<User> for entity::ActiveModel {
    fn from(value: User) -> Self {
        let id = convert_option_to_set(value.id);
        Self {
            id,
            name: Set(value.username),
            hashed_password: Set(value.password as i64),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::backend::prepare_db;

    use super::{User, UserInClear};
    use rocket::tokio;

    #[tokio::test]
    async fn check_auth() {
        let db = prepare_db("sqlite::memory:").await.expect("Can prepare db");

        // Create user
        User::create_user(&db, "me", "1234")
            .await
            .expect("Can create character");

        // Check if auth works
        let success = UserInClear {
            username: String::from("me"),
            password: String::from("1234"),
        };

        User::check_auth(&db, success).await.expect("Auth works");

        // Check if it fails (only cloud version)
        let failure = UserInClear {
            username: String::from("me"),
            password: String::from("12345"),
        };
        let err = User::check_auth(&db, failure).await;
        if cfg!(feature = "cloud") {
            assert!(err.is_err());
        } else {
            err.unwrap();
        }
    }
}
