use std::{
    ffi::OsStr,
    fs::{read_dir, rename, File},
    path::Path,
};

use anyhow::Result;
use sea_orm::DatabaseConnection;

use crate::{adversary::Adversary, character::Character, users::User};

pub async fn run(db: &DatabaseConnection, directory: &Path) -> Result<()> {
    if !directory.is_dir() {
        return Ok(());
    }
    if cfg!(feature = "cloud") {
        let file = File::open(directory.join("users.json"))?;
        let users: Vec<User> = serde_json::from_reader(file)?;
        for user in &users {
            let username = user.username.clone();

            if User::get_user(db, &username).await.is_err() {
                user.import_user(db).await?;
            }

            let user_dir = directory.join(username.clone());
            if user_dir.is_dir() {
                eprintln!("Processing {user_dir:?}");
                process_data_from_user(db, &user_dir, &username).await?;
            }
        }
        Ok(())
    } else {
        process_data_from_user(db, directory, "").await
    }
}

async fn process_data_from_user(
    db: &DatabaseConnection,
    directory: &Path,
    username: &str,
) -> Result<()> {
    let user = User::get_user(db, username).await?;
    read_characters(db, directory, &user).await?;
    read_adversaries(db, directory, &user).await?;
    Ok(())
}

async fn read_characters(db: &DatabaseConnection, directory: &Path, user: &User) -> Result<()> {
    let char_dir = directory.join("Characters");
    if !char_dir.is_dir() {
        return Ok(());
    }

    for entry in read_dir(char_dir)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() && path.extension() == Some(OsStr::new("json")) {
            eprintln!("Reading {path:?}");
            if let Ok(mut character) = Character::load(&path) {
                character.reset_ids();
                eprintln!("Char {character:?}");

                character.save_to_db(db, user).await?;

                let mut new_path = path.clone();
                new_path.set_extension("old");
                rename(path, new_path)?;
            } else {
                eprintln!("Failed to convert {path:?}");
            }
        }
    }

    Ok(())
}

async fn read_adversaries(db: &DatabaseConnection, directory: &Path, user: &User) -> Result<()> {
    let adv_dir = directory.join("Adversaries");
    if !adv_dir.is_dir() {
        return Ok(());
    }

    for entry in read_dir(adv_dir)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() && path.extension() == Some(OsStr::new("json")) {
            eprintln!("Reading {path:?}");
            let adv = Adversary::load(&path)?;
            adv.save_to_db(db, user).await?;
            eprintln!("Adv {adv:?}");

            let mut new_path = path.clone();
            new_path.set_extension("old");
            rename(path, new_path)?;
        }
    }

    Ok(())
}
