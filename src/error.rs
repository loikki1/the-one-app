// This is a copy from rocket_anyhow (copied to avoid any issue with rocket releases)
//! This library provided [`rocket_anyhow::Error`][Error],
//! a wrapper around [`anyhow::Error`]
//! with rocket's [responder] implemented.
//!
//! [anyhow::Error]: https://docs.rs/anyhow/1.0/anyhow/struct.Error.html
//! [responder]: https://api.rocket.rs/v0.4/rocket/response/trait.Responder.html

use core::fmt;

use rocket::http::Status;
use rocket::response::{self, Responder};
use rocket::Request;
use std::fmt::Formatter;
use std::fmt::Result as FmtResult;
use std::result::Result as StdResult;

pub type Result<T = ()> = StdResult<T, RocketError>;

/// Wrapper around [`anyhow::Error`]
/// with rocket's [responder] implemented
///
/// [anyhow::Error]: https://docs.rs/anyhow/1.0/anyhow/struct.Error.html
/// [responder]: https://api.rocket.rs/v0.4/rocket/response/trait.Responder.html
/// Error that can be convert into `anyhow::Error` can be convert directly to this type.
///
/// Responder part are internally delegated to [rocket::response::Debug] which
/// "debug prints the internal value before responding with a 500 error"
///
/// [rocket::response::Debug]: https://api.rocket.rs/v0.4/rocket/response/struct.Debug.html
#[derive(Debug)]
pub struct RocketError(pub anyhow::Error);

impl<E> From<E> for RocketError
where
    E: Into<anyhow::Error>,
{
    fn from(error: E) -> Self {
        RocketError(error.into())
    }
}

impl<'r> Responder<'r, 'static> for RocketError {
    fn respond_to(self, request: &'r Request<'_>) -> response::Result<'static> {
        let body = format!("{:?}", self.0);
        response::Response::build_from(body.respond_to(request)?)
            .status(Status::InternalServerError)
            .ok()
    }
}

impl fmt::Display for RocketError {
    #[expect(clippy::min_ident_chars)]
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{self:?}")
    }
}
