use crate::cache_response::CacheResponse;
use crate::character::combat_skills::CombatSkills;
use crate::character::Character;
use crate::error::Result;
use crate::new_character::previous_experience::PreviousExperience;
use crate::new_character::{calling::Calling, choices::Choices, culture::Culture};
use crate::users::User;
use enums::calling::CallingEnum;
use enums::combat_skill::CombatSkillEnum;
use enums::culture::CultureEnum;
use enums::skill::SkillEnum;
use rocket::serde::json::Json;
use rocket::{get, post, put, routes, Route, State};
use sea_orm::DatabaseConnection;
use strum::EnumCount as _;

pub fn get_routes() -> Vec<Route> {
    routes![
        get_empty_character,
        create_character,
        get_culture,
        get_calling,
        get_can_upgrade_skill,
        get_can_upgrade_combat_skill,
        get_skill_cost,
        get_combat_skill_cost,
        get_combat_skills,
    ]
}

#[get("/empty")]
pub fn get_empty_character() -> CacheResponse<Choices> {
    CacheResponse::new(Choices::new())
}

#[put("/create", data = "<choices>")]
pub async fn create_character(
    db: &State<DatabaseConnection>,
    user: User,
    choices: Json<Choices>,
) -> Result<Json<Character>> {
    let mut character = Character::new(&choices, user.get_username().to_string())?;
    let id = character.save_to_db(db, &user).await?;
    character.set_id(id);
    Ok(Json(character))
}

#[get("/culture/<culture>")]
pub fn get_culture(culture: CultureEnum) -> CacheResponse<Culture> {
    CacheResponse::new(Culture::new(culture))
}

#[get("/calling/<calling>")]
pub fn get_calling(calling: CallingEnum) -> CacheResponse<Calling> {
    CacheResponse::new(Calling::new(calling))
}

#[post("/skills/can-upgrade/<previous_experience>", data = "<skills>")]
pub fn get_can_upgrade_skill(
    previous_experience: u32,
    skills: Json<[u32; SkillEnum::COUNT]>,
) -> Json<Vec<SkillEnum>> {
    let skills =
        PreviousExperience::get_can_upgrade_skills(previous_experience, &skills.into_inner());
    Json(skills)
}

#[post("/combat-skills/can-upgrade/<previous_experience>", data = "<skills>")]
pub fn get_can_upgrade_combat_skill(
    previous_experience: u32,
    skills: Json<[u32; CombatSkillEnum::COUNT]>,
) -> Json<Vec<CombatSkillEnum>> {
    let skills = PreviousExperience::get_can_upgrade_combat_skills(
        previous_experience,
        &skills.into_inner(),
    );
    Json(skills)
}

#[get("/skill-cost/<new_value>")]
pub fn get_skill_cost(new_value: u32) -> CacheResponse<u32> {
    let cost = PreviousExperience::get_skill_cost(new_value);
    CacheResponse::new(cost)
}

#[get("/combat-skill-cost/<new_value>")]
pub fn get_combat_skill_cost(new_value: u32) -> CacheResponse<u32> {
    let cost = PreviousExperience::get_combat_skill_cost(new_value);
    CacheResponse::new(cost)
}

#[post("/combat-skills", data = "<choices>")]
pub fn get_combat_skills(choices: Json<Choices>) -> Result<Json<CombatSkills>> {
    let skills = CombatSkills::new(&choices, false)?;
    Ok(Json(skills))
}
