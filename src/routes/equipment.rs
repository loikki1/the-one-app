use rocket::serde::json::Json;
use rocket::{delete, put, routes, Route, State};
use sea_orm::DatabaseConnection;

use crate::character::equipment::EquipmentType;
use crate::character::Character;
use crate::character::{armor::Armor, equipment::Equipment, shield::Shield, weapon::Weapon};
use crate::error::Result;
use crate::users::User;

pub fn get_routes() -> Vec<Route> {
    routes![
        add_shield,
        delete_item,
        add_weapon,
        add_armor,
        add_equipment,
        update_wearing
    ]
}

#[put("/<character_id>/Shield", data = "<shield>")]
pub async fn add_shield(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    shield: Json<Shield>,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.items.add_shield(&shield.into_inner())?;
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[put("/<character_id>/Weapon", data = "<weapon>")]
pub async fn add_weapon(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    weapon: Json<Weapon>,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.items.add_weapon(&weapon.into_inner())?;
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[put("/<character_id>/Armor", data = "<armor>")]
pub async fn add_armor(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    armor: Json<Armor>,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.items.add_armor(&armor.into_inner())?;
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[delete("/<character_id>/<eq_type>/<id>")]
pub async fn delete_item(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    eq_type: EquipmentType,
    id: i32,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.items.delete_item(eq_type, id)?;
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[put("/<character_id>/Equipment", data = "<equipment>")]
pub async fn add_equipment(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    equipment: Json<Equipment>,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.items.add_equipment(&equipment.into_inner())?;
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[put("/<character_id>/<eq_type>/wearing/<id>/<value>")]
pub async fn update_wearing(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    eq_type: EquipmentType,
    id: i32,
    value: bool,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.items.update_wearing(eq_type, id, value)?;
    character.save_to_db(db, &user).await?;
    Ok(())
}
