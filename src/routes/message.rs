use crate::connection_handler::ConnectionHandler;
use crate::error::Result;
use anyhow::anyhow;
use rocket::{get, post, routes, serde::json::Json, Route, State};
use sea_orm::DatabaseConnection;

use crate::message::Message;

use super::socket::Event;
use super::LoremasterOrPlayer;

pub fn get_routes() -> Vec<Route> {
    routes![send, get]
}

#[post("/<fellowship_id>", data = "<message>")]
pub async fn send(
    db: &State<DatabaseConnection>,
    user: LoremasterOrPlayer,
    fellowship_id: i32,
    message: Json<Message>,
) -> Result<()> {
    let loremaster = user.get_loremaster();
    let message = message.into_inner();

    message.save_to_db(db, &loremaster, fellowship_id).await?;

    // Update player
    let player_to_update = if message.get_to_loremaster() {
        loremaster.get_username()
    } else {
        message.get_player()
    };
    ConnectionHandler::update_single(
        loremaster.username.clone(),
        player_to_update,
        Event::UpdateMessage,
    )
    .await;
    Ok(())
}

#[get("/<fellowship_id>?<player>")]
pub async fn get(
    db: &State<DatabaseConnection>,
    user: LoremasterOrPlayer,
    fellowship_id: i32,
    player: Option<&str>,
) -> Result<Json<Vec<Message>>> {
    if let LoremasterOrPlayer::Player(_) = user {
        if player.is_none() {
            Err(anyhow!("Only the loremaster can access all the messages"))?;
        }
    }
    let loremaster = user.get_loremaster();
    let messages = Message::get(db, &loremaster, fellowship_id, player).await?;

    Ok(Json(messages))
}
