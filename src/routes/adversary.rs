use rocket::serde::json::Json;
use rocket::{delete, get, post, put, routes, Route, State};
use sea_orm::DatabaseConnection;

use crate::adversary::Adversary;
use crate::cache_response::CacheResponse;
use crate::connection_handler::ConnectionHandler;
use crate::error::Result;
use crate::name_or_id::NameOrId;
use crate::users::User;

use super::socket::Event;

pub fn get_routes() -> Vec<Route> {
    routes![
        get_list_adversary,
        get_adversary,
        delete_adversary,
        save_adversary,
        get_empty_adversary,
        loose_endurance,
        loose_hate,
        loose_resolve
    ]
}

#[get("/")]
pub async fn get_list_adversary(
    db: &State<DatabaseConnection>,
    user: User,
) -> Result<Json<Vec<Adversary>>> {
    let mut advs = Adversary::get_list_from_enums();
    advs.append(&mut Adversary::get_list_from_db(db, user).await?);
    // TODO cache enum advs
    Ok(Json(advs))
}

#[get("/empty")]
pub fn get_empty_adversary() -> CacheResponse<Adversary> {
    CacheResponse::new(Adversary::default())
}

#[get("/<name_or_id>")]
pub async fn get_adversary(
    db: &State<DatabaseConnection>,
    user: User,
    name_or_id: NameOrId,
) -> Result<Json<Adversary>> {
    let adversary = Adversary::get(db, &user, name_or_id).await?;
    Ok(Json(adversary))
}

#[delete("/<id>")]
pub async fn delete_adversary(db: &State<DatabaseConnection>, user: User, id: i32) -> Result<()> {
    Adversary::delete_from_db(db.inner(), user, id).await?;
    Ok(())
}

#[put("/", data = "<adversary>")]
pub async fn save_adversary(
    db: &State<DatabaseConnection>,
    user: User,
    adversary: Json<Adversary>,
) -> Result<Json<i32>> {
    let id = adversary.save_to_db(db.inner(), &user).await?;
    Ok(Json(id))
}

// TODO merge all loose together (same for character)
#[post("/<adversary_id>/loose-Endurance/<value>")]
pub async fn loose_endurance(
    db: &State<DatabaseConnection>,
    user: User,
    adversary_id: i32,
    value: i32,
) -> Result<()> {
    let adversary_id = NameOrId::Id(adversary_id);
    let mut adversary = Adversary::get(db, &user, adversary_id).await?;

    adversary.loose_endurance(value);

    adversary.save_to_db(db, &user).await?;

    // Update players
    ConnectionHandler::update_everyone(user.username.clone(), Event::UpdateFellowship).await;
    Ok(())
}

#[post("/<adversary_id>/loose-Hate/<value>")]
pub async fn loose_hate(
    db: &State<DatabaseConnection>,
    user: User,
    adversary_id: i32,
    value: i32,
) -> Result<()> {
    let adversary_id = NameOrId::Id(adversary_id);
    let mut adversary = Adversary::get(db, &user, adversary_id).await?;

    adversary.loose_hate(value);

    adversary.save_to_db(db, &user).await?;

    // Update players
    ConnectionHandler::update_everyone(user.username.clone(), Event::UpdateFellowship).await;
    Ok(())
}

#[post("/<adversary_id>/loose-Resolve/<value>")]
pub async fn loose_resolve(
    db: &State<DatabaseConnection>,
    user: User,
    adversary_id: i32,
    value: i32,
) -> Result<()> {
    loose_hate(db, user, adversary_id, value).await
}
