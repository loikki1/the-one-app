use crate::cache_response::CacheResponse;
use crate::error::Result;
use crate::users::User;
use crate::version::Version;
use rocket::http::Status;
use rocket::serde::json::Json;
use rocket::{get, post, routes, Route, State};
use sea_orm::DatabaseConnection;

pub fn get_routes() -> Vec<Route> {
    routes![
        health,
        requires_authentication,
        check_auth,
        create_user,
        get_version,
    ]
}

#[get("/health")]
pub fn health() -> Status {
    Status::Ok
}

#[get("/requires-authentication")]
pub fn requires_authentication() -> CacheResponse<&'static str> {
    CacheResponse::new(if cfg!(feature = "cloud") {
        "true"
    } else {
        "false"
    })
}

#[get("/check-auth")]
pub fn check_auth(_user: User) -> &'static str {
    "Ok"
}

#[post("/create-user/<user>", data = "<password>")]
pub async fn create_user(db: &State<DatabaseConnection>, user: &str, password: &str) -> Result<()> {
    User::create_user(db.inner(), user, password).await?;
    Ok(())
}

#[get("/version")]
pub fn get_version() -> Json<Version> {
    Json(Version::new())
}
