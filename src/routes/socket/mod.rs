pub mod fellowship;

use async_stream::stream;
use futures::FutureExt as _;
use futures::Stream;
use rocket::tokio::time::sleep;
use std::fmt;
use std::fmt::Debug;
use std::fmt::Display;
use std::time::Duration;
use ts_rs::TS;

use rocket::futures::{SinkExt as _, StreamExt as _};
use rocket::{routes, warn, Route};
use rocket_ws::result::Error::{self, ConnectionClosed};
use serde::{Deserialize, Serialize};

use crate::character::Character;
use crate::connection_handler::SafeStream;
use crate::version::Version;

pub fn get_routes() -> Vec<Route> {
    routes![fellowship::socket]
}

#[derive(Serialize, Deserialize, TS)]
#[ts(export)]
struct Message {
    from_user: String,
    to_user: String,
    text: String,
}

#[derive(Serialize, Deserialize, TS)]
#[ts(export)]
pub enum Event {
    UpdateMessage,
    UpdateFellowship,
    Version(Version),
    Character(Box<Character>),
}

// TODO cleanup with macro
impl Display for Event {
    #[expect(clippy::min_ident_chars)]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            Self::UpdateMessage => "UpdateMessage",
            Self::UpdateFellowship => "UpdateFellowship",
            Self::Version(_) => "Version",
            Self::Character(_) => "Character",
        };
        write!(f, "{text}")
    }
}

fn non_blocking_stream(
    stream: SafeStream,
) -> impl Stream<Item = Result<rocket_ws::Message, Error>> {
    stream! {
        loop {
            {
                let result = stream.lock().await.next().now_or_never();
                if let Some(x) = result {
                    yield x.expect("Failed to yield stream");
                }
            }
            sleep(Duration::from_millis(20)).await;
        }
    }
}

async fn close_connection<T: Debug>(stream: SafeStream, error: T) -> Error {
    let error = format!("{error:?}");
    let err = stream.lock().await.send(error.into()).await;
    if err.is_err() {
        warn!("Failed to close connection {err:?}");
    }
    ConnectionClosed
}
