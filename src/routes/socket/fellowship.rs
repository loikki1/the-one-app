use std::sync::Arc;

use anyhow::anyhow;
use futures::{pin_mut, StreamExt as _};
use rocket::info;
use rocket::tokio::sync::Mutex;
use rocket::{debug, error, get, warn, State};
use rocket_ws::{Channel, Message, WebSocket};
use sea_orm::DatabaseConnection;
use serde::Deserialize;

use crate::connection_handler::{Connection, ConnectionHandler, SafeStream, CONNECTIONS};
use crate::error::Result;
use crate::fellowship::Fellowship;
use crate::routes::socket::Event;
use crate::users::{User, UserInClear};
use crate::version::Version;

use super::{close_connection, non_blocking_stream};

#[derive(Clone)]
struct ConnectionData {
    loremaster: String,
    player: Option<String>,
}

impl ConnectionData {
    pub fn get_key(&self) -> &str {
        &self.loremaster
    }
    pub fn get_name(&self) -> String {
        self.clone().player.unwrap_or(self.clone().loremaster)
    }
}

#[derive(Deserialize, ts_rs::TS)]
#[ts(export)]
pub enum Auth {
    Loremaster(UserInClear),
    Player(Connection),
}

#[get("/fellowship")]
pub fn socket(db: &State<DatabaseConnection>, ws: WebSocket) -> Channel<'static> {
    let db = db.inner().clone();
    ws.channel(move |stream| {
        Box::pin(async move {
            // Authenticate the request
            let stream = Arc::new(Mutex::new(stream));
            let conn = match process_handshake(&db, stream.clone()).await {
                Err(x) => {
                    warn!("{:}", x);
                    return Err(close_connection(stream.clone(), x).await);
                }
                Ok(x) => x,
            };

            info!("Connection is setup");
            let iter = non_blocking_stream(stream.clone());
            pin_mut!(iter);
            while let Some(message) = iter.next().await {
                debug!("Loremaster - {:?}", message);
                let message = message?;

                match message {
                    // Handle connection closed
                    Message::Close(_) => {
                        {
                            let mut handler = CONNECTIONS.lock().await;
                            match handler.get_mut(conn.get_key()) {
                                None => panic!("No frontend available when disconnecting"),
                                Some(x) => {
                                    let player = conn.get_name();
                                    x.remove(&player).await;
                                }
                            };
                        };
                        ConnectionHandler::cleanup().await;
                    }
                    Message::Text(x) => {
                        if let Err(x) = handle_message(&db, &conn, x).await {
                            error!("Failed to process socket message {x}");
                        };
                    }

                    // Normal handling
                    x => {
                        eprintln!("{x:?}");
                    }
                }
            }
            Ok(())
        })
    })
}

async fn handle_message(
    db: &DatabaseConnection,
    conn: &ConnectionData,
    message: String,
) -> Result<()> {
    let message: Event = serde_json::from_str(&message)?;

    info!(
        "Request {} between {:?} and {:?}",
        message, conn.loremaster, conn.player
    );
    match message {
        Event::UpdateFellowship => {
            ConnectionHandler::update_everyone(conn.loremaster.clone(), Event::UpdateFellowship)
                .await;
        }
        Event::Character(mut x) => {
            let loremaster = User::get_user(db, &conn.loremaster).await?;
            x.find_and_convert_to_external(db, &loremaster).await?;
            x.save_to_db(db, &loremaster).await?;
        }
        _ => Err(anyhow!("Message not implemented"))?,
    }
    Ok(())
}

async fn check_auth(db: &DatabaseConnection, auth: &Auth) -> Result<ConnectionData> {
    match auth {
        Auth::Loremaster(user) => {
            User::check_auth(db, user.clone()).await?;

            Ok(ConnectionData {
                loremaster: user.username.clone(),
                player: None,
            })
        }
        Auth::Player(conn) => {
            info!(
                "User {} connecting to {} from {}",
                conn.player, conn.fellowship, conn.loremaster
            );

            // Check if fellowship exists
            Fellowship::get_fellowship_and_loremaster_from_names(
                db,
                &conn.fellowship,
                &conn.loremaster,
            )
            .await?;

            Ok(ConnectionData {
                loremaster: conn.loremaster.clone(),
                player: Some(conn.player.clone()),
            })
        }
    }
}

async fn process_handshake(db: &DatabaseConnection, stream: SafeStream) -> Result<ConnectionData> {
    // Get the version
    let message = stream
        .lock()
        .await
        .next()
        .await
        .ok_or(anyhow!("No next stream"))??;

    // Check the major version
    let version: Version = match message {
        Message::Text(x) => serde_json::from_str(&x)?,
        _ => return Err(anyhow!("Type not implemented").into()),
    };
    let local = Version::new();
    if version.major != local.major {
        Err(anyhow!("Wrong version"))?;
    }

    let message = stream
        .lock()
        .await
        .next()
        .await
        .ok_or(anyhow!("No next stream"))??;
    let auth: Auth = match message {
        Message::Text(x) => serde_json::from_str(&x)?,
        _ => return Err(anyhow!("Type not implemented").into()),
    };

    let conn = check_auth(db, &auth).await?;

    // Manage connection handler
    let mut handler = CONNECTIONS.lock().await;
    let key: String = conn.get_key().into();
    let name: String = conn.get_name();
    match handler.get_mut(&key) {
        None => {
            info!("Creating a new connection {key}");
            handler.insert(key, ConnectionHandler::new(name, stream));
        }
        Some(x) => {
            x.add(name, stream);
            // Check the minor version
            if version.minor != local.minor {
                x.send_to_single_player(
                    &conn.get_name(),
                    "Minor versions mismatch, risk of errors",
                )
                .await?;
            }
        }
    }

    Ok(conn)
}
