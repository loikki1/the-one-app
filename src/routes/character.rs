use enums::combat_skill::CombatSkillEnum;
use enums::skill::SkillEnum;
use rocket::serde::json::Json;
use rocket::{delete, get, post, put, routes, Route, State};
use sea_orm::DatabaseConnection;

use crate::character::reward::Reward;
use crate::character::roll::CharacterRoll;
use crate::character::skill_type::SkillType;
use crate::character::virtue::Virtue;
use crate::character::{secondary_attributes::SecondaryAttributeEnum, Character};
use crate::error::Result;
use crate::roll::RollType;
use crate::users::User;

pub fn get_routes() -> Vec<Route> {
    routes![
        get_character,
        delete_character,
        get_list_character,
        set_character,
        loose_endurance,
        loose_hope,
        skills_can_upgrade,
        combat_skills_can_upgrade,
        secondary_attributes_can_upgrade,
        increase_skill,
        increase_combat_skill,
        increase_wisdom,
        increase_valor,
        update_notes,
        get_notes,
        add_virtue,
        add_reward,
        long_rest,
        short_rest,
        take_fellowship_phase,
        reward_xp,
        convert_shadow,
        roll_skill,
    ]
}

#[get("/<character_id>")]
pub async fn get_character(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
) -> Result<Json<Character>> {
    let character = Character::get(db, &user, character_id).await?;
    Ok(Json(character))
}

#[delete("/<character_id>")]
pub async fn delete_character(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
) -> Result<()> {
    Character::delete_from_db(db, character_id, &user).await?;
    Ok(())
}

#[get("/")]
pub async fn get_list_character(
    db: &State<DatabaseConnection>,
    user: User,
) -> Result<Json<Vec<i32>>> {
    let out = Character::get_list_characters(db, &user).await?;
    Ok(Json(out))
}

#[put("/", data = "<character>")]
pub async fn set_character(
    db: &State<DatabaseConnection>,
    user: User,
    character: Json<Character>,
) -> Result<Json<i32>> {
    let id = character.into_inner().save_to_db(db, &user).await?;
    Ok(Json(id))
}

#[post("/<character_id>/loose-endurance/<value>")]
pub async fn loose_endurance(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    value: i32,
) -> Result<Json<u32>> {
    let mut character = Character::get(db, &user, character_id).await?;

    character.characteristics.loose_endurance(value);

    character.save_to_db(db, &user).await?;

    Ok(Json(
        character.characteristics.attributes.endurance.get_current(),
    ))
}

#[post("/<character_id>/loose-hope/<value>")]
pub async fn loose_hope(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    value: i32,
) -> Result<Json<u32>> {
    let mut character = Character::get(db, &user, character_id).await?;

    character.characteristics.loose_hope(value);

    character.save_to_db(db, &user).await?;

    Ok(Json(
        character.characteristics.attributes.hope.get_current(),
    ))
}

#[get("/<character_id>/skills/can-upgrade")]
pub async fn skills_can_upgrade(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
) -> Result<Json<Vec<SkillEnum>>> {
    let character = Character::get(db, &user, character_id).await?;
    Ok(Json(character.characteristics.can_upgrade_skills()))
}

#[get("/<character_id>/combat-skills/can-upgrade")]
pub async fn combat_skills_can_upgrade(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
) -> Result<Json<Vec<CombatSkillEnum>>> {
    let character = Character::get(db, &user, character_id).await?;
    Ok(Json(character.characteristics.can_upgrade_combat_skills()))
}

#[get("/<character_id>/secondary-attributes/can-upgrade")]
pub async fn secondary_attributes_can_upgrade(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
) -> Result<Json<Vec<SecondaryAttributeEnum>>> {
    let character = Character::get(db, &user, character_id).await?;
    Ok(Json(
        character.characteristics.can_upgrade_secondary_attributes(),
    ))
}

#[post("/<character_id>/increase-skill/<skill>/<delta>?<free>")]
pub async fn increase_skill(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    skill: SkillEnum,
    delta: i32,
    free: Option<bool>,
) -> Result<Json<u32>> {
    let mut character = Character::get(db, &user, character_id).await?;
    let new_value =
        character
            .characteristics
            .increase_skill(&skill, delta, free.unwrap_or(false))?;
    character.save_to_db(db, &user).await?;
    Ok(Json(new_value))
}

#[post("/<character_id>/increase-combat-skill/<skill>/<delta>?<free>")]
pub async fn increase_combat_skill(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    skill: CombatSkillEnum,
    delta: i32,
    free: Option<bool>,
) -> Result<Json<u32>> {
    let mut character = Character::get(db, &user, character_id).await?;
    let new_value =
        character
            .characteristics
            .increase_combat_skill(&skill, delta, free.unwrap_or(false))?;
    character.save_to_db(db, &user).await?;
    Ok(Json(new_value))
}

#[post(
    "/<character_id>/increase-secondary-attribute/Wisdom/<delta>?<free>",
    data = "<virtue>"
)]
pub async fn increase_wisdom(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    delta: i32,
    virtue: Json<Virtue>,
    free: Option<bool>,
) -> Result<Json<(u32, Vec<Virtue>)>> {
    let mut character = Character::get(db, &user, character_id).await?;
    let new_values =
        character
            .characteristics
            .increase_wisdom(&virtue, delta, free.unwrap_or(false))?;
    character.save_to_db(db, &user).await?;
    Ok(Json(new_values))
}

#[post(
    "/<character_id>/increase-secondary-attribute/Valor/<delta>?<free>",
    data = "<reward>"
)]
pub async fn increase_valor(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    delta: i32,
    reward: Json<Reward>,
    free: Option<bool>,
) -> Result<Json<(u32, Vec<Reward>)>> {
    let mut character = Character::get(db, &user, character_id).await?;
    let new_values =
        character
            .characteristics
            .increase_valor(&reward, delta, free.unwrap_or(false))?;
    character.save_to_db(db, &user).await?;
    Ok(Json(new_values))
}

#[post("/<character_id>/notes", data = "<notes>")]
pub async fn update_notes(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    notes: &str,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.set_notes(notes);
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[get("/<character_id>/notes")]
pub async fn get_notes(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
) -> Result<Json<String>> {
    let character = Character::get(db, &user, character_id).await?;
    Ok(Json(character.get_notes().clone()))
}

#[post("/<character_id>/Valor/advantage", data = "<adv>")]
pub async fn add_reward(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    adv: Json<Reward>,
) -> Result<Json<Vec<Reward>>> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.characteristics.valor.add_advantage(&adv);
    character.save_to_db(db, &user).await?;
    let ret = character.characteristics.get_rewards();
    Ok(Json(ret.to_vec()))
}

#[post("/<character_id>/Wisdom/advantage", data = "<adv>")]
pub async fn add_virtue(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    adv: Json<Virtue>,
) -> Result<Json<Vec<Virtue>>> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.characteristics.wisdom.add_advantage(&adv);
    character.save_to_db(db, &user).await?;
    let ret = character.characteristics.get_virtues();
    Ok(Json(ret.to_vec()))
}

#[post("/<character_id>/long-rest")]
pub async fn long_rest(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.rest(true);
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[post("/<character_id>/short-rest")]
pub async fn short_rest(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.rest(false);
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[post("/<character_id>/fellowship-phase/<yule>")]
pub async fn take_fellowship_phase(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    yule: bool,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.take_fellowship_phase(yule);
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[post("/<character_id>/reward-xp/skill/<skill>/adventure/<adventure>")]
pub async fn reward_xp(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    skill: i32,
    adventure: i32,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.characteristics.reward_xp(skill, adventure)?;
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[post("/<character_id>/convert-shadow")]
pub async fn convert_shadow(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
) -> Result<()> {
    let mut character = Character::get(db, &user, character_id).await?;
    character.characteristics.attributes.shadow.convert();
    character.save_to_db(db, &user).await?;
    Ok(())
}

#[post("/<character_id>/roll?<roll_type>&<bonus_dices>", data = "<skill>")]
pub async fn roll_skill(
    db: &State<DatabaseConnection>,
    user: User,
    character_id: i32,
    bonus_dices: i32,
    skill: Json<SkillType>,
    roll_type: Option<RollType>,
) -> Result<Json<CharacterRoll>> {
    let character = Character::get(db, &user, character_id).await?;
    let skill = skill.into_inner();
    Ok(Json(character.roll(&skill, bonus_dices, roll_type)))
}
