use crate::adversary::Adversary;
use crate::character::Character;
use crate::connection_handler::ConnectionHandler;
use crate::error::Result;
use anyhow::anyhow;
use enums::character_type::CharacterType;
use rocket::{delete, get, post, routes, serde::json::Json, Route, State};
use sea_orm::{ColumnTrait as _, DatabaseConnection, EntityTrait as _, QueryFilter as _};
use serde::{Deserialize, Serialize};

use crate::fellowship::patron::Patron;
use crate::fellowship::request::FellowshipRequest;
use crate::name_or_id::NameOrId;
use crate::{
    cache_response::CacheResponse,
    entity::fellowship as fellowship_entity,
    fellowship::{
        undertaking::{Undertaking, UndertakingEnum},
        Fellowship,
    },
    users::User,
};

use super::socket::Event;
use super::LoremasterOrPlayer;

pub fn get_routes() -> Vec<Route> {
    routes![
        get_undertaking,
        get_fellowships,
        get_fellowship,
        set_fellowship,
        delete_fellowship,
        get_patrons,
        get_patron,
        delete_patron,
        add_character,
        add_adversary,
        delete_adversary,
        delete_character,
        get_player_fellowship,
        increase_eye,
        reset_eye,
        reset_fellowship_points,
        increase_points
    ]
}

#[get("/undertaking/<undertaking>")]
pub fn get_undertaking(undertaking: UndertakingEnum) -> CacheResponse<Undertaking> {
    let undertaking = Undertaking::new(undertaking);
    CacheResponse::new(undertaking)
}

#[get("/")]
pub async fn get_fellowships(
    db: &State<DatabaseConnection>,
    user: User,
) -> Result<Json<Vec<Fellowship>>> {
    let fellowship_ids = fellowship_entity::Entity::find()
        .filter(fellowship_entity::Column::OwnerId.eq(user.id))
        .all(db.inner())
        .await?;

    let mut fellowships = Vec::with_capacity(fellowship_ids.len());
    for id in fellowship_ids {
        fellowships.push(Fellowship::from_db_model(db, &user, &id).await?);
    }

    Ok(Json(fellowships))
}

#[get("/patron")]
pub async fn get_patrons(db: &State<DatabaseConnection>, user: User) -> Result<Json<Vec<Patron>>> {
    let mut out = Patron::get_list_from_enums();
    out.extend(Patron::get_list_from_db(db, &user).await?);
    Ok(Json(out))
}

#[get("/patron/<name_or_id>")]
pub async fn get_patron(
    db: &State<DatabaseConnection>,
    user: User,
    name_or_id: NameOrId,
) -> Result<Json<Patron>> {
    let patron = Patron::get(db, &user, name_or_id).await?;
    Ok(Json(patron))
}

#[get("/<id>")]
pub async fn get_fellowship(
    db: &State<DatabaseConnection>,
    user: User,
    id: i32,
) -> Result<Json<Fellowship>> {
    Ok(Json(Fellowship::get(db, &user, id).await?))
}

#[post("/", data = "<fellowship>")]
pub async fn set_fellowship(
    db: &State<DatabaseConnection>,
    user: User,
    fellowship: Json<Fellowship>,
) -> Result<Json<i32>> {
    let id = fellowship.into_inner().save_to_db(db, &user).await?;

    // Update players
    ConnectionHandler::update_everyone(user.get_username().clone(), Event::UpdateFellowship).await;

    Ok(Json(id))
}

#[delete("/<id>")]
pub async fn delete_fellowship(db: &State<DatabaseConnection>, user: User, id: i32) -> Result<()> {
    Fellowship::delete_from_db(db, &user, id).await?;
    Ok(())
}

#[delete("/patron/<id>")]
pub async fn delete_patron(db: &State<DatabaseConnection>, user: User, id: i32) -> Result<()> {
    Patron::delete_from_db(db, &user, id).await?;
    Ok(())
}

#[derive(Serialize, Deserialize, PartialEq, ts_rs::TS)]
#[ts(export)]
pub enum RequestOrigin {
    Local,
    Extern,
}

#[derive(Serialize, Deserialize, ts_rs::TS)]
#[ts(export)]
pub struct AddToFellowship {
    pub fellowship: String,
    pub loremaster: String,
    pub user: String,
    pub character: Character,
    pub origin: RequestOrigin,
}

#[post("/character", data = "<data>")]
pub async fn add_character(
    db: &State<DatabaseConnection>,
    data: Json<AddToFellowship>,
) -> Result<()> {
    let data = data.into_inner();

    let (fellowship, loremaster) = Fellowship::get_fellowship_and_loremaster_from_names(
        db,
        &data.fellowship,
        &data.loremaster,
    )
    .await?;

    // Add to local db if needed
    let (character, owner) = if data.origin == RequestOrigin::Extern {
        let mut character = data.character;
        character.set_player_name(data.user);
        character.save_external(db, &loremaster).await?;
        (character, loremaster.clone())
    } else {
        (data.character, User::get_user(db, &data.user).await?)
    };

    let id = character
        .get_id()
        .ok_or(anyhow!("Character does not seem to exist in the DB"))?;
    fellowship
        .add_character(db, &loremaster, &owner, id)
        .await?;

    // Update players
    ConnectionHandler::update_everyone(loremaster.get_username().clone(), Event::UpdateFellowship)
        .await;

    Ok(())
}

#[post("/<fellowship_id>/adversary", data = "<adversary>")]
pub async fn add_adversary(
    db: &State<DatabaseConnection>,
    user: User,
    fellowship_id: i32,
    adversary: Json<Adversary>,
) -> Result<()> {
    let adversary = adversary.into_inner();
    let fellowship = Fellowship::get(db, &user, fellowship_id).await?;
    let id = if let Some(id) = adversary.get_id() {
        id
    } else {
        adversary.save_to_db(db, &user).await?
    };
    fellowship.add_adversary(db, &user, id).await?;

    // Update players
    ConnectionHandler::update_everyone(user.username.clone(), Event::UpdateFellowship).await;

    Ok(())
}

#[delete("/<fellowship_id>/adversary/<adversary_id>")]
pub async fn delete_adversary(
    db: &State<DatabaseConnection>,
    user: User,
    fellowship_id: i32,
    adversary_id: i32,
) -> Result<()> {
    let fellowship = Fellowship::get(db, &user, fellowship_id).await?;
    fellowship.remove_adversary(db, &user, adversary_id).await?;

    // Update players
    ConnectionHandler::update_everyone(user.username.clone(), Event::UpdateFellowship).await;

    Ok(())
}

#[delete("/<fellowship_id>/character/<character_id>")]
pub async fn delete_character(
    db: &State<DatabaseConnection>,
    user: User,
    fellowship_id: i32,
    character_id: i32,
) -> Result<()> {
    let fellowship = Fellowship::get(db, &user, fellowship_id).await?;
    fellowship.remove_character(db, &user, character_id).await?;

    let character = Character::get(db, &user, character_id).await?;
    if character.get_character_type() == CharacterType::External {
        Character::delete_from_db(db, character_id, &user).await?;
    }

    // Update players
    ConnectionHandler::update_everyone(user.username.clone(), Event::UpdateFellowship).await;

    Ok(())
}

#[get("/player")]
pub fn get_player_fellowship(request: FellowshipRequest) -> Json<Fellowship> {
    Json(request.fellowship)
}

#[post("/<fellowship_id>/increase-eye/<increase>")]
pub async fn increase_eye(
    db: &State<DatabaseConnection>,
    user: User,
    fellowship_id: i32,
    increase: i32,
) -> Result<Json<u32>> {
    let mut fellowship = Fellowship::get(db, &user, fellowship_id).await?;
    fellowship.increase_eye_rating(increase)?;
    fellowship.save_to_db(db, &user).await?;

    Ok(Json(fellowship.get_eye()))
}

#[post("/<fellowship_id>/reset-eye")]
pub async fn reset_eye(
    db: &State<DatabaseConnection>,
    user: User,
    fellowship_id: i32,
) -> Result<Json<u32>> {
    let mut fellowship = Fellowship::get(db, &user, fellowship_id).await?;
    fellowship.reset_eye_rating();
    fellowship.save_to_db(db, &user).await?;
    Ok(Json(fellowship.get_eye()))
}

#[post("/<fellowship_id>/reset-points")]
pub async fn reset_fellowship_points(
    db: &State<DatabaseConnection>,
    user: User,
    fellowship_id: i32,
) -> Result<Json<u32>> {
    let mut fellowship = Fellowship::get(db, &user, fellowship_id).await?;
    fellowship.reset_points();
    fellowship.save_to_db(db, &user).await?;

    // Update players
    ConnectionHandler::update_everyone(user.username.clone(), Event::UpdateFellowship).await;
    Ok(Json(fellowship.get_points()))
}

#[post("/<fellowship_id>/increase-points/<increase>")]
pub async fn increase_points(
    db: &State<DatabaseConnection>,
    user: LoremasterOrPlayer,
    fellowship_id: i32,
    increase: i32,
) -> Result<Json<u32>> {
    if let LoremasterOrPlayer::Player(_x) = &user {
        if increase > 0 {
            Err(anyhow!(
                "Only the loremaster can increase the fellowship points"
            ))?;
        }
    }
    let loremaster = user.clone().get_loremaster();
    let mut fellowship = user.get_fellowship(db, fellowship_id).await?;

    fellowship.increase_points(increase)?;
    fellowship.save_to_db(db, &loremaster).await?;

    // Update players
    ConnectionHandler::update_everyone(loremaster.username.clone(), Event::UpdateFellowship).await;

    Ok(Json(fellowship.get_eye()))
}
