use anyhow::{Error, Result};
use rocket::{
    request::{self, FromRequest, Outcome},
    Request,
};
use sea_orm::DatabaseConnection;
use serde::Serialize;

use crate::{
    fellowship::{request::FellowshipRequest, Fellowship},
    users::User,
};

pub mod adversary;
pub mod character;
pub mod enums;
pub mod equipment;
pub mod fellowship;
pub mod general;
pub mod message;
pub mod new_character;
pub mod socket;
pub mod tools;

#[derive(Serialize, Clone, Debug)]
pub enum LoremasterOrPlayer {
    Loremaster(User),
    Player(FellowshipRequest),
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for LoremasterOrPlayer {
    type Error = Error;
    async fn from_request(request: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        match User::from_request(request).await {
            request::Outcome::Success(x) => Outcome::Success(Self::Loremaster(x)),
            request::Outcome::Error(_) => match FellowshipRequest::from_request(request).await {
                request::Outcome::Success(x) => Outcome::Success(Self::Player(x)),
                request::Outcome::Error(x) => request::Outcome::Error(x),
                request::Outcome::Forward(_) => panic!("Should not happen"),
            },
            request::Outcome::Forward(_) => panic!("Should not happen"),
        }
    }
}

impl LoremasterOrPlayer {
    pub fn get_loremaster(&self) -> User {
        match self {
            LoremasterOrPlayer::Loremaster(x) => x.clone(),
            LoremasterOrPlayer::Player(x) => x.loremaster.clone(),
        }
    }

    pub async fn get_fellowship(
        &self,
        db: &DatabaseConnection,
        fellowship_id: i32,
    ) -> Result<Fellowship> {
        match self {
            LoremasterOrPlayer::Loremaster(x) => Fellowship::get(db, x, fellowship_id).await,
            LoremasterOrPlayer::Player(x) => Ok(x.fellowship.clone()),
        }
    }
}
