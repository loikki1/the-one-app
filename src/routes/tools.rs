use rocket::serde::json::Json;
use rocket::{get, post, routes, Route};

use crate::roll::{Roll, RollType};
use crate::roll_statistics::RollStatistics;
use crate::{
    cache_response::CacheResponse,
    name_generator::{get_generator, Genders, NameGenerators},
    random_features::RandomFeatures,
};

pub fn get_routes() -> Vec<Route> {
    routes![
        get_name_generator_genders,
        roll_name_generator,
        get_random_features,
        roll_statistics
    ]
}

#[get("/name-generator/<race>/genders")]
pub fn get_name_generator_genders(race: NameGenerators) -> CacheResponse<Vec<Genders>> {
    let generator = get_generator(race);
    CacheResponse::new(generator.get_genders())
}

// Use post to prevent any caching
#[post("/name-generator/roll/<n_rolls>/<race>/<gender>")]
pub fn roll_name_generator(
    n_rolls: u32,
    gender: Genders,
    race: NameGenerators,
) -> CacheResponse<Vec<String>> {
    let generator = get_generator(race);
    let mut out = vec![];
    for _ in 0..n_rolls {
        out.push(generator.get_name(gender));
    }
    CacheResponse::new(out)
}

// Use post to prevent any caching
#[post("/random-features/roll/<n>/<positive>")]
pub fn get_random_features(positive: bool, n: u32) -> Json<Vec<String>> {
    let mut out = vec![];
    for _ in 0..n {
        out.push(RandomFeatures::get(positive));
    }
    Json(out)
}

#[get("/roll-statistics/<n_roll>/<number_dices>/<roll_type>/<miserable>/<weary>")]
pub fn roll_statistics(
    n_roll: u32,
    number_dices: u32,
    roll_type: RollType,
    miserable: bool,
    weary: bool,
) -> CacheResponse<RollStatistics> {
    let mut stats = RollStatistics::new();
    for _ in 0..n_roll {
        stats += Roll::roll_dices(roll_type, number_dices, weary, miserable);
    }

    stats.finalize();
    CacheResponse::new(stats)
}
