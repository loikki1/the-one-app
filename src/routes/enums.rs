use std::collections::HashMap;

use anyhow::anyhow;
use enums::calling::CallingEnum;
use enums::combat_proficiency::CombatProficiency;
use enums::combat_skill::CombatSkillEnum;
use enums::culture::CultureEnum;
use enums::region::Region;
use enums::skill::SkillEnum;
use rocket::{get, routes, Route};
use strum::IntoEnumIterator as _;

use crate::cache_response::CacheResponse;
use crate::character::advantage::AdvantageTrait as _;
use crate::character::armor::{Armor, ArmorEnum};
use crate::character::equipment::{Equipment, EquipmentObject, EquipmentType};
use crate::character::reward::{Reward, RewardEnum};
use crate::character::secondary_attributes::{SecondaryAttributeAdvantage, SecondaryAttributeEnum};
use crate::character::shield::{Shield, ShieldEnum};
use crate::character::treasure::StandardOfLiving;
use crate::character::virtue::{Virtue, VirtueEnum};
use crate::character::weapon::{Weapon, WeaponEnum};
use crate::error::Result;
use crate::fellowship::undertaking::UndertakingEnum;
use crate::name_generator::NameGenerators;
use crate::roll::RollType;

pub fn get_routes() -> Vec<Route> {
    routes![
        get_skills,
        get_combat_skills,
        get_combat_proficiencies,
        get_callings,
        get_cultures,
        get_standard_living,
        get_secondary_attributes,
        get_empty_item,
        get_undertakings,
        get_name_generator_races,
        get_roll_type,
        get_advantage,
        get_virtues,
        get_rewards,
        get_virtue,
        get_reward,
        get_item,
        get_list_items,
        get_eye_threshold
    ]
}

#[get("/skills")]
pub fn get_skills() -> CacheResponse<Vec<SkillEnum>> {
    CacheResponse::new(SkillEnum::iter().collect())
}

#[get("/combat-skills")]
pub fn get_combat_skills() -> CacheResponse<Vec<CombatSkillEnum>> {
    CacheResponse::new(CombatSkillEnum::iter().collect())
}

#[get("/combat-proficiencies")]
pub fn get_combat_proficiencies() -> CacheResponse<Vec<CombatProficiency>> {
    CacheResponse::new(CombatProficiency::iter().collect())
}

#[get("/callings")]
pub fn get_callings() -> CacheResponse<Vec<CallingEnum>> {
    CacheResponse::new(CallingEnum::iter().collect())
}

#[get("/cultures")]
pub fn get_cultures() -> CacheResponse<Vec<CultureEnum>> {
    CacheResponse::new(CultureEnum::iter().collect())
}

#[get("/standard-living")]
pub fn get_standard_living() -> CacheResponse<Vec<StandardOfLiving>> {
    CacheResponse::new(StandardOfLiving::iter().collect())
}

#[get("/secondary-attributes")]
pub fn get_secondary_attributes() -> CacheResponse<Vec<SecondaryAttributeEnum>> {
    CacheResponse::new(SecondaryAttributeEnum::iter().collect())
}

#[get("/undertaking")]
pub fn get_undertakings() -> CacheResponse<Vec<UndertakingEnum>> {
    CacheResponse::new(UndertakingEnum::iter().collect())
}

#[get("/empty/<eq_type>", rank = 1)]
pub fn get_empty_item(eq_type: EquipmentType) -> CacheResponse<EquipmentObject> {
    CacheResponse::new(match eq_type {
        EquipmentType::Armor => EquipmentObject::Armor(Armor::new(String::new(), 0, 0)),
        EquipmentType::Equipment => EquipmentObject::Equipment(Equipment::new(String::new(), 0)),
        EquipmentType::Shield => EquipmentObject::Shield(Shield::new(String::new(), 0, 0)),
        EquipmentType::Weapon => EquipmentObject::Weapon(Weapon::new(
            String::new(),
            0,
            0,
            None,
            0,
            CombatProficiency::Brawling,
        )),
    })
}

#[get("/empty/<attr>", rank = 2)]
pub fn get_advantage(attr: SecondaryAttributeEnum) -> CacheResponse<SecondaryAttributeAdvantage> {
    let answer = match attr {
        SecondaryAttributeEnum::Valor => SecondaryAttributeAdvantage::Reward(Reward::default()),
        SecondaryAttributeEnum::Wisdom => SecondaryAttributeAdvantage::Virtue(Virtue::default()),
    };
    CacheResponse::new(answer)
}

#[get("/name-generator/races")]
pub fn get_name_generator_races() -> CacheResponse<Vec<NameGenerators>> {
    CacheResponse::new(NameGenerators::iter().collect())
}

#[get("/roll-type")]
pub fn get_roll_type() -> CacheResponse<Vec<RollType>> {
    CacheResponse::new(RollType::iter().collect())
}

#[get("/virtue?<culture>&<creation>")]
pub fn get_virtues(
    culture: Option<CultureEnum>,
    creation: Option<bool>,
) -> CacheResponse<HashMap<VirtueEnum, Virtue>> {
    let mut out = HashMap::new();
    let creation = creation.unwrap_or(false);

    for virtue in VirtueEnum::iter() {
        let vir = Virtue::from_enum(&virtue);
        let restricted_to = vir.get_culture_restrictions();
        // at creation? only creation : all
        let ok_creation = creation == vir.is_available_at_creation() || !creation;
        // culture? culture + none : all
        let ok_culture = culture.is_none()
            || restricted_to.is_none()
            || restricted_to.unwrap().contains(&culture.clone().unwrap());

        if vir.is_available() && ok_creation && ok_culture {
            out.insert(virtue, vir);
        }
    }
    CacheResponse::new(out)
}

#[get("/reward?<creation>")]
pub fn get_rewards(creation: Option<bool>) -> CacheResponse<HashMap<RewardEnum, Reward>> {
    let mut out = HashMap::new();
    for reward in RewardEnum::iter() {
        let rew = Reward::from_enum(&reward);

        if creation.is_none()
            || !creation.unwrap()
            || (rew.is_available_at_creation() && creation.unwrap())
        {
            out.insert(reward, rew);
        }
    }
    CacheResponse::new(out)
}

#[get("/reward/<name>")]
pub fn get_reward(name: RewardEnum) -> CacheResponse<Reward> {
    CacheResponse::new(Reward::from_enum(&name))
}

#[get("/virtue/<name>")]
pub fn get_virtue(name: VirtueEnum) -> CacheResponse<Virtue> {
    CacheResponse::new(Virtue::from_enum(&name))
}

#[get("/<eq_type>/<item_name>", rank = 3)]
pub fn get_item(eq_type: EquipmentType, item_name: &str) -> Result<CacheResponse<EquipmentObject>> {
    Ok(CacheResponse::new(match eq_type {
        EquipmentType::Armor => EquipmentObject::Armor(Armor::from(item_name)),
        EquipmentType::Shield => EquipmentObject::Shield(Shield::from(item_name)),
        EquipmentType::Weapon => EquipmentObject::Weapon(Weapon::from(item_name)),
        EquipmentType::Equipment => Err(anyhow!("No standard equipment available"))?,
    }))
}

#[get("/item/<eq_type>")]
pub fn get_list_items(eq_type: EquipmentType) -> Result<CacheResponse<Vec<String>>> {
    Ok(CacheResponse::new(match eq_type {
        EquipmentType::Armor => ArmorEnum::iter()
            .map(|x| format!("{x:?}"))
            .collect::<Vec<String>>(),
        EquipmentType::Equipment => Err(anyhow!("No standard equipment available"))?,
        EquipmentType::Shield => ShieldEnum::iter()
            .map(|x| format!("{x:?}"))
            .collect::<Vec<String>>(),
        EquipmentType::Weapon => WeaponEnum::iter()
            .map(|x| format!("{x:?}"))
            .collect::<Vec<String>>(),
    }))
}

#[get("/eye-threshold/<eye>")]
pub fn get_eye_threshold(eye: u32) -> CacheResponse<Option<Region>> {
    CacheResponse::new(Region::hunt_threshold(eye))
}
