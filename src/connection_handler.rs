use anyhow::{anyhow, Result};
use futures::stream::FusedStream as _;
use lazy_static::lazy_static;
use rocket::{futures::SinkExt as _, info, tokio::sync::Mutex};
use rocket_ws::stream::DuplexStream;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, sync::Arc};
use ts_rs::TS;

use crate::routes::socket::Event;

// Define a few types
pub type RocketMessage = rocket_ws::Message;
pub type SafeStream = Arc<Mutex<DuplexStream>>;
pub type Conn = Arc<Mutex<HashMap<String, ConnectionHandler>>>;

// Define the global connection handler
lazy_static! {
    pub static ref CONNECTIONS: Conn = Arc::new(Mutex::new(HashMap::new()));
}

#[derive(Serialize, Deserialize, Clone, TS)]
#[ts(export)]
pub struct Connection {
    pub player: String,
    pub loremaster: String,
    pub fellowship: String,
}

pub struct ConnectionHandler {
    connections: HashMap<String, SafeStream>,
}

impl ConnectionHandler {
    pub fn new(name: String, stream: SafeStream) -> Self {
        let mut out = Self {
            connections: HashMap::new(),
        };
        out.add(name, stream);
        out
    }

    pub async fn update_everyone(loremaster: String, event: Event) {
        let mut handler = CONNECTIONS.lock().await;
        let conn = handler.get_mut(&loremaster);
        info!("updating {loremaster}");
        if let Some(conn) = conn {
            let err = conn.send_to_players(event).await;
            if let Err(x) = err {
                eprintln!("Error while updating everyone: {x:?}");
            }
        }
    }

    pub async fn update_single(loremaster: String, player: &str, event: Event) {
        let mut handler = CONNECTIONS.lock().await;
        let conn = handler.get_mut(&loremaster);
        info!("updating message {loremaster} - {player}");
        if let Some(conn) = conn {
            let err = conn.send_to_single_player(player, event).await;
            if let Err(x) = err {
                eprintln!("Error while updating everyone: {x:?}");
            }
        }
    }

    pub fn add(&mut self, conn: String, stream: SafeStream) {
        info!("Adding a connection {conn}");
        self.connections.insert(conn, stream);
    }

    pub async fn remove(&mut self, user: &str) {
        info!("Removing a connection {user}");
        self.connections.remove(user);
        self.remove_terminated_connections().await;
    }

    pub async fn send_to_single_player<T>(&mut self, username: &str, obj: T) -> Result<()>
    where
        T: Serialize,
    {
        let text = serde_json::to_string(&obj)?;
        let conn = self
            .connections
            .get(username)
            .ok_or(anyhow!(format!("Cannot find user {username}")))?;
        let mut conn = conn.lock().await;
        conn.send(RocketMessage::Text(text.clone())).await?;
        Ok(())
    }

    async fn send_to_players<T>(&mut self, obj: T) -> Result<()>
    where
        T: Serialize,
    {
        let text = serde_json::to_string(&obj)?;
        let mut status = Ok(());
        for locked_conn in &self.connections {
            let mut conn = locked_conn.1.lock().await;
            match conn.send(RocketMessage::Text(text.clone())).await {
                Ok(()) => {}
                Err(x) => status = Err(x),
            };
        }
        status?;
        Ok(())
    }

    pub async fn cleanup() {
        info!("Cleanup sockets");
        let mut handler = CONNECTIONS.lock().await;
        handler.retain(|_, x| !x.connections.is_empty());
    }

    async fn remove_terminated_connections(&mut self) {
        let mut keys: Vec<String> = vec![];
        for locked_conn in &self.connections {
            let conn = locked_conn.1.lock().await;
            if conn.is_terminated() {
                keys.push(locked_conn.0.to_string());
            };
        }
        for key in &keys {
            self.connections.remove(key);
        }
    }
}
