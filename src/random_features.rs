use rand::seq::SliceRandom as _;
use serde::Deserialize;
use ts_rs::TS;

use crate::assets::read_file;

#[derive(Deserialize, TS)]
#[ts(export)]
pub struct RandomFeatures {
    positive: Vec<String>,
    negative: Vec<String>,
}

impl RandomFeatures {
    pub fn get(positive: bool) -> String {
        const FILENAME: &str = "random_features/features.json";

        let data: Self = read_file(FILENAME);

        let mut rng = rand::thread_rng();
        if positive {
            data.positive
        } else {
            data.negative
        }
        .choose(&mut rng)
        .unwrap()
        .to_string()
    }
}
