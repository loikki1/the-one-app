use crate::users::User;
use anyhow::{anyhow, Error};
use serde::Serialize;

use super::Fellowship;
use rocket::State;
use rocket::{http::Status, outcome::Outcome, request, request::FromRequest, request::Request};
use sea_orm::DatabaseConnection;

#[derive(Serialize, Debug, Clone)]
pub struct FellowshipRequest {
    pub fellowship: Fellowship,
    pub loremaster: User,
}

impl FellowshipRequest {
    pub fn new(fellowship: Fellowship, loremaster: User) -> Self {
        Self {
            fellowship,
            loremaster,
        }
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for FellowshipRequest {
    type Error = Error;

    async fn from_request(request: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        if let Outcome::Success(db) = request.guard::<&State<DatabaseConnection>>().await {
            match Fellowship::check_request(db, request).await {
                Ok(x) => Outcome::Success(FellowshipRequest::new(x.0, x.1)),
                Err(x) => Outcome::Error((Status::Unauthorized, x)),
            }
        } else {
            Outcome::Error((Status::Unauthorized, anyhow!("Failed to reach DB")))
        }
    }
}
