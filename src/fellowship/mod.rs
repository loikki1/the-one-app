use crate::adversary::Adversary;
use crate::check_ownership_none_is_ok;
use crate::entity::fellowship as fellowship_entity;
use crate::name_or_id::NameOrId;
use crate::users::User;
use crate::utils::convert_option_to_set;
use anyhow::{anyhow, bail, Result};
use patron::Patron;
use rocket::{error, request::Request};
use sea_orm::{
    ActiveModelTrait as _, ColumnTrait as _, ConnectionTrait, DatabaseConnection, EntityTrait as _,
    IntoActiveModel as _, QueryFilter as _, Set, TransactionTrait as _,
};
use serde::{Deserialize, Serialize};
use std::cmp::max;

use crate::entity::adversary as adversary_entity;
use crate::entity::character as character_entity;
use crate::{
    character::Character, entity::fellowship_adversary as fellowship_adversary_entity,
    entity::fellowship_character as fellowship_character_entity, entity::message as message_entity,
};
use ts_rs::TS;

pub mod patron;
pub mod request;
pub mod undertaking;

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct Fellowship {
    id: Option<i32>,
    name: String,
    characters: Vec<Character>,
    adversaries: Vec<Adversary>,
    patron: Option<Patron>,
    points: u32,
    eye: u32,
}

impl Fellowship {
    /// Only player can add
    pub async fn add_character(
        &self,
        db: &DatabaseConnection,
        fellowship_owner: &User,
        character_owner: &User,
        character_id: i32,
    ) -> Result<()> {
        check_ownership_none_is_ok!(db, character_owner, Some(character_id), character_entity)?;
        check_ownership_none_is_ok!(db, fellowship_owner, self.id, fellowship_entity)?;

        let fellowship_id = self
            .id
            .ok_or(anyhow!("Cannot add a character to a fellowship without id"))?;
        // Check if already exist
        if fellowship_character_entity::Entity::find()
            .filter(fellowship_character_entity::Column::CharacterId.eq(character_id))
            .filter(fellowship_character_entity::Column::FellowshipId.eq(fellowship_id))
            .one(db)
            .await?
            .is_some()
        {
            bail!("Character is already in the fellowship");
        };

        fellowship_character_entity::ActiveModel {
            character_id: Set(character_id),
            fellowship_id: Set(fellowship_id),
        }
        .insert(db)
        .await?;
        Ok(())
    }

    /// Only loremasters can remove
    pub async fn remove_character(
        &self,
        db: &DatabaseConnection,
        user: &User,
        character_id: i32,
    ) -> Result<()> {
        check_ownership_none_is_ok!(db, user, self.id, fellowship_entity)?;

        let fellowship_id = self
            .id
            .ok_or(anyhow!("Cannot add a character to a fellowship without id"))?;
        fellowship_character_entity::ActiveModel {
            fellowship_id: Set(fellowship_id),
            character_id: Set(character_id),
        }
        .delete(db)
        .await?;

        Ok(())
    }

    pub async fn add_adversary(
        &self,
        db: &DatabaseConnection,
        user: &User,
        adversary_id: i32,
    ) -> Result<()> {
        check_ownership_none_is_ok!(db, user, Some(adversary_id), adversary_entity)?;
        check_ownership_none_is_ok!(db, user, self.id, fellowship_entity)?;

        // Check if already exist
        let fellowship_id = self.id.ok_or(anyhow!(
            "Cannot add an adversary to a fellowship without id"
        ))?;
        if fellowship_adversary_entity::Entity::find()
            .filter(fellowship_adversary_entity::Column::AdversaryId.eq(adversary_id))
            .filter(fellowship_adversary_entity::Column::FellowshipId.eq(fellowship_id))
            .one(db)
            .await?
            .is_some()
        {
            bail!("Adversary is already in the fellowship");
        };

        fellowship_adversary_entity::ActiveModel {
            adversary_id: Set(adversary_id),
            fellowship_id: Set(fellowship_id),
        }
        .insert(db)
        .await?;

        Ok(())
    }

    pub async fn remove_adversary(
        &self,
        db: &DatabaseConnection,
        user: &User,
        adversary_id: i32,
    ) -> Result<()> {
        if self.id.is_none() {
            bail!("You need a fellowship id to remove an adversary");
        }
        check_ownership_none_is_ok!(db, user, self.id, fellowship_entity)?;

        let fellowship_id = self
            .id
            .ok_or(anyhow!("Cannot add a character to a fellowship without id"))?;
        fellowship_adversary_entity::ActiveModel {
            adversary_id: Set(adversary_id),
            fellowship_id: Set(fellowship_id),
        }
        .delete(db)
        .await?;
        Ok(())
    }

    pub async fn unset_patron<C: ConnectionTrait>(
        db: &C,
        user: &User,
        patron_id: i32,
    ) -> Result<()> {
        let owner_id = user.id.ok_or(anyhow!("User should have an id"))?;
        let mut fellowships = fellowship_entity::Entity::find()
            .filter(fellowship_entity::Column::OwnerId.eq(owner_id))
            .filter(fellowship_entity::Column::PatronId.eq(patron_id))
            .all(db)
            .await?;

        if fellowships.is_empty() {
            return Ok(());
        }

        fellowships.iter_mut().for_each(|x| x.patron_id = None);
        let fellowships: Vec<_> = fellowships
            .iter()
            .map(|x| x.clone().into_active_model())
            .collect();
        fellowship_entity::Entity::insert_many(fellowships)
            .exec(db)
            .await?;
        Ok(())
    }

    async fn get_adversaries(
        db: &DatabaseConnection,
        user: &User,
        fellowship_id: i32,
    ) -> Result<Vec<Adversary>> {
        let adversary_ids: Vec<i32> = fellowship_adversary_entity::Entity::find()
            .filter(fellowship_adversary_entity::Column::FellowshipId.eq(fellowship_id))
            .all(db)
            .await?
            .iter()
            .map(|x| x.adversary_id)
            .collect();

        let mut out: Vec<Adversary> = Vec::with_capacity(adversary_ids.len());
        for id in &adversary_ids {
            out.push(Adversary::get(db, user, NameOrId::Id(*id)).await?);
        }
        Ok(out)
    }

    async fn get_characters(db: &DatabaseConnection, fellowship_id: i32) -> Result<Vec<Character>> {
        let character_ids: Vec<i32> = fellowship_character_entity::Entity::find()
            .filter(fellowship_character_entity::Column::FellowshipId.eq(fellowship_id))
            .all(db)
            .await?
            .iter()
            .map(|x| x.character_id)
            .collect();

        let mut out: Vec<Character> = Vec::with_capacity(character_ids.len());
        for id in &character_ids {
            let owner_id = Character::get_owner_id(db, *id).await?;
            let user = User::get_user_from_id(db, owner_id).await?;
            out.push(Character::get(db, &user, *id).await?);
        }
        Ok(out)
    }

    pub async fn get(db: &DatabaseConnection, user: &User, id: i32) -> Result<Self> {
        let fellowship = fellowship_entity::Entity::find_by_id(id)
            .one(db)
            .await?
            .ok_or(anyhow!("Cannot find fellowship"))?;

        Self::from_db_model(db, user, &fellowship).await
    }

    pub async fn from_db_model(
        db: &DatabaseConnection,
        user: &User,
        value: &fellowship_entity::Model,
    ) -> Result<Self> {
        check_ownership_none_is_ok!(db, user, Some(value.id), fellowship_entity)?;

        let characters = Self::get_characters(db, value.id).await?;
        let adversaries = Self::get_adversaries(db, user, value.id).await?;
        let patron = if let Some(id) = value.patron_id {
            Some(Patron::get(db, user, NameOrId::Id(id)).await?)
        } else {
            None
        };
        Ok(Self {
            id: Some(value.id),
            name: value.name.clone(),
            characters,
            adversaries,
            patron,
            points: value.points.try_into()?,
            eye: value.eye.try_into()?,
        })
    }

    pub fn get_points(&self) -> u32 {
        self.points
    }

    pub fn increase_points(&mut self, increase: i32) -> Result<u32> {
        self.points = self
            .points
            .checked_add_signed(increase)
            .ok_or(anyhow!("Failed to increase points"))?;
        Ok(self.points)
    }

    pub fn reset_points(&mut self) {
        self.points = 0;
        for character in &self.characters {
            self.points += character.get_initial_fellowship_points();
        }
        self.points += self.patron.clone().map_or(0, |x| x.get_fellowship_points());
    }

    pub async fn delete_from_db(db: &DatabaseConnection, user: &User, id: i32) -> Result<()> {
        check_ownership_none_is_ok!(db, user, Some(id), fellowship_entity)?;

        let transaction = db.begin().await?;

        message_entity::Entity::delete_many()
            .filter(message_entity::Column::FellowshipId.eq(id))
            .exec(&transaction)
            .await?;

        fellowship_adversary_entity::Entity::delete_many()
            .filter(fellowship_adversary_entity::Column::FellowshipId.eq(id))
            .exec(&transaction)
            .await?;

        fellowship_character_entity::Entity::delete_many()
            .filter(fellowship_character_entity::Column::FellowshipId.eq(id))
            .exec(&transaction)
            .await?;

        fellowship_entity::Entity::delete_by_id(id)
            .exec(&transaction)
            .await?;

        transaction.commit().await?;
        Ok(())
    }

    /// This function does not save characters and players
    pub async fn save_to_db(&self, db: &DatabaseConnection, user: &User) -> Result<i32> {
        check_ownership_none_is_ok!(db, user, self.id, fellowship_entity)?;

        let patron_id = if let Some(patron) = &self.patron {
            if patron.get_id().is_none() {
                Some(patron.save_to_db(db, user).await?)
            } else {
                patron.get_id()
            }
        } else {
            None
        };
        let fellowship_id = fellowship_entity::ActiveModel {
            id: convert_option_to_set(self.id),
            name: Set(self.name.clone()),
            points: Set(self.points.try_into()?),
            patron_id: Set(patron_id),
            eye: Set(self.eye.try_into()?),
            owner_id: convert_option_to_set(user.id),
        }
        .save(db)
        .await?
        .id
        .unwrap();

        // Only players can add their characters to a fellowship
        // => No saving (delete is done in another function)

        // Adversary: To mimic API for characters, do it outside this function
        Ok(fellowship_id)
    }

    pub fn get_eye(&self) -> u32 {
        self.eye
    }

    pub fn reset_eye_rating(&mut self) {
        let culture_eye = self
            .characters
            .iter()
            .fold(0, |acc, x| max(acc, x.get_culture().get_starting_eye()));

        let starting_eye = self.characters.iter().fold(0, |acc, x| {
            acc + x.get_starting_eye_rating_without_culture()
        });

        self.eye = culture_eye + starting_eye;
    }

    pub fn increase_eye_rating(&mut self, increase: i32) -> Result<u32> {
        self.eye = self
            .eye
            .checked_add_signed(increase)
            .ok_or(anyhow!("Failed to increase eye rating"))?;
        Ok(self.eye)
    }

    pub async fn get_fellowship_and_loremaster_from_names(
        db: &DatabaseConnection,
        fellowship_name: &str,
        loremaster: &str,
    ) -> Result<(Self, User)> {
        let loremaster = User::get_user(db, loremaster).await.map_err(|x| {
            error!("{x}");
            anyhow!("Loremaster does not exist")
        })?;

        let fellowship = fellowship_entity::Entity::find()
            .filter(fellowship_entity::Column::Name.eq(fellowship_name))
            .filter(fellowship_entity::Column::OwnerId.eq(loremaster.id))
            .one(db)
            .await?
            .ok_or(anyhow!("Fellowship does not exist"))?;

        let fellowship = Self::from_db_model(db, &loremaster, &fellowship).await?;
        Ok((fellowship, loremaster))
    }

    pub async fn check_request(db: &DatabaseConnection, req: &Request<'_>) -> Result<(Self, User)> {
        if cfg!(not(feature = "cloud")) {
            bail!("Cannot use a fellowship outside the cloud version");
        }

        // Get the headers
        let headers = req.headers();

        // Fellowship name
        let fellowship_name = headers
            .get_one("fellowship")
            .ok_or(anyhow!("Missing fellowship"))?;

        // loremaster
        let loremaster = headers
            .get_one("loremaster")
            .ok_or(anyhow!("Missing loremaster"))?;

        Self::get_fellowship_and_loremaster_from_names(db, fellowship_name, loremaster).await
    }
}
