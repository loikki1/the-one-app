use crate::check_ownership_none_is_ok;
use crate::entity::patron as patron_entity;
use crate::name_or_id::NameOrId;
use crate::utils::convert_option_to_set;
use anyhow::{anyhow, bail, Result};
use sea_orm::{
    ActiveModelTrait as _, ColumnTrait as _, DatabaseConnection, EntityTrait as _,
    QueryFilter as _, Set, TransactionTrait as _,
};
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter, IntoEnumIterator as _};
use ts_rs::TS;

use crate::{assets::read_enum_from_assets, users::User};

use super::Fellowship;

const DIRECTORY: &str = "fellowship/patron";

#[derive(Debug, Clone, Serialize, Deserialize, TS)]
pub struct Patron {
    id: Option<i32>,
    name: String,
    fellowship_points: u32,
    advantage: String,
}

impl Patron {
    pub fn new(patron: PatronEnum) -> Self {
        read_enum_from_assets(&patron, DIRECTORY)
    }

    pub async fn save_to_db(&self, db: &DatabaseConnection, user: &User) -> Result<i32> {
        let user_id = user.id.ok_or(anyhow!("User should have an id"))?;
        check_ownership_none_is_ok!(db, user, self.id, patron_entity)?;

        let id = convert_option_to_set(self.id);
        let entity = patron_entity::ActiveModel {
            id,
            name: Set(self.name.clone()),
            fellowship_points: Set(self.fellowship_points.try_into()?),
            advantage: Set(self.advantage.clone()),
            owner_id: Set(user_id),
        };

        let entity = entity.save(db).await?;
        Ok(entity.id.unwrap())
    }

    pub fn get_id(&self) -> Option<i32> {
        self.id
    }

    pub fn get_fellowship_points(&self) -> u32 {
        self.fellowship_points
    }

    pub async fn get_list_from_db(db: &DatabaseConnection, user: &User) -> Result<Vec<Self>> {
        let entities = patron_entity::Entity::find()
            .filter(patron_entity::Column::OwnerId.eq(user.id))
            .all(db)
            .await?;

        let mut patrons = Vec::with_capacity(entities.len());
        for ent in entities {
            patrons.push(Patron::from_db_model(&ent)?);
        }
        Ok(patrons)
    }

    pub fn get_list_from_enums() -> Vec<Self> {
        PatronEnum::iter().map(Self::new).collect()
    }

    pub async fn get(db: &DatabaseConnection, user: &User, id: NameOrId) -> Result<Self> {
        match id {
            NameOrId::Name(x) => {
                let name = format!("\"{x}\"");
                let patron = serde_json::from_str::<PatronEnum>(&name)?;
                Ok(Self::new(patron))
            }
            NameOrId::Id(id) => {
                check_ownership_none_is_ok!(db, user, Some(id), patron_entity)?;

                let patron = patron_entity::Entity::find_by_id(id).one(db).await?;
                if let Some(patron) = patron {
                    Self::from_db_model(&patron)
                } else {
                    bail!("Patron not found");
                }
            }
        }
    }

    pub fn from_db_model(value: &patron_entity::Model) -> Result<Self> {
        Ok(Self {
            id: Some(value.id),
            name: value.name.clone(),
            fellowship_points: value.fellowship_points.try_into()?,
            advantage: value.advantage.clone(),
        })
    }

    pub async fn delete_from_db(
        db: &DatabaseConnection,
        user: &User,
        patron_id: i32,
    ) -> Result<()> {
        check_ownership_none_is_ok!(db, user, Some(patron_id), patron_entity)?;

        let transaction = db.begin().await?;

        Fellowship::unset_patron(&transaction, user, patron_id).await?;

        patron_entity::Entity::delete_by_id(patron_id)
            .exec(&transaction)
            .await?;

        transaction.commit().await?;
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug, EnumIter, EnumCount, TS)]
pub enum PatronEnum {
    Balin,
    Bilbo,
    Cirdan,
    Gandalf,
    Gilraen,
    TomBombadilAndLadyGoldberry,
    Frora,
    Mjolin,
    Daza,
    Nal,
    Saruman,
    Elrond,
}

#[cfg(test)]
mod test {
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::assets::get_number_files;

    use super::{Patron, PatronEnum, DIRECTORY};

    #[test]
    fn check_patron_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(PatronEnum::COUNT == number_files);

        for patron in PatronEnum::iter() {
            eprintln!("Testing {patron:?}");
            let _: Patron = Patron::new(patron);
        }
    }
}
