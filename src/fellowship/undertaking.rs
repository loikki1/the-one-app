use crate::assets::read_enum_from_assets;
use enums::calling::CallingEnum;
use rocket::request::FromParam;
use serde::{Deserialize, Serialize};

use strum::{EnumCount, EnumIter};
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

const DIRECTORY: &str = "fellowship/undertaking";

#[derive(Debug, Serialize, Deserialize, Clone, FromParamEnum, EnumIter, EnumCount, TS)]
#[ts(export)]
pub enum UndertakingEnum {
    GatherRumours,
    HealScars,
    MeetPatron,
    PonderStoriedAndFiguredMaps,
    RaiseAnHeir,
    RecountAStory,
    StrengthenFellowship,
    StudyMagicalItems,
    WriteASong,
}

#[derive(Debug, Serialize, Deserialize, Clone, TS)]
#[ts(export)]
pub struct Undertaking {
    title: String,
    description: String,
    yule: bool,
    free_for: Option<CallingEnum>,
}

impl Undertaking {
    pub fn new(undertaking: UndertakingEnum) -> Self {
        read_enum_from_assets(&undertaking, DIRECTORY)
    }
}

#[cfg(test)]
mod test {
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{assets::get_number_files, fellowship::undertaking::DIRECTORY};

    use super::{Undertaking, UndertakingEnum};

    #[test]
    fn check_undertaking_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(UndertakingEnum::COUNT == number_files);

        for undertaking in UndertakingEnum::iter() {
            eprintln!("Testing {undertaking:?}");
            Undertaking::new(undertaking);
        }
    }
}
