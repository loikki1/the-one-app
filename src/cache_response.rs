use std::io::Cursor;

use rocket::{
    error,
    http::{ContentType, Status},
    response::{self, Responder},
    Request, Response,
};
use serde::Serialize;

pub struct CacheResponse<T> {
    data: T,
}

impl<T> CacheResponse<T> {
    pub fn new(data: T) -> Self {
        Self { data }
    }
}

impl<'r, T> Responder<'r, 'static> for CacheResponse<T>
where
    T: Serialize,
{
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
        let data = serde_json::to_string(&self.data).map_err(|error| {
            error!("Failure in cache {error}");
            Status::InternalServerError
        })?;
        Response::build()
            .header(ContentType::JSON)
            // 1 day of cache
            .raw_header("Cache-Control", "max-age=86400")
            .sized_body(data.len(), Cursor::new(data))
            .ok()
    }
}
