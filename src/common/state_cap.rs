use serde::{Deserialize, Serialize};
use std::cmp;
use ts_rs::TS;

#[derive(Default, Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct StateCap {
    #[serde(default)]
    current: u32,
    max: u32,
}

impl StateCap {
    pub fn new(max: u32) -> Self {
        Self { current: max, max }
    }

    pub fn set_current(&mut self, current: u32) {
        self.current = cmp::min(self.max, current);
    }

    pub fn loose(&mut self, value: i32) {
        self.current = match self.current.checked_add_signed(-value) {
            Some(val) => val,
            None => {
                if value > 0 {
                    0
                } else {
                    self.max
                }
            }
        };
        self.set_current(self.current);
    }

    pub fn get_max(&self) -> u32 {
        self.max
    }

    pub fn get_current(&self) -> u32 {
        self.current
    }

    pub fn reset(&mut self) {
        self.current = self.max;
    }
}
