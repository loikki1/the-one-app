use anyhow::{bail, Result};
use migration::MigratorTrait as _;
use rocket::{
    http::Method,
    tokio::{task::spawn_blocking, time::sleep},
    Build, Rocket,
};
use rocket_cors::{AllowedOrigins, CorsOptions};
use sea_orm::{ConnectOptions, Database, DatabaseConnection};
use std::{net::Ipv4Addr, time::Duration};

use crate::{
    config::{Config, CONFIG},
    convert_to_sql,
    error_fairing::ErrorFairing,
    routes::{
        adversary, character, enums, equipment, fellowship, general, message as message_route,
        new_character, socket, tools,
    },
};

#[cfg(not(feature = "cloud"))]
use crate::users::User;

async fn get_configured_config() -> Result<Config> {
    const LIMIT: u32 = 100;
    let mut step: u32 = 0;
    while step < LIMIT {
        step += 1;
        let config = CONFIG.lock().await;
        if config.is_configured() {
            return Ok(config.clone());
        }
        eprintln!("Not configured yet");
        sleep(Duration::new(0, 100)).await;
    }

    bail!("Failed to get a configuration")
}

pub async fn prepare_db(url: &str) -> Result<DatabaseConnection> {
    let mut options = ConnectOptions::new(url);
    options.sqlx_logging(false);
    let db = Database::connect(options).await?;

    migration::Migrator::up(&db, None).await?;

    #[cfg(not(feature = "cloud"))]
    User::create_empty_user(&db).await?;

    Ok(db)
}

pub async fn get_rocket() -> Result<Rocket<Build>> {
    eprintln!("Starting rocket");
    let config = spawn_blocking(get_configured_config).await?.await?;
    eprintln!("Configuration: {config:?}");

    let ip = if config.expose_rocket() {
        eprintln!("Exposing rocket");
        Ipv4Addr::new(0, 0, 0, 0)
    } else {
        eprintln!("Running on localhost");
        Ipv4Addr::new(127, 0, 0, 1)
    };

    let rocket_config = rocket::Config::figment().merge(("address", ip));

    // Cors
    let cors = CorsOptions::default()
        .allowed_origins(AllowedOrigins::all())
        .allowed_methods(
            vec![
                Method::Get,
                Method::Post,
                Method::Patch,
                Method::Put,
                Method::Delete,
            ]
            .into_iter()
            .map(From::from)
            .collect(),
        )
        .allow_credentials(true);

    // DB
    let database_url = format!(
        "sqlite://{}?mode=rwc",
        config
            .get_directory()
            .join("data.db")
            .to_str()
            .expect("Should be able to convert to string")
    );
    eprintln!("Database: {database_url}");
    let db = prepare_db(&database_url).await?;

    // Convert old json files to sql
    convert_to_sql::run(&db, &config.get_directory().join("TheOneApp")).await?;

    // Fairing
    Ok(rocket::custom(rocket_config)
        .manage(db)
        .attach(ErrorFairing)
        .mount("/character", character::get_routes())
        .mount("/adversary", adversary::get_routes())
        .mount("/equipment", equipment::get_routes())
        .mount("/new-character", new_character::get_routes())
        .mount("/", enums::get_routes())
        .mount("/", general::get_routes())
        .mount("/", tools::get_routes())
        .mount("/fellowship", fellowship::get_routes())
        .mount("/socket", socket::get_routes())
        .mount("/message", message_route::get_routes())
        .attach(cors.to_cors().expect("failed to setup cors")))
}
