use convert_case::{Case, Casing as _};
use sea_orm::ActiveValue::{self, NotSet};
use sea_orm::Set;
use unidecode::unidecode;

pub fn uppercase_first_letter(string: &str) -> String {
    let mut chars = string.chars();
    chars.next().map_or_else(String::new, |text| {
        text.to_uppercase().collect::<String>() + chars.as_str()
    })
}

pub fn to_enum(name: &str) -> String {
    let mut name = name.to_case(Case::Title);
    name = name.replace([' ', '-', '!', '\''], "");

    unidecode(&name)
}

pub fn convert_option_to_set<T>(value: Option<T>) -> ActiveValue<T>
where
    sea_orm::Value: From<T>,
{
    if let Some(value) = value {
        Set(value)
    } else {
        NotSet
    }
}

#[macro_export]
macro_rules! check_ownership_none_is_ok {
    ($db: ident, $user: ident, $object_id: expr, $entity: ident) => {{
        let user_id = $user.id.ok_or(anyhow!("Users should have an id"))?;
        // Check permissions
        if let Some(id) = $object_id {
            let previous = $entity::Entity::find_by_id(id).one($db).await?;
            if let Some(previous) = previous {
                if user_id == previous.owner_id {
                    anyhow::Ok(())
                } else {
                    bail!("You cannot modify the object of someone else")
                }
            } else {
                bail!("Object does not exist")
            }
        } else {
            // no id => new object
            anyhow::Ok(())
        }
    }};
}

#[macro_export]
macro_rules! divide_round_up {
    ($a:expr, $b:expr) => {
        $a / $b + if ($a % $b) == 0 { 0 } else { 1 }
    };
}

#[macro_export]
macro_rules! divide_round_down {
    ($a:expr, $b:expr) => {
        $a / $b
    };
}

#[cfg(test)]
mod test {
    #[test]
    fn test_divide() {
        // Round up
        assert_eq!(divide_round_up!(14, 2), 7);
        assert_eq!(divide_round_up!(13, 2), 7);
        assert_eq!(divide_round_up!(12, 2), 6);

        // Round down
        assert_eq!(divide_round_down!(14, 2), 7);
        assert_eq!(divide_round_down!(13, 2), 6);
        assert_eq!(divide_round_down!(12, 2), 6);
    }
}
