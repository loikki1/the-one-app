use anyhow::{anyhow, bail, Result};
use enums::calling::CallingEnum;
use enums::character_type::CharacterType;
use enums::culture::CultureEnum;
use roll::CharacterRoll;
use sea_orm::{
    ActiveModelTrait as _, ActiveValue::Set, DatabaseConnection, EntityTrait as _,
    TransactionTrait as _,
};
use sea_orm::{ColumnTrait as _, QueryFilter as _};
use serde::{Deserialize, Serialize};
use skill_type::SkillType;
use std::cmp::max;
use std::fs::File;
use std::path::Path;
use ts_rs::TS;
use virtue::VirtueEnum;

pub mod advantage;
pub mod armor;
pub mod attribute;
pub mod attribute_field;
pub mod characteristics;
pub mod combat_skills;
pub mod equipment;
pub mod experience;
pub mod injury;
pub mod items;
pub mod reward;
pub mod roll;
pub mod secondary_attributes;
pub mod shadow;
pub mod shield;
pub mod skill_type;
pub mod skills;
pub mod treasure;
pub mod virtue;
pub mod weapon;

use self::characteristics::Characteristics;
use self::injury::Injury;
use self::items::Items;
use crate::check_ownership_none_is_ok;
use crate::new_character::calling::Calling;
use crate::new_character::{choices::Choices, culture::Culture};
use crate::roll::{Roll, RollType};
use crate::users::User;
use crate::{entity::character as character_entity, utils::convert_option_to_set};

#[derive(Default, Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
#[ts(rename = "CharacterDerived")]
pub struct Derived {
    pub total_load_fatigue: u32,
    pub total_parry: u32,
}

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct Character {
    id: Option<i32>,
    #[serde(alias = "character_name")]
    name: String,
    player_name: String,
    age: u32,
    culture: CultureEnum,
    calling: CallingEnum,
    distinctive_features: Vec<String>,
    flaws: Vec<String>,
    pub items: Items,
    pub characteristics: Characteristics,
    injury: Injury,
    notes: String,
    #[serde(default)]
    one_shot_rules: bool,
    #[serde(default)]
    strider_mode: bool,
    #[serde(skip_deserializing)]
    derived: Derived,
    char_type: CharacterType,
}

impl Character {
    pub fn new(choices: &Choices, player_name: String) -> Result<Self> {
        if choices.distinctive_features.len() != 2 {
            bail!("You need to pick 2 distinctive features");
        }

        let charac = Characteristics::new(choices)?;

        // Get all items
        let mut items = choices.items.clone();
        let treasure = Culture::new(choices.culture.clone()).treasures;
        items.treasure.set_treasure(treasure);
        // Create the distinctive features
        let mut distinctive_features = choices.distinctive_features.clone();
        let calling_feature = Calling::new(choices.calling.clone()).distinctive_feature;
        distinctive_features.push(calling_feature);

        if choices.strider_mode {
            distinctive_features.push(String::from("Strider"));
        }

        let mut out = Self {
            id: None,
            name: choices.character_name.clone(),
            player_name,
            age: 0,
            culture: choices.culture.clone(),
            calling: choices.calling.clone(),
            distinctive_features,
            flaws: vec![],
            items,
            characteristics: charac,
            injury: Injury::default(),
            notes: String::new(),
            derived: Derived::default(),
            one_shot_rules: choices.one_shot_rules,
            strider_mode: choices.strider_mode,
            char_type: CharacterType::Internal,
        };

        out.compute_derived_values();
        Ok(out)
    }

    pub fn set_player_name(&mut self, name: String) {
        self.player_name = name;
    }

    pub fn get_character_type(&self) -> CharacterType {
        self.char_type
    }

    pub async fn save_external(&mut self, db: &DatabaseConnection, user: &User) -> Result<()> {
        self.char_type = CharacterType::External;

        // Check if already added
        let char_ids = Self::get_list_characters(db, user).await?;
        let external = character_entity::Entity::find()
            .filter(character_entity::Column::Id.is_in(char_ids))
            .all(db)
            .await?;

        let matching: Vec<_> = external.iter().filter(|x| self.name == x.name).collect();
        if !matching.is_empty() {
            if matching.len() > 1 {
                eprintln!("Same character has been saved multiple times {self:?} {matching:?}");
            }
            self.id = Some(matching[0].id);
            return Ok(());
        }

        self.id = None;
        self.id = Some(self.save_to_db(db, user).await?);

        Ok(())
    }

    pub fn compute_derived_values(&mut self) {
        let virtues = self.characteristics.get_virtue_enums();
        let load = self.items.get_load(&virtues);
        let fatigue = self.characteristics.attributes.get_fatigue();

        self.characteristics
            .compute_derived_values(self.one_shot_rules || self.strider_mode);
        self.items.compute_derived_values(&virtues);

        self.derived = Derived {
            total_load_fatigue: load + fatigue,
            total_parry: self.items.get_parry() + self.characteristics.attributes.get_parry(),
        };
    }

    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    pub fn load(filename: &Path) -> Result<Character> {
        let file = File::open(filename)?;
        let mut character: Self = serde_json::from_reader(file)?;
        character.compute_derived_values();
        Ok(character)
    }

    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    pub fn reset_ids(&mut self) {
        self.characteristics.valor.reset_ids();
        self.characteristics.wisdom.reset_ids();
        self.items.reset_ids();
    }

    pub async fn from_db_model(
        db: &DatabaseConnection,
        value: &character_entity::Model,
    ) -> Result<Self> {
        let characteristics = Characteristics::from_db_model(db, value).await?;
        let items = Items::from_db_model(db, value).await?;
        let culture =
            serde_json::from_str(&value.culture).expect("SQL and serde enums are not compatible");
        let calling =
            serde_json::from_str(&value.calling).expect("SQL and serde enums are not compatible");

        let mut out = Self {
            id: Some(value.id),
            name: value.name.clone(),
            player_name: value.player_name.clone(),
            age: value.age.try_into()?,
            items,
            culture,
            calling,
            distinctive_features: value
                .distinctive_features
                .split(',')
                .map(String::from)
                .collect(),
            flaws: value.flaws.split(',').map(String::from).collect(),
            characteristics,
            injury: Injury::new(
                value.injury_duration.try_into()?,
                value.injury_description.clone(),
            ),
            notes: value.notes.clone(),
            one_shot_rules: value.one_shot_rules,
            strider_mode: value.strider_mode,
            derived: Derived::default(),
            char_type: CharacterType::Internal,
        };
        out.compute_derived_values();
        Ok(out)
    }

    pub async fn get_owner_id(db: &DatabaseConnection, character_id: i32) -> Result<i32> {
        Ok(Self::get_model_without_ownership_check(db, character_id)
            .await?
            .owner_id)
    }

    async fn get_model_without_ownership_check(
        db: &DatabaseConnection,
        character_id: i32,
    ) -> Result<character_entity::Model> {
        character_entity::Entity::find_by_id(character_id)
            .one(db)
            .await?
            .ok_or(anyhow!("Cannot find requested character"))
    }

    pub async fn get(db: &DatabaseConnection, user: &User, character_id: i32) -> Result<Self> {
        let character = Self::get_model_without_ownership_check(db, character_id).await?;
        if character.owner_id == user.id.expect("User should have an id") {
            Ok(Self::from_db_model(db, &character).await?)
        } else {
            bail!("You cannot get the character of someone else")
        }
    }

    pub async fn get_list_characters(db: &DatabaseConnection, user: &User) -> Result<Vec<i32>> {
        let user_id = user.id.ok_or(anyhow!("Users should have an id"))?;
        let chars = character_entity::Entity::find()
            .filter(character_entity::Column::OwnerId.eq(user_id))
            .all(db)
            .await?;
        Ok(chars.iter().map(|x| x.id).collect())
    }

    pub async fn delete_from_db(
        db: &DatabaseConnection,
        character_id: i32,
        user: &User,
    ) -> Result<()> {
        check_ownership_none_is_ok!(db, user, Some(character_id), character_entity)?;

        // Start a transaction
        let transaction = db.begin().await?;

        Characteristics::delete_by_character_id(&transaction, character_id).await?;
        Items::delete_by_character_id(&transaction, character_id).await?;

        character_entity::Entity::delete_by_id(character_id)
            .exec(&transaction)
            .await?;

        transaction.commit().await?;
        Ok(())
    }

    pub fn get_id(&self) -> Option<i32> {
        self.id
    }

    pub fn set_id(&mut self, id: i32) {
        self.id = Some(id);
    }

    pub async fn find_and_convert_to_external(
        &mut self,
        db: &DatabaseConnection,
        loremaster: &User,
    ) -> Result<()> {
        let chars = character_entity::Entity::find()
            .filter(character_entity::Column::OwnerId.eq(loremaster.id))
            .filter(character_entity::Column::Name.eq(self.name.clone()))
            .filter(character_entity::Column::CharType.eq("\"External\""))
            .all(db)
            .await?;

        if chars.len() != 1 {
            eprintln!("{:?} - {}", loremaster.id, self.name);
            bail!(
                "Failed to identify external character. Found {} matches",
                chars.len()
            );
        }

        self.id = Some(chars[0].id);
        self.char_type = serde_json::from_str(&chars[0].char_type)?;

        Ok(())
    }

    pub async fn save_to_db(&self, db: &DatabaseConnection, user: &User) -> Result<i32> {
        let user_id = user.id.ok_or(anyhow!("Users should have an id"))?;
        check_ownership_none_is_ok!(db, user, self.id, character_entity)?;

        // Start a transaction
        let transaction = db.begin().await?;

        // Character
        let player_name = if self.player_name.is_empty() {
            user.get_username()
        } else {
            &self.player_name
        };
        let id = convert_option_to_set(self.id);
        let attributes = &self.characteristics.attributes;
        let entity = character_entity::ActiveModel {
            id,
            name: Set(self.name.clone()),
            player_name: Set(player_name.clone()),
            age: Set(self.age.try_into()?),
            culture: Set(
                serde_json::to_string(&self.culture).expect("Failed to convert enum to string")
            ),
            calling: Set(
                serde_json::to_string(&self.calling).expect("Failed to convert enum to string")
            ),
            distinctive_features: Set(self.distinctive_features.join(",")),
            flaws: Set(self.flaws.join(",")),
            items_treasure_total: Set(self.items.treasure.get_total().try_into()?),
            items_treasure_carrying: Set(self.items.treasure.get_carrying().try_into()?),
            characteristics_skills: Set(self.characteristics.skills.get_skills_bytes()),
            characteristics_combat_skills: Set(self
                .characteristics
                .combat_skills
                .get_skills_bytes()),
            characteristics_attributes_strength: Set(attributes.strength.get_value().try_into()?),
            characteristics_attributes_heart: Set(attributes.heart.get_value().try_into()?),
            characteristics_attributes_wits: Set(attributes.wits.get_value().try_into()?),
            characteristics_attributes_endurance_current: Set(attributes
                .endurance
                .get_current()
                .try_into()?),
            characteristics_attributes_endurance_max: Set(attributes
                .endurance
                .get_max()
                .try_into()?),
            characteristics_attributes_hope_current: Set(attributes
                .hope
                .get_current()
                .try_into()?),
            characteristics_attributes_hope_max: Set(attributes.hope.get_max().try_into()?),
            characteristics_attributes_parry: Set(attributes.get_parry().try_into()?),
            characteristics_attributes_shadow_current: Set(attributes
                .shadow
                .get_current()
                .try_into()?),
            characteristics_attributes_shadow_scars: Set(attributes
                .shadow
                .get_scars()
                .try_into()?),
            characteristics_attributes_shadow_path: Set(attributes.shadow.get_path().to_owned()),
            characteristics_attributes_fatigue: Set(attributes.get_fatigue().try_into()?),
            characteristics_adventure_experience_current: Set(self
                .characteristics
                .adventure_experience
                .get_current()
                .try_into()?),
            characteristics_adventure_experience_total: Set(self
                .characteristics
                .adventure_experience
                .get_total()
                .try_into()?),
            characteristics_skill_experience_current: Set(self
                .characteristics
                .skill_experience
                .get_current()
                .try_into()?),
            characteristics_skill_experience_total: Set(self
                .characteristics
                .skill_experience
                .get_total()
                .try_into()?),
            characteristics_valor: Set(self.characteristics.valor.get_value().try_into()?),
            characteristics_wisdom: Set(self.characteristics.wisdom.get_value().try_into()?),
            injury_duration: Set(self.injury.get_duration().try_into()?),
            injury_description: Set(self.injury.get_description().to_owned()),
            notes: Set(self.notes.clone()),
            one_shot_rules: Set(self.one_shot_rules),
            strider_mode: Set(self.strider_mode),
            owner_id: Set(user_id),
            char_type: Set(
                serde_json::to_string(&self.char_type).expect("Failed to convert enum to string")
            ),
        };
        let entity = entity.save(&transaction).await?;

        let character_id = entity.id.unwrap();

        self.characteristics
            .save(&transaction, character_id)
            .await?;
        self.items.save(&transaction, character_id).await?;

        // Close the transaction
        transaction.commit().await?;
        Ok(character_id)
    }

    pub fn set_notes(&mut self, notes: &str) {
        notes.clone_into(&mut self.notes);
    }

    pub fn get_notes(&self) -> &String {
        &self.notes
    }

    pub fn rest(&mut self, long: bool) {
        let virtues = self.characteristics.get_virtue_enums();
        let long = if virtues.contains(&VirtueEnum::ElvishDreams) {
            true
        } else {
            long
        };

        self.characteristics
            .attributes
            .rest(long, self.injury.is_wounded(), &virtues);

        self.injury.rest(long);
    }

    pub fn take_fellowship_phase(&mut self, yule: bool) {
        self.characteristics.take_fellowship_phase(yule);
    }

    pub fn is_weary(&self) -> bool {
        self.characteristics.attributes.endurance.get_current() <= self.derived.total_load_fatigue
    }

    pub fn is_miserable(&self) -> bool {
        self.characteristics.attributes.hope.get_current()
            <= self.characteristics.attributes.shadow.get_total_shadow()
    }

    pub fn get_skill(&self, skill: &SkillType) -> u32 {
        match skill {
            SkillType::Combat(skill) => self.characteristics.combat_skills.get_skill(skill),
            SkillType::Skill(skill) => self.characteristics.skills.get_skill(skill),
            SkillType::SecondaryAttribute(skill) => {
                self.characteristics.get_secondary_attribute(skill)
            }
            SkillType::Protection => self.items.get_protection(),
        }
    }

    pub fn roll_type(&self, skill: &SkillType) -> RollType {
        if self.characteristics.attributes.shadow.get_total_shadow()
            == self.characteristics.attributes.hope.get_max()
        {
            return RollType::IllFavoured;
        }
        match skill {
            SkillType::Protection | SkillType::SecondaryAttribute(_) | SkillType::Combat(_) => {
                RollType::Normal
            }
            SkillType::Skill(x) => {
                if self.characteristics.skills.is_favoured(x) {
                    RollType::Favoured
                } else {
                    RollType::Normal
                }
            }
        }
    }

    pub fn roll(
        &self,
        skill: &SkillType,
        bonus_dices: i32,
        roll_type: Option<RollType>,
    ) -> CharacterRoll {
        #[expect(clippy::cast_sign_loss)]
        let success_dices = max((self.get_skill(skill) as i32) + bonus_dices, 0) as u32;
        let roll_type = if let Some(roll_type) = roll_type {
            roll_type
        } else {
            self.roll_type(skill)
        };

        let roll = Roll::roll_dices(
            roll_type,
            success_dices,
            self.is_weary(),
            self.is_miserable(),
        );

        let target = self
            .characteristics
            .get_target_number(skill, self.one_shot_rules);

        CharacterRoll::new(roll, target)
    }

    pub fn get_culture(&self) -> &CultureEnum {
        &self.culture
    }

    pub fn get_starting_eye_rating_without_culture(&self) -> u32 {
        // TODO add legendary weapon / armor / object
        u32::from(self.characteristics.valor.get_value() >= 4)
    }

    pub fn get_initial_fellowship_points(&self) -> u32 {
        let mut out = 1;
        let virtues = self.characteristics.get_virtue_enums();
        if virtues.contains(&VirtueEnum::ThreeIsCompany) {
            out += 1;
        }
        if virtues.contains(&VirtueEnum::BreeBlood) {
            out += 1;
        }
        if virtues.contains(&VirtueEnum::TwiceBakedHoneyCakes) {
            out += 1;
        }
        out
    }
}
