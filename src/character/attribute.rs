use serde::{Deserialize, Serialize};
use std::cmp::max;
use ts_rs::TS;

use super::attribute_field::AttributeField;
use super::shadow::Shadow;
use super::virtue::VirtueEnum;
use crate::common::state_cap::StateCap;
use crate::divide_round_up;
use crate::entity::character as character_entity;
use crate::new_character::calling::Calling;
use crate::new_character::{choices::Choices, culture::Culture};

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct Attributes {
    pub strength: AttributeField,
    pub heart: AttributeField,
    pub wits: AttributeField,
    pub endurance: StateCap,
    pub hope: StateCap,
    parry: u32,
    pub shadow: Shadow,
    fatigue: u32,
}

impl Attributes {
    pub fn new(choices: &Choices) -> Self {
        let culture = Culture::new(choices.culture.clone());
        let calling = Calling::new(choices.calling.clone());
        let attr = culture.attributes[choices.attributes];
        let strength = attr[0];
        let heart = attr[1];
        let wits = attr[2];
        let der = culture.derived_stats;

        Self {
            strength: AttributeField::new(strength),
            heart: AttributeField::new(heart),
            wits: AttributeField::new(wits),
            endurance: StateCap::new(der.endurance + strength),
            hope: StateCap::new(der.hope + heart),
            parry: der.parry + wits,
            shadow: Shadow::new(&calling.shadow_path),
            fatigue: 0,
        }
    }

    pub fn from_db_model(value: &character_entity::Model) -> Self {
        let mut endurance = StateCap::new(
            value
                .characteristics_attributes_endurance_max
                .try_into()
                .expect("Failed to convert"),
        );
        endurance.set_current(
            value
                .characteristics_attributes_endurance_current
                .try_into()
                .expect("Failed to convert"),
        );

        let mut hope = StateCap::new(
            value
                .characteristics_attributes_hope_max
                .try_into()
                .expect("Failed to convert"),
        );
        hope.set_current(
            value
                .characteristics_attributes_hope_current
                .try_into()
                .expect("Failed to convert"),
        );

        Self {
            strength: AttributeField::new(
                value
                    .characteristics_attributes_strength
                    .try_into()
                    .expect("Failed to convert"),
            ),
            heart: AttributeField::new(
                value
                    .characteristics_attributes_heart
                    .try_into()
                    .expect("Failed to convert"),
            ),
            wits: AttributeField::new(
                value
                    .characteristics_attributes_wits
                    .try_into()
                    .expect("Failed to convert"),
            ),
            endurance,
            hope,
            parry: value
                .characteristics_attributes_parry
                .try_into()
                .expect("Failed to convert"),
            shadow: Shadow::from_db_model(value),
            fatigue: value
                .characteristics_attributes_fatigue
                .try_into()
                .expect("Failed to convert"),
        }
    }

    pub fn compute_derived_values(&mut self, one_shot_rules_or_strider: bool) {
        self.strength
            .compute_derived_values(one_shot_rules_or_strider);
        self.heart.compute_derived_values(one_shot_rules_or_strider);
        self.wits.compute_derived_values(one_shot_rules_or_strider);
        self.shadow.compute_derived_values();
    }

    pub fn rest(&mut self, long: bool, injured: bool, virtues: &[VirtueEnum]) {
        // Hope
        if long && self.hope.get_current() == 0 {
            self.hope.loose(-1);
        }

        // Endurance
        if long && injured || !long && !injured {
            let mut increase = self.strength.get_value() as i32;
            if virtues.contains(&VirtueEnum::ToughAsOldTreeRoots) {
                increase *= 2;
            };

            self.endurance.loose(-increase);
        } else if long {
            self.endurance.reset();
        }
    }

    pub fn take_fellowship_phase(&mut self, yule: bool, virtues: &[VirtueEnum]) {
        self.rest(true, false, virtues);
        let mut increase = if yule {
            self.hope.get_max() as i32
        } else {
            self.heart.get_value() as i32
        };

        if virtues.contains(&VirtueEnum::AllegianceOfTheDunedain) {
            let limit: i32 = divide_round_up!(self.heart.get_value() as i32, 2);
            increase = max(increase, limit);
        }
        self.hope.loose(-increase);
    }

    pub fn get_fatigue(&self) -> u32 {
        self.fatigue
    }

    pub fn get_parry(&self) -> u32 {
        self.parry
    }
}
