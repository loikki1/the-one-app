use anyhow::{bail, Result};
use sea_orm::ActiveValue::NotSet;
use sea_orm::{
    ActiveModelTrait as _, ColumnTrait as _, ConnectionTrait, EntityTrait as _, ModelTrait as _,
    QueryFilter as _, Set,
};
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter};
use ts_rs::TS;

use crate::entity::armor as entity;
use crate::utils::convert_option_to_set;
use crate::{assets::read_enum_from_assets, divide_round_up};

use super::{equipment::EquipmentTrait, virtue::VirtueEnum};

const DIRECTORY: &str = "equipments/armors";

#[derive(Serialize, Deserialize, EnumIter, EnumCount, Debug, TS)]
#[ts(export)]
pub enum ArmorEnum {
    CoatOfMail,
    Helm,
    LeatherCorslet,
    LeatherShirt,
    MailShirt,
}

#[derive(Serialize, Deserialize, Clone, Default, TS, Debug)]
#[ts(export)]
pub struct Armor {
    id: Option<i32>,
    name: String,
    protection: u32,
    load: u32,
    #[serde(default)]
    wearing: bool,
    #[serde(default)]
    notes: String,
}

impl Armor {
    pub fn new(name: String, protection: u32, load: u32) -> Self {
        Armor {
            id: None,
            name,
            protection,
            load,
            wearing: true,
            notes: String::new(),
        }
    }

    pub fn get_protection(&self) -> u32 {
        self.protection
    }
    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    pub fn reset_id(&mut self) {
        self.id = None;
    }
}

impl From<ArmorEnum> for Armor {
    fn from(value: ArmorEnum) -> Self {
        read_enum_from_assets(&value, DIRECTORY)
    }
}

impl From<&str> for Armor {
    fn from(value: &str) -> Self {
        let value = format!("\"{value}\"");
        let value: ArmorEnum =
            serde_json::from_str(&value).expect("Failed to convert to armor enum");
        read_enum_from_assets(&value, DIRECTORY)
    }
}

impl EquipmentTrait for Armor {
    fn get_id(&self) -> Option<i32> {
        self.id
    }
    fn get_load(&self, virtues: &[VirtueEnum]) -> u32 {
        if virtues.contains(&VirtueEnum::Redoubtable) {
            divide_round_up!(self.load, 2)
        } else {
            self.load
        }
    }
    fn is_wearing(&self) -> bool {
        self.wearing
    }
    fn set_wearing(&mut self, wearing: bool) {
        self.wearing = wearing;
    }
    async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        let mut model: entity::ActiveModel = (*self).clone().into();
        model.character_id = Set(character_id);
        model.save(db).await?;
        Ok(())
    }

    async fn from_db_model<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<Vec<Self>> {
        Ok(entity::Entity::find()
            .filter(entity::Column::CharacterId.eq(character_id))
            .all(db)
            .await?
            .iter()
            .map(|x| (*x).clone().into())
            .collect())
    }

    async fn delete_by_character_id<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<()> {
        entity::Entity::delete_many()
            .filter(entity::Column::CharacterId.eq(character_id))
            .exec(db)
            .await?;
        Ok(())
    }

    async fn delete_by_id<C: ConnectionTrait>(db: &C, character_id: i32, id: i32) -> Result<()> {
        let armor = entity::Entity::find_by_id(id).one(db).await?;

        if let Some(armor) = armor {
            if armor.character_id == character_id {
                armor.delete(db).await?;
                Ok(())
            } else {
                bail!("Cannot delete armor from someone else")
            }
        } else {
            bail!("Armor not found")
        }
    }
}

impl From<entity::Model> for Armor {
    fn from(value: entity::Model) -> Self {
        Self {
            id: Some(value.id),
            name: value.name,
            protection: value.protection.try_into().expect("Failed to convert"),
            load: value.load.try_into().expect("Failed to convert"),
            wearing: value.wearing,
            notes: value.notes,
        }
    }
}

impl From<Armor> for entity::ActiveModel {
    fn from(value: Armor) -> Self {
        let id = convert_option_to_set(value.id);
        Self {
            id,
            name: Set(value.name),
            protection: Set(value.protection.try_into().expect("Failed to convert")),
            load: Set(value.load.try_into().expect("Failed to convert")),
            wearing: Set(value.wearing),
            notes: Set(value.notes),
            character_id: NotSet,
        }
    }
}

#[cfg(test)]
mod test {
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{
        assets::get_number_files,
        character::armor::{Armor, DIRECTORY},
    };

    use super::ArmorEnum;

    #[test]
    fn check_armor_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(ArmorEnum::COUNT == number_files);

        for armor in ArmorEnum::iter() {
            eprintln!("Testing {armor:?}");
            let _: Armor = Armor::from(armor);
        }
    }
}
