use std::collections::HashMap;

use anyhow::Result;
use rocket::request::FromParam;
use sea_orm::ConnectionTrait;
use sea_orm::DatabaseConnection;
use serde::{Deserialize, Serialize};
use strum::EnumIter;
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

use super::advantage::AdvantageTrait;
use super::reward::Reward;
use super::virtue::Virtue;

#[derive(Serialize, Deserialize, Eq, PartialEq, Clone, FromParamEnum, EnumIter, TS)]
pub enum SecondaryAttributeEnum {
    Valor,
    Wisdom,
}

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub enum SecondaryAttributeAdvantage {
    Virtue(Virtue),
    Reward(Reward),
}

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
pub struct SecondaryAttribute<Advantage> {
    value: u32,
    // TODO: Hashmap might be faster (or use some sorting)
    advantages: Vec<Advantage>,
}

impl<E, T> Default for SecondaryAttribute<T>
where
    E: PartialEq + for<'sa> Deserialize<'sa> + TS,
    T: AdvantageTrait<Enum = E> + Clone + Serialize + for<'sa> Deserialize<'sa> + Ord,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<E, T> SecondaryAttribute<T>
where
    E: PartialEq + for<'sa> Deserialize<'sa> + TS,
    T: AdvantageTrait<Enum = E> + Clone + Serialize + for<'sa> Deserialize<'sa>,
{
    pub fn new() -> Self {
        Self {
            value: 1,
            advantages: vec![],
        }
    }

    pub async fn from_db_model(
        db: &DatabaseConnection,
        value: u32,
        character_id: i32,
    ) -> Result<Self> {
        let advantages = T::from_db_model(db, character_id).await?;
        Ok(Self { value, advantages })
    }

    pub fn get_value(&self) -> u32 {
        self.value
    }

    pub fn set_value(&mut self, value: u32) {
        self.value = value;
    }

    pub fn add_advantage(&mut self, adv: &T) {
        self.advantages.push(adv.clone());
    }

    pub fn get_advantages(&self) -> &[T] {
        &self.advantages
    }

    pub async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        let previous = T::from_db_model(db, character_id).await?;

        // Convert to a map
        let mut in_db = HashMap::<i32, T>::new();
        for x in &previous {
            in_db.insert(x.get_id().expect("Should have an id"), x.clone());
        }

        // Add new advantages
        for adv in &self.advantages {
            let id = adv.get_id();
            if let Some(id) = id {
                if in_db.contains_key(&id) {
                    in_db.remove(&id).unwrap();
                };
            }
            adv.save(db, character_id).await?;
        }

        // Remove old advantages
        for id in in_db.keys() {
            T::delete_by_id(db, character_id, *id).await?;
        }

        Ok(())
    }

    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    pub fn reset_ids(&mut self) {
        for adv in &mut self.advantages {
            adv.reset_id();
        }
    }
}
