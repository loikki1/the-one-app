use std::collections::HashMap;

use anyhow::{anyhow, bail, Result};
use sea_orm::{ConnectionTrait, DatabaseConnection};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

use super::armor::Armor;
use super::equipment::{Equipment, EquipmentTrait, EquipmentType};
use super::shield::Shield;
use super::treasure::Treasure;
use super::virtue::VirtueEnum;
use super::weapon::Weapon;
use crate::entity::character as character_entity;

#[derive(Default, Serialize, Deserialize, Clone, TS, Debug)]
#[ts(export)]
#[ts(rename = "ItemsDerived")]
struct Derived {
    protection: u32,
    parry: u32,
    load: u32,
}

#[derive(Serialize, Deserialize, Clone, TS, Debug)]
#[ts(export)]
pub struct Items {
    weapons: Vec<Weapon>,
    armors: Vec<Armor>,
    shields: Vec<Shield>,
    equipments: Vec<Equipment>,
    pub treasure: Treasure,
    #[serde(skip_deserializing)]
    derived: Derived,
}

impl Default for Items {
    fn default() -> Self {
        Self::new()
    }
}

impl Items {
    pub fn new() -> Self {
        Self {
            weapons: vec![],
            armors: vec![],
            shields: vec![],
            equipments: vec![],
            treasure: Treasure::new(),
            derived: Derived::default(),
        }
    }

    pub async fn from_db_model(
        db: &DatabaseConnection,
        character: &character_entity::Model,
    ) -> Result<Self> {
        let weapons = Weapon::from_db_model(db, character.id).await?;
        let armors = Armor::from_db_model(db, character.id).await?;
        let shields = Shield::from_db_model(db, character.id).await?;
        let equipments = Equipment::from_db_model(db, character.id).await?;
        let mut treasure = Treasure::new();
        treasure.set_treasure(character.items_treasure_total.try_into()?);
        treasure.set_carrying(character.items_treasure_carrying.try_into()?);

        Ok(Self {
            weapons,
            armors,
            shields,
            equipments,
            treasure,
            derived: Derived::default(),
        })
    }

    pub fn compute_derived_values(&mut self, virtues: &[VirtueEnum]) {
        self.derived.protection = self.get_protection();
        self.derived.parry = self.get_parry();
        self.derived.load = self.get_load(virtues);
        self.treasure.compute_derived_values();
    }

    pub fn get_protection(&self) -> u32 {
        self.armors.iter().fold(0, |acc, elem| {
            acc + if elem.is_wearing() {
                elem.get_protection()
            } else {
                0
            }
        })
    }

    pub fn get_parry(&self) -> u32 {
        self.shields.iter().fold(0, |acc, elem| {
            acc + if elem.is_wearing() {
                elem.get_parry()
            } else {
                0
            }
        })
    }

    fn add_item<T: EquipmentTrait + Clone>(items: &mut Vec<T>, item: &T) -> Result<()> {
        if let Some(id) = item.get_id() {
            let index = items
                .iter()
                .position(|x| {
                    if let Some(x_id) = x.get_id() {
                        x_id == id
                    } else {
                        false
                    }
                })
                .ok_or(anyhow!("Failed to find item"))?;
            items[index] = item.clone();
        } else {
            items.push(item.clone());
        }
        Ok(())
    }

    /// Add weapon
    /// Not safe: does not check owner
    pub fn add_weapon(&mut self, weapon: &Weapon) -> Result<()> {
        Self::add_item(&mut self.weapons, weapon)
    }

    /// Add shield
    /// Not safe: does not check owner
    pub fn add_shield(&mut self, shield: &Shield) -> Result<()> {
        Self::add_item(&mut self.shields, shield)
    }

    /// Add armor
    /// Not safe: does not check owner
    pub fn add_armor(&mut self, armor: &Armor) -> Result<()> {
        Self::add_item(&mut self.armors, armor)
    }

    /// Add equipment
    /// Not safe: does not check owner
    pub fn add_equipment(&mut self, equipment: &Equipment) -> Result<()> {
        Self::add_item(&mut self.equipments, equipment)
    }

    /// Not safe: does not check owner
    fn delete_item_intern<T>(vec: &mut Vec<T>, id: i32) -> Result<()>
    where
        T: EquipmentTrait,
    {
        let index = vec.iter().position(|x| {
            if let Some(x_id) = x.get_id() {
                x_id == id
            } else {
                false
            }
        });
        if let Some(index) = index {
            vec.remove(index);
            Ok(())
        } else {
            bail!(format!("Cannot find item {id}"))
        }
    }

    /// Not safe: does not check owner
    pub fn delete_item(&mut self, eq_type: EquipmentType, id: i32) -> Result<()> {
        match eq_type {
            EquipmentType::Armor => Items::delete_item_intern(&mut self.armors, id),
            EquipmentType::Equipment => Items::delete_item_intern(&mut self.equipments, id),
            EquipmentType::Weapon => Items::delete_item_intern(&mut self.weapons, id),
            EquipmentType::Shield => Items::delete_item_intern(&mut self.shields, id),
        }
    }

    /// Update wearing
    /// Not safe: does not check owner
    fn update_wearing_intern<T>(vec: &mut [T], id: i32, value: bool) -> Result<()>
    where
        T: EquipmentTrait,
    {
        let index = vec.iter().position(|x| {
            if let Some(x_id) = x.get_id() {
                x_id == id
            } else {
                false
            }
        });
        if let Some(index) = index {
            vec[index].set_wearing(value);
            Ok(())
        } else {
            bail!(format!("Cannot find item {id}"))
        }
    }

    /// Not safe: does not check owner
    pub fn update_wearing(&mut self, eq_type: EquipmentType, id: i32, value: bool) -> Result<()> {
        match eq_type {
            EquipmentType::Armor => Items::update_wearing_intern(&mut self.armors, id, value),
            EquipmentType::Equipment => {
                Items::update_wearing_intern(&mut self.equipments, id, value)
            }
            EquipmentType::Weapon => Items::update_wearing_intern(&mut self.weapons, id, value),
            EquipmentType::Shield => Items::update_wearing_intern(&mut self.shields, id, value),
        }
    }

    fn get_load_items<T>(items: &[T], virtues: &[VirtueEnum]) -> u32
    where
        T: EquipmentTrait,
    {
        items.iter().fold(0, |acc, elem| {
            acc + if elem.is_wearing() {
                elem.get_load(virtues)
            } else {
                0
            }
        })
    }

    pub fn get_load(&self, virtues: &[VirtueEnum]) -> u32 {
        Self::get_load_items(&self.armors, virtues)
            + Self::get_load_items(&self.weapons, virtues)
            + Self::get_load_items(&self.shields, virtues)
            + Self::get_load_items(&self.equipments, virtues)
            + self.treasure.get_load()
    }

    /// Not safe: does not check ownership
    pub async fn delete_by_character_id<C: ConnectionTrait>(
        db: &C,
        character_id: i32,
    ) -> Result<()> {
        Equipment::delete_by_character_id(db, character_id).await?;
        Weapon::delete_by_character_id(db, character_id).await?;
        Shield::delete_by_character_id(db, character_id).await?;
        Armor::delete_by_character_id(db, character_id).await
    }

    /// Not safe: does not check ownership
    pub async fn save_items<C: ConnectionTrait, E: EquipmentTrait + Clone>(
        db: &C,
        character_id: i32,
        items: &[E],
    ) -> Result<()> {
        let previous = E::from_db_model(db, character_id).await?;

        // Convert to a map
        let mut in_db = HashMap::<i32, E>::new();
        for x in &previous {
            in_db.insert(x.get_id().expect("Should have an id"), x.clone());
        }

        // Add new items
        for item in items {
            let id = item.get_id();
            if let Some(id) = id {
                if in_db.contains_key(&id) {
                    in_db.remove(&id).unwrap();
                };
            }
            item.save(db, character_id).await?;
        }

        // Remove old advantages
        for id in in_db.keys() {
            E::delete_by_id(db, character_id, *id).await?;
        }

        Ok(())
    }

    /// Not safe: does not check ownership
    pub async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        Self::save_items(db, character_id, &self.weapons).await?;
        Self::save_items(db, character_id, &self.armors).await?;
        Self::save_items(db, character_id, &self.shields).await?;
        Self::save_items(db, character_id, &self.equipments).await
    }

    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    pub fn reset_ids(&mut self) {
        self.weapons.iter_mut().for_each(Weapon::reset_id);
        self.shields.iter_mut().for_each(Shield::reset_id);
        self.armors.iter_mut().for_each(Armor::reset_id);
        self.equipments.iter_mut().for_each(Equipment::reset_id);
    }
}
