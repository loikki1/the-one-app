use anyhow::{bail, Result};
use rocket::request::FromParam;
use sea_orm::{ActiveModelTrait as _, ConnectionTrait, ModelTrait as _};
use sea_orm::{ActiveValue::NotSet, ColumnTrait as _, EntityTrait as _, QueryFilter as _, Set};
use serde::{Deserialize, Serialize};
use std::future::Future;
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

use crate::entity::equipment as entity;
use crate::utils::convert_option_to_set;

use super::{armor::Armor, shield::Shield, virtue::VirtueEnum, weapon::Weapon};

#[derive(Serialize, Deserialize, Clone, Debug, FromParamEnum, TS)]
pub enum EquipmentType {
    Equipment,
    Armor,
    Weapon,
    Shield,
}

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub enum EquipmentObject {
    Equipment(Equipment),
    Armor(Armor),
    Weapon(Weapon),
    Shield(Shield),
}

pub trait EquipmentTrait: Sized {
    fn get_id(&self) -> Option<i32>;
    fn get_load(&self, virtues: &[VirtueEnum]) -> u32;
    fn is_wearing(&self) -> bool;
    fn set_wearing(&mut self, wearing: bool);
    fn save<C: ConnectionTrait>(
        &self,
        db: &C,
        character_id: i32,
    ) -> impl Future<Output = Result<()>> + Send;
    fn delete_by_character_id<C: ConnectionTrait>(
        db: &C,
        character_id: i32,
    ) -> impl Future<Output = Result<()>> + Send;
    fn delete_by_id<C: ConnectionTrait>(
        db: &C,
        character_id: i32,
        id: i32,
    ) -> impl Future<Output = Result<()>> + Send;
    fn from_db_model<C: ConnectionTrait>(
        db: &C,
        character_id: i32,
    ) -> impl Future<Output = Result<Vec<Self>>> + Send;
}

#[derive(Serialize, Deserialize, Clone, TS, Debug)]
#[ts(export)]
pub struct Equipment {
    id: Option<i32>,
    name: String,
    load: u32,
    wearing: bool,
    notes: String,
}

impl Equipment {
    pub fn new(name: String, load: u32) -> Self {
        Self {
            id: None,
            name,
            load,
            wearing: true,
            notes: String::new(),
        }
    }
    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    pub fn reset_id(&mut self) {
        self.id = None;
    }
}

impl From<entity::Model> for Equipment {
    fn from(value: entity::Model) -> Self {
        Self {
            id: Some(value.id),
            name: value.name,
            load: value.load.try_into().expect("Failed to convert"),
            wearing: value.wearing,
            notes: value.notes,
        }
    }
}

impl From<Equipment> for entity::ActiveModel {
    fn from(value: Equipment) -> Self {
        let id = convert_option_to_set(value.id);
        Self {
            id,
            name: Set(value.name),
            load: Set(value.load.try_into().expect("Failed to convert")),
            wearing: Set(value.wearing),
            notes: Set(value.notes),
            character_id: NotSet,
        }
    }
}

impl EquipmentTrait for Equipment {
    fn get_id(&self) -> Option<i32> {
        self.id
    }
    fn get_load(&self, _: &[VirtueEnum]) -> u32 {
        self.load
    }
    fn is_wearing(&self) -> bool {
        self.wearing
    }
    fn set_wearing(&mut self, wearing: bool) {
        self.wearing = wearing;
    }

    async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        let mut model: entity::ActiveModel = (*self).clone().into();
        model.character_id = Set(character_id);
        model.save(db).await?;
        Ok(())
    }

    async fn from_db_model<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<Vec<Self>> {
        Ok(entity::Entity::find()
            .filter(entity::Column::CharacterId.eq(character_id))
            .all(db)
            .await?
            .iter()
            .map(|x| (*x).clone().into())
            .collect())
    }

    async fn delete_by_character_id<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<()> {
        entity::Entity::delete_many()
            .filter(entity::Column::CharacterId.eq(character_id))
            .exec(db)
            .await?;
        Ok(())
    }

    async fn delete_by_id<C: ConnectionTrait>(db: &C, character_id: i32, id: i32) -> Result<()> {
        let eq = entity::Entity::find_by_id(id).one(db).await?;

        if let Some(eq) = eq {
            if eq.character_id == character_id {
                eq.delete(db).await?;
                Ok(())
            } else {
                bail!("Cannot delete equipment from someone else")
            }
        } else {
            bail!("Equipment not found")
        }
    }
}
