use anyhow::{bail, Result};
use enums::combat_proficiency::CombatProficiency;
use sea_orm::ActiveValue::NotSet;
use sea_orm::{
    ActiveModelTrait as _, ColumnTrait as _, ConnectionTrait, EntityTrait as _, ModelTrait as _,
    QueryFilter as _, Set,
};
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter};
use ts_rs::TS;

use crate::assets::read_enum_from_assets;
use crate::entity::weapon as entity;
use crate::utils::convert_option_to_set;

use super::equipment::EquipmentTrait;
use super::virtue::VirtueEnum;

const DIRECTORY: &str = "equipments/weapons";

#[derive(Serialize, Deserialize, EnumIter, EnumCount, Debug, TS)]
#[ts(export)]
pub enum WeaponEnum {
    Axe,
    Bow,
    Cudgel,
    Club,
    Dagger,
    GreatAxe,
    GreatBow,
    GreatSpear,
    LongHaftedAxe,
    LongSword,
    Mattock,
    ShortSpear,
    ShortSword,
    Spear,
    Sword,
    Unarmed,
}

#[derive(Serialize, Deserialize, Clone, Default, TS, Debug)]
#[ts(export)]
pub struct Weapon {
    id: Option<i32>,
    name: String,
    #[serde(alias = "one_hand_damage")]
    damage: u32,
    injury: u32,
    two_hand_injury: Option<u32>,
    load: u32,
    #[serde(default)]
    combat_proficiency: CombatProficiency,
    #[serde(default)]
    wearing: bool,
    #[serde(default)]
    notes: String,
}

impl Weapon {
    pub fn new(
        name: String,
        damage: u32,
        injury: u32,
        two_hand_injury: Option<u32>,
        load: u32,
        combat_proficiency: CombatProficiency,
    ) -> Self {
        Self {
            id: None,
            name,
            damage,
            injury,
            two_hand_injury,
            load,
            combat_proficiency,
            wearing: true,
            notes: String::new(),
        }
    }

    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    pub fn reset_id(&mut self) {
        self.id = None;
    }
}

impl From<WeaponEnum> for Weapon {
    fn from(value: WeaponEnum) -> Self {
        read_enum_from_assets(&value, DIRECTORY)
    }
}

impl From<&str> for Weapon {
    fn from(value: &str) -> Self {
        let value = format!("\"{value}\"");
        let value: WeaponEnum =
            serde_json::from_str(&value).expect("Failed to convert to weapon enum");
        read_enum_from_assets(&value, DIRECTORY)
    }
}

impl EquipmentTrait for Weapon {
    fn get_id(&self) -> Option<i32> {
        self.id
    }
    fn get_load(&self, _: &[VirtueEnum]) -> u32 {
        self.load
    }
    fn is_wearing(&self) -> bool {
        self.wearing
    }
    fn set_wearing(&mut self, wearing: bool) {
        self.wearing = wearing;
    }
    async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        let mut model: entity::ActiveModel = (*self).clone().into();
        model.character_id = Set(character_id);
        model.save(db).await?;
        Ok(())
    }
    async fn delete_by_character_id<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<()> {
        entity::Entity::delete_many()
            .filter(entity::Column::CharacterId.eq(character_id))
            .exec(db)
            .await?;
        Ok(())
    }

    async fn from_db_model<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<Vec<Self>> {
        Ok(entity::Entity::find()
            .filter(entity::Column::CharacterId.eq(character_id))
            .all(db)
            .await?
            .iter()
            .map(|x| (*x).clone().into())
            .collect())
    }

    async fn delete_by_id<C: ConnectionTrait>(db: &C, character_id: i32, id: i32) -> Result<()> {
        let weapon = entity::Entity::find_by_id(id).one(db).await?;

        if let Some(weapon) = weapon {
            if weapon.character_id == character_id {
                weapon.delete(db).await?;
                Ok(())
            } else {
                bail!("Cannot delete weapon from someone else")
            }
        } else {
            bail!("Weapon not found")
        }
    }
}

impl From<entity::Model> for Weapon {
    fn from(value: entity::Model) -> Self {
        let prof = serde_json::from_str(&value.combat_proficiency)
            .expect("SQL and serde enums are not compatible");

        Self {
            id: Some(value.id),
            name: value.name,
            damage: value.damage.try_into().expect("Failed to convert"),
            injury: value.injury.try_into().expect("Failed to convert"),
            two_hand_injury: value
                .two_hand_injury
                .map(|x| x.try_into().expect("Failed to convert")),
            load: value.load.try_into().expect("Failed to convert"),
            combat_proficiency: prof,
            wearing: value.wearing,
            notes: value.notes,
        }
    }
}

impl From<Weapon> for entity::ActiveModel {
    fn from(value: Weapon) -> Self {
        let id = convert_option_to_set(value.id);
        let prof = serde_json::to_string(&value.combat_proficiency)
            .expect("Failed to convert enum to string");
        Self {
            id,
            name: Set(value.name),
            damage: Set(value.damage.try_into().expect("Failed to convert")),
            injury: Set(value.injury.try_into().expect("Failed to convert")),
            two_hand_injury: Set(value
                .two_hand_injury
                .map(|x| x.try_into().expect("Failed to convert"))),
            load: Set(value.load.try_into().expect("Failed to convert")),
            combat_proficiency: Set(prof),
            wearing: Set(value.wearing),
            notes: Set(value.notes),
            character_id: NotSet,
        }
    }
}

#[cfg(test)]
mod test {
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{
        assets::get_number_files,
        character::weapon::{Weapon, DIRECTORY},
    };

    use super::WeaponEnum;

    #[test]
    fn check_weapon_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(WeaponEnum::COUNT == number_files);

        for weapon in WeaponEnum::iter() {
            eprintln!("Testing {weapon:?}");
            let _: Weapon = Weapon::from(weapon);
        }
    }
}
