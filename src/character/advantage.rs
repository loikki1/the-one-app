use anyhow::Result;
use sea_orm::ConnectionTrait;
use std::future::Future;

pub trait AdvantageTrait: Sized + Clone {
    type Enum;
    fn from_enum(value: &Self::Enum) -> Self;
    fn get_enum(&self) -> Option<Self::Enum>;
    fn get_id(&self) -> Option<i32>;
    fn is_available_at_creation(&self) -> bool;
    fn is_available(&self) -> bool {
        true
    }
    fn from_db_model<C: ConnectionTrait>(
        db: &C,
        character_id: i32,
    ) -> impl Future<Output = Result<Vec<Self>>> + Send;
    fn save<C: ConnectionTrait>(
        &self,
        db: &C,
        character_id: i32,
    ) -> impl Future<Output = Result<()>> + Send;

    fn delete_by_character_id<C: ConnectionTrait>(
        db: &C,
        character_id: i32,
    ) -> impl Future<Output = Result<()>> + Send;

    fn delete_by_id<C: ConnectionTrait>(
        db: &C,
        character_id: i32,
        id: i32,
    ) -> impl Future<Output = Result<()>> + Send;

    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    fn reset_id(&mut self);
}
