use serde::{Deserialize, Serialize};
use strum::{EnumIter, IntoEnumIterator as _};
use ts_rs::TS;

#[derive(Default, PartialEq, PartialOrd, Clone, Serialize, Deserialize, Debug, EnumIter, TS)]
#[ts(export)]
pub enum StandardOfLiving {
    #[default]
    Frugal = 0,
    Common = 30,
    Prosperous = 90,
    Rich = 180,
    VeryRich = 300,
}

#[derive(Default, Serialize, Deserialize, Clone, TS, Debug)]
#[ts(export)]
#[ts(rename = "TreasureDerived")]
pub struct Derived {
    standard_of_living: StandardOfLiving,
}

#[derive(Default, Serialize, Deserialize, Clone, TS, Debug)]
#[ts(export)]
pub struct Treasure {
    total: u32,
    carrying: u32,
    #[serde(skip_deserializing)]
    derived: Derived,
}

impl Treasure {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn get_load(&self) -> u32 {
        self.carrying
    }

    pub fn set_treasure(&mut self, treasures: u32) {
        self.total = treasures;
    }

    pub fn set_carrying(&mut self, treasures: u32) {
        self.carrying = treasures;
    }

    pub fn get_carrying(&self) -> u32 {
        self.carrying
    }

    pub fn get_total(&self) -> u32 {
        self.total
    }

    pub fn compute_derived_values(&mut self) {
        self.derived.standard_of_living = self.get_standard_living();
    }

    pub fn get_standard_living(&self) -> StandardOfLiving {
        let mut current = StandardOfLiving::Frugal;
        for standard in StandardOfLiving::iter() {
            if self.total >= standard.clone() as u32 {
                current = standard.clone();
            } else {
                return current;
            }
        }
        current
    }
}
