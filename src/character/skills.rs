use std::collections::HashMap;

use anyhow::{anyhow, bail, Result};
use enums::skill::SkillEnum;
use sea_orm::ActiveValue::NotSet;
use sea_orm::{
    ActiveModelTrait as _, ColumnTrait as _, ConnectionTrait, DatabaseConnection, EntityTrait as _,
    QueryFilter as _, Set,
};
use serde::{Deserialize, Serialize};
use strum::EnumCount as _;
use ts_rs::TS;

use crate::entity::favoured_skill as favoured_skill_entity;
use crate::{new_character::choices::Choices, new_character::culture::Culture};

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct Skills {
    skills: [u32; SkillEnum::COUNT],
    // TODO convert to set
    favoured: Vec<SkillEnum>,
}

impl Skills {
    pub fn new(choices: &Choices) -> Result<Self> {
        // Check number of choices
        if choices.culture_skill.is_none() {
            bail!("You need to specify a culture skill");
        };
        if choices.calling_skills.len() != 2 {
            bail!("You need to pick 2 calling combat skills");
        }

        // Check there is no duplicates
        let culture_skill = choices
            .culture_skill
            .clone()
            .ok_or(anyhow!("Failed to clone skill"))?;
        if choices.calling_skills.iter().any(|x| x == &culture_skill) {
            bail!("You cannot choose twice the same skill");
        };

        // Pick favoured skills
        let mut favoured = choices.calling_skills.clone();
        favoured.push(
            choices
                .culture_skill
                .clone()
                .ok_or(anyhow!("Failed to clone"))?,
        );

        // Update skills value
        let mut skills = Culture::new(choices.culture.clone()).skills;
        for improvement in &choices.improvements.skills {
            let new_value = *improvement.1;
            skills[improvement.0.clone() as usize] = new_value;
        }

        Ok(Self { skills, favoured })
    }

    pub fn get_skills_bytes(&self) -> Vec<u8> {
        self.skills.iter().flat_map(|x| x.to_be_bytes()).collect()
    }

    pub async fn from_db_model(
        db: &DatabaseConnection,
        character_id: i32,
        skills: &[u8],
    ) -> Result<Self> {
        if skills.len() != 4 * SkillEnum::COUNT {
            bail!(format!(
                "This should never happen, number of skills in db does not match the app"
            ));
        };

        let mut skills_array: [u32; SkillEnum::COUNT] = [0; SkillEnum::COUNT];
        for (i, el) in skills_array.iter_mut().enumerate() {
            let slice = &skills[(4 * i)..(4 * (i + 1))];
            *el = u32::from_be_bytes(slice.try_into().unwrap());
        }

        let favoured = favoured_skill_entity::Entity::find()
            .filter(favoured_skill_entity::Column::CharacterId.eq(character_id))
            .all(db)
            .await?;
        let favoured = favoured
            .iter()
            .map(|x| {
                serde_json::from_str(&x.skill).expect("SQL and serde enums are not compatible")
            })
            .collect();

        Ok(Self {
            skills: skills_array,
            favoured,
        })
    }

    pub async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        let previous = favoured_skill_entity::Entity::find()
            .filter(favoured_skill_entity::Column::CharacterId.eq(character_id))
            .all(db)
            .await?;

        // Convert to a map
        let mut in_db = HashMap::<SkillEnum, favoured_skill_entity::Model>::new();
        for x in &previous {
            let skill =
                serde_json::from_str(&x.skill).expect("SQL and serde enums are not compatible");
            in_db.insert(skill, x.clone());
        }

        // Add new skills
        for skill in &self.favoured {
            if in_db.contains_key(skill) {
                in_db.remove(skill).unwrap();
                continue;
            };
            let model = favoured_skill_entity::ActiveModel {
                id: NotSet,
                character_id: Set(character_id),
                skill: Set(serde_json::to_string(&skill).expect("Failed to convert enum to string")),
            };
            model.save(db).await?;
        }

        // Remove old skills
        for val in in_db.values() {
            favoured_skill_entity::Entity::delete_by_id(val.id)
                .exec(db)
                .await?;
        }

        Ok(())
    }

    pub async fn delete_by_character_id<C: ConnectionTrait>(
        db: &C,
        character_id: i32,
    ) -> Result<()> {
        favoured_skill_entity::Entity::delete_many()
            .filter(favoured_skill_entity::Column::CharacterId.eq(character_id))
            .exec(db)
            .await?;
        Ok(())
    }

    pub fn set_skill(&mut self, skill: &SkillEnum, value: u32) {
        self.skills[skill.clone() as usize] = value;
    }

    pub fn get_skill(&self, skill: &SkillEnum) -> u32 {
        self.skills[skill.clone() as usize]
    }

    pub fn is_favoured(&self, skill: &SkillEnum) -> bool {
        self.favoured.contains(skill)
    }
}
