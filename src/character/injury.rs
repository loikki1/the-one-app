use serde::{Deserialize, Serialize};
use ts_rs::TS;

#[derive(Default, Serialize, Deserialize, Clone, TS, Debug)]
#[ts(export)]
pub struct Injury {
    duration: u32,
    description: String,
}

impl Injury {
    pub fn new(duration: u32, description: String) -> Self {
        Self {
            duration,
            description,
        }
    }

    pub fn rest(&mut self, long: bool) {
        if long && self.duration > 0 {
            self.duration -= 1;
        }

        if self.duration == 0 {
            self.description = String::new();
        }
    }

    pub fn is_wounded(&self) -> bool {
        self.duration != 0
    }

    pub fn get_duration(&self) -> u32 {
        self.duration
    }

    pub fn get_description(&self) -> &str {
        &self.description
    }
}
