use serde::{Deserialize, Serialize};
use ts_rs::TS;

use crate::entity::character as character_entity;

#[derive(Default, Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
#[ts(rename = "ShadowDerived")]
pub struct Derived {
    pub total_shadow: u32,
}

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct Shadow {
    current: u32,
    scars: u32,
    path: String,
    #[serde(skip_deserializing)]
    derived: Derived,
}

impl Shadow {
    pub fn new(path: &String) -> Self {
        let mut out = Self {
            current: 0,
            scars: 0,
            path: path.to_string(),
            derived: Derived::default(),
        };
        out.compute_derived_values();
        out
    }

    pub fn from_db_model(value: &character_entity::Model) -> Self {
        Self {
            current: value
                .characteristics_attributes_shadow_current
                .try_into()
                .expect("Failed to convert"),
            scars: value
                .characteristics_attributes_shadow_scars
                .try_into()
                .expect("Failed to convert"),
            path: value.characteristics_attributes_shadow_path.clone(),
            derived: Derived::default(),
        }
    }

    pub fn compute_derived_values(&mut self) {
        self.derived.total_shadow = self.current + self.scars;
    }

    pub fn convert(&mut self) {
        self.current = 0;
        self.scars += 1;
    }

    pub fn get_current(&self) -> u32 {
        self.current
    }

    pub fn get_total_shadow(&self) -> u32 {
        self.current + self.scars
    }

    pub fn get_scars(&self) -> u32 {
        self.scars
    }

    pub fn get_path(&self) -> &str {
        &self.path
    }
}
