use serde::{Deserialize, Serialize};
use ts_rs::TS;

#[derive(Serialize, Deserialize, Default, TS, Debug, Clone)]
#[ts(export)]
#[ts(rename = "AttributeFieldDerived")]
struct Derived {
    target: u32,
}

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct AttributeField {
    value: u32,
    #[serde(skip_deserializing)]
    derived: Derived,
}

impl AttributeField {
    pub fn new(value: u32) -> Self {
        Self {
            value,
            derived: Derived::default(),
        }
    }

    pub fn get_target_number(&self, one_shot_rules: bool) -> u32 {
        if one_shot_rules {
            18 - self.value
        } else {
            20 - self.value
        }
    }

    pub fn get_value(&self) -> u32 {
        self.value
    }

    pub fn compute_derived_values(&mut self, one_shot_rules: bool) {
        self.derived.target = self.get_target_number(one_shot_rules);
    }
}
