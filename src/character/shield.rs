use anyhow::{bail, Result};
use sea_orm::ActiveValue::NotSet;
use sea_orm::{
    ActiveModelTrait as _, ColumnTrait as _, ConnectionTrait, EntityTrait as _, ModelTrait as _,
    QueryFilter as _, Set,
};
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter};
use ts_rs::TS;

use crate::assets::read_enum_from_assets;
use crate::entity::shield as entity;
use crate::utils::convert_option_to_set;

use super::{equipment::EquipmentTrait, virtue::VirtueEnum};

const DIRECTORY: &str = "equipments/shields";

#[derive(Serialize, Deserialize, EnumIter, EnumCount, Debug, TS)]
#[ts(export)]
pub enum ShieldEnum {
    Buckler,
    GreatShield,
    Shield,
}

#[derive(Serialize, Deserialize, Clone, Default, TS, Debug)]
#[ts(export)]
pub struct Shield {
    id: Option<i32>,
    name: String,
    parry: u32,
    load: u32,
    #[serde(default)]
    wearing: bool,
    #[serde(default)]
    notes: String,
}

impl Shield {
    pub fn new(name: String, parry: u32, load: u32) -> Self {
        Self {
            name,
            parry,
            load,
            wearing: true,
            ..Self::default()
        }
    }

    pub fn get_parry(&self) -> u32 {
        self.parry
    }
    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    pub fn reset_id(&mut self) {
        self.id = None;
    }
}

impl From<ShieldEnum> for Shield {
    fn from(value: ShieldEnum) -> Self {
        read_enum_from_assets(&value, DIRECTORY)
    }
}

impl From<&str> for Shield {
    fn from(value: &str) -> Self {
        let value = format!("\"{value}\"");
        let value: ShieldEnum =
            serde_json::from_str(&value).expect("Failed to convert to shield enum");
        read_enum_from_assets(&value, DIRECTORY)
    }
}

impl EquipmentTrait for Shield {
    fn get_id(&self) -> Option<i32> {
        self.id
    }
    fn get_load(&self, _: &[VirtueEnum]) -> u32 {
        self.load
    }
    fn is_wearing(&self) -> bool {
        self.wearing
    }
    fn set_wearing(&mut self, wearing: bool) {
        self.wearing = wearing;
    }

    async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        let mut model: entity::ActiveModel = (*self).clone().into();
        model.character_id = Set(character_id);
        model.save(db).await?;
        Ok(())
    }

    async fn from_db_model<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<Vec<Self>> {
        Ok(entity::Entity::find()
            .filter(entity::Column::CharacterId.eq(character_id))
            .all(db)
            .await?
            .iter()
            .map(|x| (*x).clone().into())
            .collect())
    }

    async fn delete_by_character_id<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<()> {
        entity::Entity::delete_many()
            .filter(entity::Column::CharacterId.eq(character_id))
            .exec(db)
            .await?;
        Ok(())
    }

    async fn delete_by_id<C: ConnectionTrait>(db: &C, character_id: i32, id: i32) -> Result<()> {
        let shield = entity::Entity::find_by_id(id).one(db).await?;

        if let Some(shield) = shield {
            if shield.character_id == character_id {
                shield.delete(db).await?;
                Ok(())
            } else {
                bail!("Cannot delete shield from someone else")
            }
        } else {
            bail!("Shield not found")
        }
    }
}

impl From<entity::Model> for Shield {
    fn from(value: entity::Model) -> Self {
        Self {
            id: Some(value.id),
            name: value.name,
            parry: value.parry.try_into().expect("Failed to convert"),
            load: value.load.try_into().expect("Failed to convert"),
            wearing: value.wearing,
            notes: value.notes,
        }
    }
}

impl From<Shield> for entity::ActiveModel {
    fn from(value: Shield) -> Self {
        let id = convert_option_to_set(value.id);
        Self {
            id,
            name: Set(value.name),
            parry: Set(value.parry.try_into().expect("Failed to convert")),
            load: Set(value.load.try_into().expect("Failed to convert")),
            wearing: Set(value.wearing),
            notes: Set(value.notes),
            character_id: NotSet,
        }
    }
}

#[cfg(test)]
mod test {
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{
        assets::get_number_files,
        character::shield::{Shield, DIRECTORY},
    };

    use super::ShieldEnum;

    #[test]
    fn check_shield_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(ShieldEnum::COUNT == number_files);

        for shield in ShieldEnum::iter() {
            eprintln!("Testing {shield:?}");
            let _: Shield = Shield::from(shield);
        }
    }
}
