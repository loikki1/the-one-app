use crate::new_character::{
    choices::Choices, culture::Culture, previous_experience::PreviousExperience,
};
use anyhow::{bail, Result};
use enums::{attribute::AttributeEnum, combat_skill::CombatSkillEnum, skill::SkillEnum};
use sea_orm::{ConnectionTrait, DatabaseConnection};
use serde::{Deserialize, Serialize};
use strum::IntoEnumIterator as _;
use ts_rs::TS;

use crate::entity::character as character_entity;

use super::{
    advantage::AdvantageTrait,
    attribute::Attributes,
    combat_skills::CombatSkills,
    experience::Experience,
    reward::Reward,
    secondary_attributes::{SecondaryAttribute, SecondaryAttributeEnum},
    skill_type::SkillType,
    skills::Skills,
    virtue::{Virtue, VirtueEnum},
};

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct Characteristics {
    pub skills: Skills,
    pub combat_skills: CombatSkills,
    pub attributes: Attributes,
    pub adventure_experience: Experience,
    pub skill_experience: Experience,
    pub valor: SecondaryAttribute<Reward>,
    pub wisdom: SecondaryAttribute<Virtue>,
}

impl Characteristics {
    pub fn new(choices: &Choices) -> Result<Self> {
        let skills = Skills::new(choices)?;
        let combat_skills = CombatSkills::new(choices, true)?;
        let attributes = Attributes::new(choices);

        // Check previous experience
        if choices.previous_experience != 0 {
            bail!("You need to use all your previous experience");
        }
        PreviousExperience::check_previous_experience(choices)?;

        // Valor - Wisdom
        let mut valor = SecondaryAttribute::new();
        let mut wisdom = SecondaryAttribute::new();

        // Add the selected valor and virtue
        valor.add_advantage(&choices.reward);
        wisdom.add_advantage(&choices.virtue);

        // Add the blessings to wisdom
        let blessings = Culture::new(choices.culture.clone()).blessings;
        for blessing in blessings {
            let adv = Virtue::from_enum(&blessing);
            wisdom.add_advantage(&adv);
        }

        Ok(Self {
            skills,
            combat_skills,
            attributes,
            adventure_experience: Experience::new(),
            skill_experience: Experience::new(),
            valor,
            wisdom,
        })
    }

    pub async fn from_db_model(
        db: &DatabaseConnection,
        value: &character_entity::Model,
    ) -> Result<Self> {
        let skills = Skills::from_db_model(db, value.id, &value.characteristics_skills).await?;
        let combat_skills = CombatSkills::from_db_model(&value.characteristics_combat_skills)?;
        let attributes = Attributes::from_db_model(value);
        let adventure_experience = Experience::from_db(
            value
                .characteristics_adventure_experience_current
                .try_into()?,
            value
                .characteristics_adventure_experience_total
                .try_into()?,
        );
        let skill_experience = Experience::from_db(
            value.characteristics_skill_experience_current.try_into()?,
            value.characteristics_skill_experience_total.try_into()?,
        );
        let valor = SecondaryAttribute::<Reward>::from_db_model(
            db,
            value.characteristics_valor.try_into()?,
            value.id,
        )
        .await?;
        let wisdom = SecondaryAttribute::<Virtue>::from_db_model(
            db,
            value.characteristics_wisdom.try_into()?,
            value.id,
        )
        .await?;

        Ok(Self {
            skills,
            combat_skills,
            attributes,
            adventure_experience,
            skill_experience,
            valor,
            wisdom,
        })
    }

    pub fn compute_derived_values(&mut self, one_shot_rules_or_strider: bool) {
        self.attributes
            .compute_derived_values(one_shot_rules_or_strider);
    }

    pub fn can_upgrade_skills(&self) -> Vec<SkillEnum> {
        let points = self.skill_experience.get_current();
        let mut out = vec![];
        for skill in SkillEnum::iter() {
            let value = self.skills.get_skill(&skill);
            if Experience::cost_new_level(value + 1) <= points {
                out.push(skill);
            }
        }
        out
    }

    pub fn can_upgrade_combat_skills(&self) -> Vec<CombatSkillEnum> {
        let points = self.adventure_experience.get_current();
        let mut out = vec![];
        for skill in CombatSkillEnum::iter() {
            let value = self.combat_skills.get_skill(&skill);
            if Experience::cost_new_level(value + 1) <= points {
                out.push(skill);
            }
        }
        out
    }

    pub fn can_upgrade_secondary_attributes(&self) -> Vec<SecondaryAttributeEnum> {
        let points = self.adventure_experience.get_current();
        let mut out = vec![];

        if Experience::cost_new_level(self.valor.get_value() + 1) <= points {
            out.push(SecondaryAttributeEnum::Valor);
        }
        if Experience::cost_new_level(self.wisdom.get_value() + 1) <= points {
            out.push(SecondaryAttributeEnum::Wisdom);
        }
        out
    }

    fn increase_skill_internal(
        experience: &mut Experience,
        mut current_value: u32,
        mut delta: i32,
        free_change: bool,
    ) -> Result<u32> {
        let mut total_xp = 0;

        if delta > 0 {
            // Deal with increase
            while delta > 0 {
                current_value += 1;
                delta -= 1;
                total_xp += Experience::cost_new_level(current_value);
            }
            if total_xp > experience.get_current() {
                bail!("Not enough xp");
            }
            if !free_change {
                experience.spend(total_xp as i32)?;
            }
        } else {
            // Deal with decrease
            while delta < 0 && current_value > 0 {
                total_xp += Experience::cost_new_level(current_value);
                current_value -= 1;
                delta += 1;
            }
            if delta < 0 {
                bail!("Cannot decrease a skill below 0");
            }
            if !free_change {
                experience.spend(-(total_xp as i32))?;
            }
        }
        Ok(current_value)
    }

    pub fn increase_skill(
        &mut self,
        skill: &SkillEnum,
        delta: i32,
        free_change: bool,
    ) -> Result<u32> {
        let current_value = self.skills.get_skill(skill);
        let new = Characteristics::increase_skill_internal(
            &mut self.skill_experience,
            current_value,
            delta,
            free_change,
        )?;
        self.skills.set_skill(skill, new);
        Ok(new)
    }

    fn increase_secondary_attribute<E, T>(
        adv_exp: &mut Experience,
        attribute: &mut SecondaryAttribute<T>,
        adv: &T,
        delta: i32,
        free_change: bool,
    ) -> Result<(u32, Vec<T>)>
    where
        T: AdvantageTrait<Enum = E> + Clone + for<'sa> Deserialize<'sa> + Serialize,
        T::Enum: PartialEq + for<'sa> Deserialize<'sa>,
        E: TS,
    {
        let new = Characteristics::increase_skill_internal(
            adv_exp,
            attribute.get_value(),
            delta,
            free_change,
        )?;
        attribute.set_value(new);
        if delta > 0 {
            attribute.add_advantage(adv);
        }
        Ok((new, attribute.get_advantages().to_vec()))
    }

    pub fn increase_wisdom(
        &mut self,
        adv: &Virtue,
        delta: i32,
        free_change: bool,
    ) -> Result<(u32, Vec<Virtue>)> {
        Self::increase_secondary_attribute(
            &mut self.adventure_experience,
            &mut self.wisdom,
            adv,
            delta,
            free_change,
        )
    }

    pub fn increase_valor(
        &mut self,
        adv: &Reward,
        delta: i32,
        free_change: bool,
    ) -> Result<(u32, Vec<Reward>)> {
        Self::increase_secondary_attribute(
            &mut self.adventure_experience,
            &mut self.valor,
            adv,
            delta,
            free_change,
        )
    }

    pub fn increase_combat_skill(
        &mut self,
        skill: &CombatSkillEnum,
        delta: i32,
        free_change: bool,
    ) -> Result<u32> {
        let current_value = self.combat_skills.get_skill(skill);
        let new = Characteristics::increase_skill_internal(
            &mut self.adventure_experience,
            current_value,
            delta,
            free_change,
        )?;
        self.combat_skills.set_skill(skill, new);
        Ok(new)
    }

    pub fn get_rewards(&self) -> &[Reward] {
        self.valor.get_advantages()
    }

    pub fn get_virtues(&self) -> &[Virtue] {
        self.wisdom.get_advantages()
    }

    pub fn get_virtue_enums(&self) -> Vec<VirtueEnum> {
        let virtues = self.wisdom.get_advantages();
        virtues.iter().filter_map(Virtue::get_enum).collect()
    }

    pub fn take_fellowship_phase(&mut self, yule: bool) {
        self.attributes
            .take_fellowship_phase(yule, &self.get_virtue_enums());
        if yule {
            self.skill_experience
                .add(self.attributes.wits.get_value() as i32)
                .expect("Failed to add wits to skill points");
        }
    }

    pub fn loose_endurance(&mut self, value: i32) {
        self.attributes.endurance.loose(value);
    }

    pub fn loose_hope(&mut self, mut value: i32) {
        let virtues = self.get_virtue_enums();
        if virtues.contains(&VirtueEnum::TheArtOfSmoking) {
            value += 1;
        };
        self.attributes.hope.loose(value);
    }

    pub fn reward_xp(&mut self, skill: i32, adventure: i32) -> Result<()> {
        self.skill_experience.add(skill)?;
        self.adventure_experience.add(adventure)?;
        Ok(())
    }

    pub fn get_secondary_attribute(&self, skill: &SecondaryAttributeEnum) -> u32 {
        match skill {
            SecondaryAttributeEnum::Valor => self.valor.get_value(),
            SecondaryAttributeEnum::Wisdom => self.wisdom.get_value(),
        }
    }

    pub fn get_target_number(&self, skill: &SkillType, one_shot_rules: bool) -> Option<u32> {
        match skill {
            SkillType::Skill(x) => match x.get_attribute() {
                AttributeEnum::Strength => {
                    Some(self.attributes.strength.get_target_number(one_shot_rules))
                }
                AttributeEnum::Heart => {
                    Some(self.attributes.heart.get_target_number(one_shot_rules))
                }
                AttributeEnum::Wits => Some(self.attributes.wits.get_target_number(one_shot_rules)),
            },
            SkillType::Combat(_) => {
                Some(self.attributes.strength.get_target_number(one_shot_rules))
            }
            SkillType::Protection => None,
            SkillType::SecondaryAttribute(x) => match x {
                SecondaryAttributeEnum::Valor => {
                    Some(self.attributes.heart.get_target_number(one_shot_rules))
                }
                SecondaryAttributeEnum::Wisdom => {
                    Some(self.attributes.wits.get_target_number(one_shot_rules))
                }
            },
        }
    }

    pub async fn delete_by_character_id<C: ConnectionTrait>(
        db: &C,
        character_id: i32,
    ) -> Result<()> {
        // Attributes
        Skills::delete_by_character_id(db, character_id).await?;
        Reward::delete_by_character_id(db, character_id).await?;
        Virtue::delete_by_character_id(db, character_id).await
    }

    pub async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        // Favoured skills
        self.skills.save(db, character_id).await?;

        // Reward
        self.valor.save(db, character_id).await?;
        self.wisdom.save(db, character_id).await
    }
}
