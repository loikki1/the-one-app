use serde::Serialize;
use ts_rs::TS;

use crate::roll::{FeatResult, Roll};

#[derive(Serialize, TS)]
#[ts(export)]
pub struct CharacterRoll {
    pub roll: Roll,
    pub success: Option<bool>,
}

impl CharacterRoll {
    pub fn new(roll: Roll, target: Option<u32>) -> Self {
        let success = if roll.feat_dice == FeatResult::AutomaticSuccess {
            Some(true)
        } else if roll.feat_dice == FeatResult::CriticalFailure {
            Some(false)
        } else if let Some(target) = target {
            if let Some(total) = roll.total {
                Some(total >= target)
            } else {
                panic!("This should never happen")
            }
        } else {
            None
        };

        CharacterRoll { roll, success }
    }
}
