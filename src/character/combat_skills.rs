use crate::new_character::choices::Choices;
use anyhow::{anyhow, bail, Result};
use enums::combat_skill::CombatSkillEnum;
use serde::{Deserialize, Serialize};
use strum::EnumCount as _;
use ts_rs::TS;

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct CombatSkills {
    skills: [u32; CombatSkillEnum::COUNT],
}

impl CombatSkills {
    pub fn new(choices: &Choices, add_improvements: bool) -> Result<Self> {
        if choices.all_combat_skill.is_none() || choices.available_combat_skill.is_none() {
            bail!("You need to select all your combat skills");
        }
        let all_skill = choices
            .all_combat_skill
            .clone()
            .ok_or(anyhow!("Failed to clone combat skills"))?;
        let available_skill = choices
            .available_combat_skill
            .clone()
            .ok_or(anyhow!("Failed to clone combat skills"))?;
        if all_skill == available_skill {
            bail!("You cannot pick twice the same combat skill");
        }

        // Add the select skills
        let mut ret = Self {
            skills: [0; CombatSkillEnum::COUNT],
        };
        ret.set_skill(&all_skill, 1);
        ret.set_skill(&available_skill, 2);

        // Leave if there is no need to add the improvements
        if !add_improvements {
            return Ok(ret);
        }

        // Update skills value
        for improvement in &choices.improvements.combat {
            let new_value = *improvement.1;
            ret.set_skill(improvement.0, new_value);
        }

        Ok(ret)
    }

    pub fn from_db_model(value: &[u8]) -> Result<Self> {
        if value.len() != 4 * CombatSkillEnum::COUNT {
            bail!("This should never happen, number of skills in db does not match the app");
        };

        let mut skills_array: [u32; CombatSkillEnum::COUNT] = [0; CombatSkillEnum::COUNT];
        for (i, el) in skills_array.iter_mut().enumerate() {
            let slice = &value[(4 * i)..(4 * (i + 1))];
            *el = u32::from_be_bytes(slice.try_into()?);
        }

        Ok(Self {
            skills: skills_array,
        })
    }

    pub fn set_skill(&mut self, skill: &CombatSkillEnum, value: u32) {
        self.skills[skill.clone() as usize] = value;
    }

    pub fn get_skill(&self, skill: &CombatSkillEnum) -> u32 {
        self.skills[skill.clone() as usize]
    }

    pub fn get_skills_bytes(&self) -> Vec<u8> {
        self.skills.iter().flat_map(|x| x.to_be_bytes()).collect()
    }
}
