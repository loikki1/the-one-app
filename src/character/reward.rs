use anyhow::{bail, Result};
use rocket::request::FromParam;
use sea_orm::{
    ActiveModelTrait as _,
    ActiveValue::{NotSet, Set},
    ColumnTrait as _, ConnectionTrait, EntityTrait as _, ModelTrait as _, QueryFilter as _,
};
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter};
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

use crate::assets::read_enum_from_assets;
use crate::entity::reward as entity;
use crate::utils::convert_option_to_set;

use super::advantage::AdvantageTrait;

const DIRECTORY: &str = "rewards";

#[derive(
    PartialEq,
    Clone,
    Serialize,
    Deserialize,
    EnumIter,
    EnumCount,
    Debug,
    FromParamEnum,
    TS,
    Hash,
    Eq,
)]
#[ts(export)]
pub enum RewardEnum {
    CloseFitting,
    CunningMake,
    Fell,
    Grievous,
    Keen,
    Reinforced,
}

#[derive(Serialize, Deserialize, Clone, TS, Debug, Default)]
#[ts(export)]
pub struct Reward {
    id: Option<i32>,
    title: String,
    description: String,
    available_at_creation: bool,
}

// AdvantageTrait
impl AdvantageTrait for Reward {
    type Enum = RewardEnum;

    fn get_id(&self) -> Option<i32> {
        self.id
    }

    fn from_enum(value: &Self::Enum) -> Self {
        read_enum_from_assets(value, DIRECTORY)
    }

    fn get_enum(&self) -> Option<Self::Enum> {
        serde_json::from_str(&self.title).ok()
    }
    fn is_available_at_creation(&self) -> bool {
        self.available_at_creation
    }

    async fn from_db_model<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<Vec<Self>> {
        Ok(entity::Entity::find()
            .filter(entity::Column::CharacterId.eq(character_id))
            .all(db)
            .await?
            .iter()
            .map(|x| (*x).clone().into())
            .collect())
    }

    async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        let mut model: entity::ActiveModel = (*self).clone().into();
        model.character_id = Set(character_id);
        model.save(db).await?;
        Ok(())
    }

    async fn delete_by_character_id<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<()> {
        entity::Entity::delete_many()
            .filter(entity::Column::CharacterId.eq(character_id))
            .exec(db)
            .await?;
        Ok(())
    }

    async fn delete_by_id<C: ConnectionTrait>(db: &C, character_id: i32, id: i32) -> Result<()> {
        let reward = entity::Entity::find_by_id(id).one(db).await?;

        if let Some(reward) = reward {
            if reward.character_id == character_id {
                reward.delete(db).await?;
                Ok(())
            } else {
                bail!("Cannot delete reward from someone else")
            }
        } else {
            bail!("Reward not found")
        }
    }

    fn reset_id(&mut self) {
        self.id = None;
    }
}

impl From<entity::Model> for Reward {
    fn from(value: entity::Model) -> Self {
        Self {
            id: Some(value.id),
            title: value.title,
            description: value.description,
            available_at_creation: false,
        }
    }
}

impl From<Reward> for entity::ActiveModel {
    fn from(value: Reward) -> Self {
        let id = convert_option_to_set(value.id);
        Self {
            id,
            title: Set(value.title),
            description: Set(value.description),
            character_id: NotSet,
        }
    }
}

#[cfg(test)]
mod test {
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{
        assets::get_number_files,
        character::{
            advantage::AdvantageTrait as _,
            reward::{Reward, DIRECTORY},
        },
    };

    use super::RewardEnum;

    #[test]
    fn check_reward_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(RewardEnum::COUNT == number_files);

        for reward in RewardEnum::iter() {
            eprintln!("Testing {reward:?}");
            Reward::from_enum(&reward);
        }
    }
}
