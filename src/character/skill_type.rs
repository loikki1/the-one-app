use enums::{combat_skill::CombatSkillEnum, skill::SkillEnum};
use serde::Deserialize;
use ts_rs::TS;

use super::secondary_attributes::SecondaryAttributeEnum;

#[derive(Deserialize, TS)]
#[ts(export)]
pub enum SkillType {
    Combat(CombatSkillEnum),
    Skill(SkillEnum),
    SecondaryAttribute(SecondaryAttributeEnum),
    Protection,
}
