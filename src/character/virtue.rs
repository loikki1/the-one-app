use std::collections::HashSet;

use anyhow::{bail, Result};
use enums::culture::CultureEnum;
use rocket::request::FromParam;
use sea_orm::{
    ActiveModelTrait as _,
    ActiveValue::{NotSet, Set},
    ColumnTrait as _, ConnectionTrait, EntityTrait as _, ModelTrait as _, QueryFilter as _,
};
use serde::{de, Deserialize, Serialize};
use strum::{EnumCount, EnumIter};
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

use crate::entity::virtue as entity;
use crate::utils::convert_option_to_set;
use crate::{assets::read_enum_from_assets, utils::to_enum};

use super::advantage::AdvantageTrait;

const DIRECTORY: &str = "virtues";

#[derive(
    PartialEq,
    Clone,
    Serialize,
    Deserialize,
    Debug,
    EnumIter,
    EnumCount,
    FromParamEnum,
    TS,
    Hash,
    Eq,
)]
#[ts(export)]
pub enum VirtueEnum {
    AgainstTheUnseen,
    AHuntersResolve,
    AllegianceOfTheDunedain,
    AncientFire,
    ArtOfDisappearing,
    ArtificerOfEregion,
    BarukKhazad,
    BeautyOfTheStars,
    BeornsEnchantment,
    BesetByWoe,
    BraveAtAPinch,
    BreeBlood,
    BreePony,
    BrokenSpells,
    BrotherToBears,
    Confidence,
    Cram,
    DarkForDarkBusiness,
    DeadlyArchery,
    Defiance,
    DesperateCourage,
    DourHanded,
    DragonSlayer,
    DurinsWay,
    DwarfFriend,
    ElberethGilthoniel,
    ElfLights,
    ElvenSkill,
    ElvenWise,
    ElvishDreams,
    EnduranceOfTheRanger,
    FierceShot,
    FolkOfTheDusk,
    ForesightOfTheirKindred,
    ForestHarrier,
    FriendlyAndFamiliar,
    Furious,
    GleamOfWrath,
    GreatStrength,
    Halflings,
    Hardiness,
    HeirOfArnor,
    HerbalRemedies,
    HighDestiny,
    HobbitSense,
    HoundOfMirkwood,
    KingsOfMen,
    Mastery,
    MemoryOfAncientDays,
    MightOfTheFirstborn,
    NaturalWatchfulness,
    Naugrim,
    Nimbless,
    PettyDwarves,
    Prowess,
    Redoubtable,
    RoyaltyRevealed,
    ShotsInTheDark,
    SkillOfTheEldar,
    SkinCoat,
    SmallFolk,
    SplittingBlow,
    StaunchingSong,
    StoneHard,
    StoutHearted,
    StrangeAsNewsFromBree,
    StrengthOfWill,
    SureAtTheMark,
    TelcharsSecrets,
    TheArtOfSmoking,
    TheLanguageOfBirds,
    TheLongDefeat,
    ThreeIsCompany,
    ToughAsOldTreeRoots,
    TwiceBakedHoneyCakes,
    UntameableSpirit,
    WaysOfTheWild,
    WoodGoer,
}

#[derive(Serialize, Deserialize, Clone, Default, TS, Debug)]
#[ts(export)]
pub struct Virtue {
    id: Option<i32>,
    title: String,
    // setting from dict value
    description: String,
    #[serde(default)]
    text_input: String,
    available: bool,
    available_at_creation: bool,
    #[serde(default, deserialize_with = "deserialize_restricted_to")]
    restricted_to: Option<HashSet<CultureEnum>>,
    #[serde(default)]
    implemented: bool,
}

// AdvantageTrait
impl AdvantageTrait for Virtue {
    type Enum = VirtueEnum;

    fn get_id(&self) -> Option<i32> {
        self.id
    }

    fn from_enum(value: &Self::Enum) -> Self {
        read_enum_from_assets(value, DIRECTORY)
    }

    fn get_enum(&self) -> Option<Self::Enum> {
        let title = to_enum(&self.title);
        let title = format!("\"{title}\"");
        serde_json::from_str(&title).ok()
    }

    fn is_available(&self) -> bool {
        self.available
    }

    fn is_available_at_creation(&self) -> bool {
        self.available_at_creation
    }

    async fn from_db_model<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<Vec<Self>> {
        Ok(entity::Entity::find()
            .filter(entity::Column::CharacterId.eq(character_id))
            .all(db)
            .await?
            .iter()
            .map(|x| (*x).clone().into())
            .collect())
    }

    async fn save<C: ConnectionTrait>(&self, db: &C, character_id: i32) -> Result<()> {
        let mut model: entity::ActiveModel = (*self).clone().into();
        model.character_id = Set(character_id);
        model.save(db).await?;
        Ok(())
    }

    async fn delete_by_character_id<C: ConnectionTrait>(db: &C, character_id: i32) -> Result<()> {
        entity::Entity::delete_many()
            .filter(entity::Column::CharacterId.eq(character_id))
            .exec(db)
            .await?;
        Ok(())
    }

    async fn delete_by_id<C: ConnectionTrait>(db: &C, character_id: i32, id: i32) -> Result<()> {
        let virtue = entity::Entity::find_by_id(id).one(db).await?;

        if let Some(virtue) = virtue {
            if virtue.character_id == character_id {
                virtue.delete(db).await?;
                Ok(())
            } else {
                bail!("Cannot delete virtue from someone else")
            }
        } else {
            bail!("Virtue not found")
        }
    }
    fn reset_id(&mut self) {
        self.id = None;
    }
}

// Implementation
impl Virtue {
    pub fn get_culture_restrictions(&self) -> Option<HashSet<CultureEnum>> {
        self.restricted_to.clone()
    }
}

// TODO delete this (compatibility)
#[derive(Deserialize)]
#[serde[untagged]]
enum RestrictedTo {
    V1(Option<CultureEnum>),
    V2(Option<HashSet<CultureEnum>>),
}

// TODO delete this (compatibility)
fn deserialize_restricted_to<'de, D>(
    deserializer: D,
) -> Result<Option<HashSet<CultureEnum>>, D::Error>
where
    D: de::Deserializer<'de>,
{
    let rest: RestrictedTo = de::Deserialize::deserialize(deserializer)?;
    match rest {
        RestrictedTo::V2(x) => Ok(x),
        RestrictedTo::V1(x) => {
            if let Some(x) = x {
                let mut hash = HashSet::new();
                hash.insert(x);
                Ok(Some(hash))
            } else {
                Ok(None)
            }
        }
    }
}

impl From<entity::Model> for Virtue {
    fn from(value: entity::Model) -> Self {
        let mut out = Self {
            id: Some(value.id),
            title: value.title,
            description: value.description,
            text_input: value.text_input,
            available: false,
            available_at_creation: false,
            restricted_to: None,
            implemented: false,
        };
        let enum_value = out.get_enum();
        if let Some(enum_value) = enum_value {
            out.implemented = Self::from_enum(&enum_value).implemented;
        };
        out
    }
}

impl From<Virtue> for entity::ActiveModel {
    fn from(value: Virtue) -> Self {
        let id = convert_option_to_set(value.id);
        Self {
            id,
            title: Set(value.title),
            description: Set(value.description),
            text_input: Set(value.text_input),
            character_id: NotSet,
        }
    }
}

// Testing
#[cfg(test)]
mod test {
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{
        assets::get_number_files,
        character::{
            advantage::AdvantageTrait as _,
            virtue::{Virtue, DIRECTORY},
        },
    };

    use super::VirtueEnum;

    #[test]
    fn check_virtue_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(VirtueEnum::COUNT == number_files);

        // Check that all the known virtues work
        for virtue in VirtueEnum::iter() {
            eprintln!("Testing {virtue:?}");
            let read = Virtue::from_enum(&virtue);
            let enum_read = read
                .get_enum()
                .expect("Official virtues should have their enum set");
            assert_eq!(enum_read, virtue);
        }
    }
}
