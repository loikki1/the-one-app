use anyhow::{anyhow, bail, Result};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

#[derive(Serialize, Deserialize, TS, Debug, Clone)]
#[ts(export)]
pub struct Experience {
    current: u32,
    total: u32,
}

impl Default for Experience {
    fn default() -> Self {
        Self::new()
    }
}

impl Experience {
    pub fn new() -> Self {
        Self {
            current: 0,
            total: 0,
        }
    }

    pub fn from_db(current: u32, total: u32) -> Self {
        Self { current, total }
    }

    pub fn get_current(&self) -> u32 {
        self.current
    }

    pub fn get_total(&self) -> u32 {
        self.total
    }

    pub fn cost_new_level(new_level: u32) -> u32 {
        match new_level {
            1 => 4,
            2 => 8,
            3 => 12,
            4 => 20,
            5 => 26,
            6 => 30,
            _ => u32::MAX,
        }
    }

    pub fn add(&mut self, value: i32) -> Result<()> {
        let new_current = self.current.checked_add_signed(value);
        if let Some(new_current) = new_current {
            self.current = new_current;
            self.total = self
                .total
                .checked_add_signed(value)
                .ok_or(anyhow!("Failed to add to total experience"))?;
            Ok(())
        } else {
            bail!("Cannot remove more xp than available")
        }
    }

    pub fn spend(&mut self, value: i32) -> Result<()> {
        let new_current = self.current.checked_add_signed(-value);
        if let Some(new_current) = new_current {
            self.current = new_current;
            Ok(())
        } else {
            bail!("Cannot remove more xp than available")
        }
    }
}
