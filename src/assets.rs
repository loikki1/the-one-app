use rust_embed::Embed;
use serde::{Deserialize, Serialize};
use std::str::from_utf8;

#[derive(Embed)]
#[folder = "data"]
pub struct Assets;

pub fn read_enum_from_assets<Enum, Type>(value: &Enum, directory: &str) -> Type
where
    Enum: Serialize,
    Type: for<'a> Deserialize<'a>,
{
    let mut value = serde_json::to_string(&value).expect("Failed to serialize");

    // Remove \"\"
    value.pop();
    value.remove(0);

    let filename = format!("{directory}/{value}.json");

    read_file(&filename)
}

pub fn read_file<Type>(filename: &str) -> Type
where
    Type: for<'a> Deserialize<'a>,
{
    let file = Assets::get(filename).expect("Failed to open asset");
    let content = from_utf8(&file.data).expect("Failed to convert to &str");
    serde_json::from_str(content).expect("Failed to deserialize")
}

#[cfg(test)]
pub fn get_number_files(directory: &str) -> usize {
    let mut count = 0;
    for file in Assets::iter() {
        if file.contains(directory) {
            count += 1;
        }
    }
    count
}
