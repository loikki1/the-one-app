use rand::Rng as _;
use rocket::{request::FromParam, FromFormField};
use serde::{Deserialize, Serialize};
use std::cmp::{max, min};
use std::iter::repeat_with;
use strum::EnumIter;
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

#[derive(
    Copy, PartialEq, Clone, Deserialize, Serialize, FromParamEnum, EnumIter, TS, FromFormField,
)]
#[ts(export)]
pub enum RollType {
    Favoured,
    Normal,
    IllFavoured,
}

#[derive(Serialize, TS, PartialEq)]
#[ts(export)]
pub enum FeatResult {
    Normal(u32),
    AutomaticSuccess,
    CriticalFailure,
}

#[derive(Serialize, TS)]
#[ts(export)]
pub struct Roll {
    pub success_dices: Vec<u32>,
    pub feat_dice: FeatResult,
    pub total: Option<u32>,
}

const SAURON: u32 = 0;
const GANDALF: u32 = 12;
pub const TENGWAR: u32 = 6;

impl Roll {
    pub fn roll_dices(
        roll_type: RollType,
        number_dices: u32,
        is_weary: bool,
        is_miserable: bool,
    ) -> Self {
        // Roll for the feat
        let mut feat = Roll::roll_feat();

        // Roll types
        if roll_type == RollType::Favoured {
            feat = max(feat, Roll::roll_feat());
        } else if roll_type == RollType::IllFavoured {
            feat = min(feat, Roll::roll_feat());
        }

        let feat_dice = if feat == GANDALF {
            FeatResult::AutomaticSuccess
        } else if feat == SAURON {
            if is_miserable {
                FeatResult::CriticalFailure
            } else {
                FeatResult::Normal(0)
            }
        } else {
            FeatResult::Normal(feat)
        };

        // Roll for the skills
        let success_dices: Vec<u32> = repeat_with(Roll::roll_skill)
            .take(number_dices as usize)
            .collect();

        let total = match feat_dice {
            FeatResult::CriticalFailure | FeatResult::AutomaticSuccess => None,
            FeatResult::Normal(feat) => Some(
                feat + success_dices
                    .iter()
                    .copied()
                    .reduce(|acc, x| {
                        let add = if is_weary && x < 4 { 0 } else { x };
                        acc + add
                    })
                    .unwrap_or(0),
            ),
        };

        // Prepare output
        Self {
            success_dices,
            feat_dice,
            total,
        }
    }

    pub fn roll_skill() -> u32 {
        let mut rng = rand::thread_rng();
        rng.gen_range(1..=6)
    }
    pub fn roll_feat() -> u32 {
        let mut rng = rand::thread_rng();
        let mut value = rng.gen_range(1..=12);
        if value == 11 {
            value = SAURON;
        }
        value
    }
}
