use crate::entity::fell_ability as entity;
use anyhow::Result;
use sea_orm::ActiveValue::NotSet;
use sea_orm::{
    ActiveModelTrait as _, ColumnTrait as _, ConnectionTrait, DatabaseTransaction,
    EntityTrait as _, QueryFilter as _, Set,
};
use serde::de::Error as SerdeError;
use serde::{Deserialize, Deserializer, Serialize};
use serde_json::value::RawValue;
use strum::{EnumCount, EnumIter};
use ts_rs::TS;

use crate::assets::read_enum_from_assets;
use crate::utils::convert_option_to_set;

const DIRECTORY: &str = "adversaries/fell_abilities";

#[derive(Clone, Copy, Serialize, Deserialize, Debug, EnumIter, EnumCount)]
pub enum FellAbilityEnum {
    Craven,
    DeadlyWound,
    Deathless,
    DenizenOfTheDark,
    DreadfulSpells,
    DullWitted,
    FearOfFire,
    FierceFolk,
    GreatLeap,
    HateSunlight,
    HatredDwarves,
    Heartless,
    HideousToughness,
    HorribleStrength,
    HowlOfTriumph,
    OrcPoison,
    SnakeLikeSpeed,
    StrikeFear,
    ThickHide,
    YellOfTriumph,
}

#[derive(Serialize, Deserialize, TS, Clone, Debug)]
#[ts(export)]
pub struct FellAbility {
    pub id: Option<i32>,
    pub title: String,
    pub description: String,
}

impl From<FellAbilityEnum> for FellAbility {
    fn from(item: FellAbilityEnum) -> Self {
        read_enum_from_assets(&item, DIRECTORY)
    }
}

// For enum and full ability
pub fn deserialize_fell_abilities<'de, D>(deserializer: D) -> Result<Vec<FellAbility>, D::Error>
where
    D: Deserializer<'de>,
{
    let json: Box<RawValue> = Deserialize::deserialize(deserializer)?;
    match serde_json::from_str::<Vec<FellAbilityEnum>>(json.get()) {
        Ok(x) => Ok(x.iter().map(|&y| FellAbility::from(y)).collect()),
        Err(_) => serde_json::from_str::<Vec<FellAbility>>(json.get()).map_err(SerdeError::custom),
    }
}

impl From<entity::Model> for FellAbility {
    fn from(value: entity::Model) -> Self {
        Self {
            id: Some(value.id),
            title: value.title,
            description: value.description,
        }
    }
}

impl From<FellAbility> for entity::ActiveModel {
    fn from(value: FellAbility) -> Self {
        let id = convert_option_to_set(value.id);
        Self {
            id,
            title: Set(value.title),
            description: Set(value.description),
            adversary_id: NotSet,
        }
    }
}

impl FellAbility {
    pub async fn save(&self, db: &DatabaseTransaction, adversary_id: i32) -> Result<()> {
        let mut model: entity::ActiveModel = (*self).clone().into();
        model.adversary_id = Set(adversary_id);
        model.save(db).await?;
        Ok(())
    }

    pub async fn delete_by_adversary_id<C: ConnectionTrait>(
        db: &C,
        adversary_id: i32,
    ) -> Result<()> {
        entity::Entity::delete_many()
            .filter(entity::Column::AdversaryId.eq(adversary_id))
            .exec(db)
            .await?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{
        adversary::fell_ability::{FellAbility, DIRECTORY},
        assets::get_number_files,
    };

    use super::FellAbilityEnum;

    #[test]
    fn check_fell_ability_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(FellAbilityEnum::COUNT == number_files);

        for ability in FellAbilityEnum::iter() {
            eprintln!("Testing {ability:?}");
            let _: FellAbility = FellAbility::from(ability);
        }
    }
}
