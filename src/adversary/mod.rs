use std::{fs::File, path::Path};

use anyhow::{anyhow, bail, Result};
use enums::hate::HateEnum;
use sea_orm::{
    ActiveModelTrait as _, ColumnTrait as _, DatabaseConnection, EntityTrait as _,
    QueryFilter as _, Set, TransactionTrait as _,
};
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter, IntoEnumIterator as _};
use ts_rs::TS;

use crate::{
    assets::read_enum_from_assets,
    common::state_cap::StateCap,
    entity::{
        adversary as adversary_entity, adversary_combat_proficiency as combat_proficiency_entity,
        fell_ability as fell_ability_entity,
    },
    name_or_id::NameOrId,
    users::User,
    utils::convert_option_to_set,
};

use self::{combat_proficiency::AdversaryCombatProficiency, fell_ability::FellAbility};

pub mod combat_proficiency;
pub mod fell_ability;

const DIRECTORY: &str = "adversaries/adversaries";

#[derive(Serialize, Deserialize, Debug, EnumIter, EnumCount, TS)]
pub enum AdversaryEnum {
    BarrowWight,
    CaveTrollSlinker,
    FellWraith,
    Footpad,
    GoblinArcher,
    GreatCaveTroll,
    GreatOrcBodyguard,
    GreatOrcChief,
    HighwayRobber,
    HoundOfSauron,
    MarshDwellers,
    OrcChieftain,
    OrcGuard,
    OrcSoldier,
    RuffianChief,
    SouthernerChampion,
    SouthernerRaider,
    StoneTrollChief,
    StoneTrollRobber,
    WildWolf,
    WolfChieftain,
}

#[derive(Serialize, Deserialize, Default, TS, Debug, Clone)]
#[ts(export)]
pub struct Adversary {
    id: Option<i32>,
    name: String,
    features: Vec<String>,
    attribute_level: u32,
    endurance: StateCap,
    might: u32,
    hate: StateCap,
    hate_type: HateEnum,
    parry: u32,
    armour: u32,
    combat_proficiencies: Vec<AdversaryCombatProficiency>,
    #[serde(deserialize_with = "fell_ability::deserialize_fell_abilities")]
    abilities: Vec<FellAbility>,
}

impl Adversary {
    pub fn new(adv: AdversaryEnum) -> Self {
        let mut adv: Self = read_enum_from_assets(&adv, DIRECTORY);
        adv.endurance.reset();
        adv.hate.reset();
        adv
    }

    #[deprecated(since = "3.0.0", note = "Please use the sql functions")]
    pub fn load(filename: &Path) -> Result<Adversary> {
        let file = File::open(filename)?;
        let adversary: Self = serde_json::from_reader(file)?;
        Ok(adversary)
    }

    pub async fn get(db: &DatabaseConnection, user: &User, adversary: NameOrId) -> Result<Self> {
        match adversary {
            NameOrId::Name(x) => {
                let name = format!("\"{x}\"");
                let adv = serde_json::from_str::<AdversaryEnum>(&name)?;
                Ok(Self::new(adv))
            }
            NameOrId::Id(x) => {
                let adv = adversary_entity::Entity::find_by_id(x)
                    .one(db)
                    .await?
                    .ok_or(anyhow!("Cannot find requested adversary"))?;
                if adv.owner_id == user.id.expect("User should have an id") {
                    Ok(Self::from_db_model(db, adv).await?)
                } else {
                    bail!("You cannot get the adversary of someone else")
                }
            }
        }
    }

    pub async fn delete_from_db(db: &DatabaseConnection, user: User, id: i32) -> Result<()> {
        let adv = adversary_entity::Entity::find_by_id(id).one(db).await?;
        // Adversary exist
        if let Some(adv) = adv {
            // Belong to user
            if adv.owner_id == user.id.expect("User has an id") {
                // Start a transaction
                let transaction = db.begin().await?;

                FellAbility::delete_by_adversary_id(&transaction, id).await?;
                AdversaryCombatProficiency::delete_by_adversary_id(&transaction, id).await?;

                adversary_entity::Entity::delete_by_id(id)
                    .exec(&transaction)
                    .await?;
                transaction.commit().await?;
                Ok(())
            }
            // Does not belong to user
            else {
                bail!(
                    "Permission denied: You cannot delete an adversary that does not belong to you"
                )
            }
        }
        // Adversary does not exist
        else {
            bail!(format!("Adversary {id} not found"))
        }
    }

    pub fn get_id(&self) -> Option<i32> {
        self.id
    }

    pub async fn get_list_from_db(db: &DatabaseConnection, owner: User) -> Result<Vec<Self>> {
        let id = owner.id.ok_or(anyhow!("Expect the owner to have an id"))?;
        let advs = adversary_entity::Entity::find()
            .filter(adversary_entity::Column::OwnerId.eq(id))
            .all(db)
            .await?;

        let mut out = Vec::with_capacity(advs.len());
        for adv in advs {
            let adv = Self::from_db_model(db, adv).await?;
            out.push(adv);
        }
        Ok(out)
    }

    pub fn get_list_from_enums() -> Vec<Self> {
        AdversaryEnum::iter().map(Self::new).collect()
    }

    pub async fn from_db_model(
        db: &DatabaseConnection,
        value: adversary_entity::Model,
    ) -> Result<Self> {
        // Endurance
        let mut endurance = StateCap::new(value.max_endurance.try_into()?);
        endurance.set_current(value.current_endurance.try_into()?);

        // Hate
        let mut hate = StateCap::new(value.max_hate.try_into()?);
        hate.set_current(value.current_hate.try_into()?);

        // Combat proficiencies
        let prof = combat_proficiency_entity::Entity::find()
            .filter(combat_proficiency_entity::Column::AdversaryId.eq(value.id))
            .all(db)
            .await?
            .iter()
            .map(|x| (*x).clone().into())
            .collect();

        // Abilities
        let abilities = fell_ability_entity::Entity::find()
            .filter(fell_ability_entity::Column::AdversaryId.eq(value.id))
            .all(db)
            .await?
            .iter()
            .map(|x| (*x).clone().into())
            .collect();

        let hate_type =
            serde_json::from_str(&value.hate_type).expect("SQL and serde enums are not compatible");
        Ok(Self {
            id: Some(value.id),
            name: value.name,
            // TODO split
            features: value.features.split(',').map(String::from).collect(),
            attribute_level: value.attribute_level.try_into()?,
            endurance,
            might: value.might.try_into()?,
            hate,
            hate_type,
            parry: value.parry.try_into()?,
            armour: value.armour.try_into()?,
            combat_proficiencies: prof,
            abilities,
        })
    }

    pub async fn save_to_db(&self, db: &DatabaseConnection, owner: &User) -> Result<i32> {
        // Check permissions
        let owner_id = owner
            .id
            .ok_or(anyhow!("Cannot save an adversary without an owner"))?;
        if let Some(id) = self.id {
            let previous = adversary_entity::Entity::find_by_id(id).one(db).await?;
            if previous.is_some() && owner_id != previous.clone().unwrap().owner_id {
                bail!("You cannot modify the adversary of someone else");
            } else if previous.is_none() {
                bail!("Adversary does not exist");
            }
        }

        // Start a transaction
        let transaction = db.begin().await?;

        // Adversary
        let id = convert_option_to_set(self.id);
        let hate_type =
            serde_json::to_string(&self.hate_type).expect("Failed to convert enum to string");
        let entity = adversary_entity::ActiveModel {
            id,
            name: Set(self.name.clone()),
            features: Set(self.features.join(",")),
            attribute_level: Set(self.attribute_level.try_into()?),
            current_endurance: Set(self.endurance.get_current().try_into()?),
            max_endurance: Set(self.endurance.get_max().try_into()?),
            might: Set(self.might.try_into()?),
            current_hate: Set(self.hate.get_current().try_into()?),
            max_hate: Set(self.hate.get_max().try_into()?),
            hate_type: Set(hate_type),
            parry: Set(self.parry.try_into()?),
            armour: Set(self.armour.try_into()?),
            owner_id: Set(owner_id),
        };
        let entity = entity.save(&transaction).await?;

        let adversary_id = entity.id.unwrap();

        // Combat Proficiencies
        for prof in &self.combat_proficiencies {
            prof.save(&transaction, adversary_id).await?;
        }

        // Fell abilities
        for ability in &self.abilities {
            ability.save(&transaction, adversary_id).await?;
        }

        // Close the transaction
        transaction.commit().await?;
        Ok(adversary_id)
    }

    pub fn loose_endurance(&mut self, inc: i32) {
        self.endurance.loose(inc);
    }
    pub fn loose_hate(&mut self, inc: i32) {
        self.hate.loose(inc);
    }
}

#[cfg(test)]
mod test {
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{
        adversary::{Adversary, DIRECTORY},
        assets::get_number_files,
    };

    use super::AdversaryEnum;

    #[test]
    fn check_adversaries_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(AdversaryEnum::COUNT == number_files);

        for adv in AdversaryEnum::iter() {
            eprintln!("Testing {adv:?}");
            let _: Adversary = Adversary::new(adv);
        }
    }
}
