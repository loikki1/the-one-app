use crate::{entity::adversary_combat_proficiency as entity, utils::convert_option_to_set};
use anyhow::Result;
use sea_orm::{
    ActiveModelTrait as _, ActiveValue::NotSet, ColumnTrait as _, ConnectionTrait,
    DatabaseTransaction, EntityTrait as _, QueryFilter as _, Set,
};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

#[derive(Serialize, Deserialize, TS, Clone, Debug)]
#[ts(export)]
pub struct AdversaryCombatProficiency {
    id: Option<i32>,
    weapon: String,
    rating: u32,
    damage: u32,
    injury: u32,
    special_damages: Vec<String>,
}

impl From<entity::Model> for AdversaryCombatProficiency {
    fn from(value: entity::Model) -> Self {
        Self {
            id: Some(value.id),
            weapon: value.weapon,
            rating: value.rating.try_into().expect("Failed to convert"),
            damage: value.damage.try_into().expect("Failed to convert"),
            injury: value.injury.try_into().expect("Failed to convert"),
            special_damages: value.special_damages.split(',').map(String::from).collect(),
        }
    }
}

impl From<AdversaryCombatProficiency> for entity::ActiveModel {
    fn from(value: AdversaryCombatProficiency) -> Self {
        let id = convert_option_to_set(value.id);
        Self {
            id,
            weapon: Set(value.weapon),
            rating: Set(value.rating.try_into().expect("Failed to convert")),
            damage: Set(value.damage.try_into().expect("Failed to convert")),
            injury: Set(value.injury.try_into().expect("Failed to convert")),
            special_damages: Set(value.special_damages.join(",")),
            adversary_id: NotSet,
        }
    }
}

impl AdversaryCombatProficiency {
    pub async fn save(&self, db: &DatabaseTransaction, adversary_id: i32) -> Result<()> {
        let mut model: entity::ActiveModel = (*self).clone().into();
        model.adversary_id = Set(adversary_id);
        model.save(db).await?;
        Ok(())
    }
    pub async fn delete_by_adversary_id<C: ConnectionTrait>(
        db: &C,
        adversary_id: i32,
    ) -> Result<()> {
        entity::Entity::delete_many()
            .filter(entity::Column::AdversaryId.eq(adversary_id))
            .exec(db)
            .await?;
        Ok(())
    }
}
