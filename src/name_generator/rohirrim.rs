use rand::seq::SliceRandom as _;
use rand::Rng as _;

use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

pub struct RohirrimNameGenerator;

impl NameGenerator for RohirrimNameGenerator {
    #[expect(clippy::too_many_lines)]
    fn get_name(&self, gender: Genders) -> String {
        const M1: [&str; 29] = [
            "Éad", "Al", "Bal", "Déor", "Dún", "Dern", "Eó", "Elf", "Erken", "Fast", "Fen", "Fol",
            "Fréa", "Frum", "Ful", "Gár", "Gam", "Gléo", "Gold", "Grim", "Guth", "Há", "Héo",
            "Here", "Heru", "Hold", "Léo", "Théo", "Wíd",
        ];
        const M2: [&str; 37] = [
            "bald", "beam", "blod", "brand", "ca", "canstan", "cred", "da", "dan", "dig", "dor",
            "dred", "ere", "fa", "fara", "fred", "gar", "gel", "grim", "helm", "heort", "here",
            "láf", "leth", "ling", "mód", "man", "mer", "mod", "mund", "nere", "or", "red",
            "thain", "tor", "ulf", "wine",
        ];
        const M3: [&str; 89] = [
            "Éadig",
            "Éadmód",
            "Éoblod",
            "Éogar",
            "Éoheort",
            "Éohere",
            "Éomód",
            "Éoman",
            "Éomer",
            "Éomund",
            "Éorcanstan",
            "Éored",
            "Éorl",
            "Éothain",
            "Éowine",
            "Aldor",
            "Baldor",
            "Baldred",
            "Bregdan",
            "Brego",
            "Brytta",
            "Ceorl",
            "Déor",
            "Déorbrand",
            "Déorgar",
            "Déorhelm",
            "Déorthain",
            "Déorwine",
            "Dúnhere",
            "Dernfara",
            "Derngar",
            "Dernhelm",
            "Dernwine",
            "Elfhelm",
            "Elfwine",
            "Erkenbrand",
            "Fasthelm",
            "Fastred",
            "Fengel",
            "Folca",
            "Folcred",
            "Folcwine",
            "Fréa",
            "Fréaláf",
            "Fréawine",
            "Fram",
            "Freca",
            "Frumgar",
            "Fulgar",
            "Fulgrim",
            "Fulor",
            "Fulthain",
            "Gálmód",
            "Gárbald",
            "Gárulf",
            "Gárwine",
            "Gamling",
            "Gléobeam",
            "Gléomer",
            "Gléothain",
            "Gléowine",
            "Goldwine",
            "Gríma",
            "Gram",
            "Grimbold",
            "Guthbrand",
            "Guthláf",
            "Guthmer",
            "Guthred",
            "Háma",
            "Héostor",
            "Haleth",
            "Helm",
            "Herefara",
            "Herubrand",
            "Herumer",
            "Heruthain",
            "Heruwine",
            "Holdred",
            "Holdwine",
            "Léod",
            "Léofa",
            "Léofara",
            "Léofred",
            "Léofwine",
            "Léonere",
            "Wídfara",
            "Walda",
            "Wulf",
        ];
        const F1: [&str; 41] = [
            "Éad", "Éor", "Ald", "Bald", "Bey", "Ceol", "Cyne", "Déor", "Dún", "Dere", "Dern",
            "Eó", "Ea", "Ead", "Eal", "Elf", "Folc", "Frum", "Gam", "Gar", "Gléo", "God", "Gold",
            "Guth", "Há", "Héo", "Here", "Heru", "Hold", "Léo", "Léof", "Leof", "Maer", "Maet",
            "Sae", "Somer", "Théo", "Théod", "Tid", "Wíd", "Waer",
        ];
        const F2: [&str; 26] = [
            "burh", "dis", "doina", "gyth", "hild", "ith", "lid", "lida", "lith", "nild", "rid",
            "rith", "run", "ryth", "wara", "well", "wen", "wena", "wine", "wyn", "wyn", "wyn",
            "wyn", "hild", "hild", "hild",
        ];

        let mut rng = rand::thread_rng();
        let name: String = if gender == Genders::Female {
            (*F1.choose(&mut rng).unwrap()).to_owned() + F2.choose(&mut rng).unwrap()
        } else {
            const PROB: f32 = 0.5;
            let prob: f32 = rng.gen();
            if prob < PROB {
                (*M1.choose(&mut rng).unwrap()).to_owned() + M2.choose(&mut rng).unwrap()
            } else {
                (*M3.choose(&mut rng).unwrap()).to_owned()
            }
        };
        uppercase_first_letter(&name)
    }
}
