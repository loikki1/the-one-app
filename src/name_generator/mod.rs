use rocket::request::FromParam;
use serde::{Deserialize, Serialize};
use strum::EnumIter;
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

use self::balrog::BalrogNameGenerator;
use self::beorning::BeorningNameGenerator;
use self::bree::BreeNameGenerator;
use self::dale::DaleNameGenerator;
use self::druedain::DruedainNameGenerator;
use self::dunlending::DunlendingNameGenerator;
use self::dwarf::DwarfNameGenerator;
use self::easterling::EasterlingNameGenerator;
use self::haradrim::HaradrimNameGenerator;
use self::hobbit::HobbitNameGenerator;
use self::maiar::MaiarNameGenerator;
use self::orc::OrcNameGenerator;
use self::quenya::QuenyaNameGenerator;
use self::rohirrim::RohirrimNameGenerator;
use self::sindarin::SindarinNameGenerator;

pub mod balrog;
pub mod beorning;
pub mod bree;
pub mod dale;
pub mod druedain;
pub mod dunlending;
pub mod dwarf;
pub mod easterling;
pub mod haradrim;
pub mod hobbit;
pub mod maiar;
pub mod orc;
pub mod quenya;
pub mod rohirrim;
pub mod sindarin;

#[derive(Copy, Clone, Debug, Serialize, Deserialize, FromParamEnum, EnumIter, TS)]
#[ts(export)]
pub enum NameGenerators {
    Balrog,
    Beorning,
    Bree,
    Dale,
    Druedain,
    Dunlending,
    Dwarf,
    Easterling,
    Haradrim,
    Hobbit,
    Maiar,
    Orc,
    Quenya,
    Rohirrim,
    Sindarin,
}

#[derive(PartialEq, Copy, Clone, Debug, Serialize, Deserialize, FromParamEnum, TS)]
#[ts(export)]
pub enum Genders {
    Male,
    Female,
    Neutral,
}

pub fn get_generator(generator: NameGenerators) -> Box<dyn NameGenerator> {
    match generator {
        NameGenerators::Balrog => Box::new(BalrogNameGenerator {}),
        NameGenerators::Beorning => Box::new(BeorningNameGenerator {}),
        NameGenerators::Bree => Box::new(BreeNameGenerator {}),
        NameGenerators::Dale => Box::new(DaleNameGenerator {}),
        NameGenerators::Druedain => Box::new(DruedainNameGenerator {}),
        NameGenerators::Dunlending => Box::new(DunlendingNameGenerator {}),
        NameGenerators::Dwarf => Box::new(DwarfNameGenerator {}),
        NameGenerators::Easterling => Box::new(EasterlingNameGenerator {}),
        NameGenerators::Haradrim => Box::new(HaradrimNameGenerator {}),
        NameGenerators::Hobbit => Box::new(HobbitNameGenerator {}),
        NameGenerators::Maiar => Box::new(MaiarNameGenerator {}),
        NameGenerators::Orc => Box::new(OrcNameGenerator {}),
        NameGenerators::Quenya => Box::new(QuenyaNameGenerator {}),
        NameGenerators::Rohirrim => Box::new(RohirrimNameGenerator {}),
        NameGenerators::Sindarin => Box::new(SindarinNameGenerator {}),
    }
}

pub trait NameGenerator {
    fn get_name(&self, gender: Genders) -> String;
    fn get_genders(&self) -> Vec<Genders> {
        vec![Genders::Male, Genders::Female]
    }
}

#[cfg(test)]
mod test {
    use strum::IntoEnumIterator as _;

    use crate::name_generator::get_generator;

    use super::{Genders, NameGenerators};

    #[test]
    fn check_name_generator() {
        for generator in NameGenerators::iter() {
            eprintln!("{generator:?}");
            let generator = get_generator(generator);
            for _ in 1..10 {
                generator.get_name(Genders::Male);
                generator.get_name(Genders::Female);
                generator.get_name(Genders::Neutral);
            }
        }
    }
}
