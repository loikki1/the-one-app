use rand::seq::SliceRandom as _;
use serde::Deserialize;
use std::str::from_utf8;

use crate::assets::Assets;
use crate::name_generator::NameGenerator;

use super::Genders;

const FILENAME: &str = "name_generator/hobbit.json";

#[derive(Deserialize)]
struct Data {
    pub female: Vec<String>,
    pub male: Vec<String>,
    pub family: Vec<String>,
}

pub struct HobbitNameGenerator;

impl HobbitNameGenerator {
    fn get_data() -> Data {
        let file = Assets::get(FILENAME).unwrap();
        let content = from_utf8(&file.data).unwrap();
        serde_json::from_str(content).unwrap()
    }
}

impl NameGenerator for HobbitNameGenerator {
    fn get_name(&self, gender: Genders) -> String {
        let data = HobbitNameGenerator::get_data();
        let mut rng = rand::thread_rng();
        let first = if gender == Genders::Male {
            data.male.choose(&mut rng).unwrap()
        } else {
            data.female.choose(&mut rng).unwrap()
        }
        .to_string();

        first + " " + data.family.choose(&mut rng).unwrap()
    }
}
