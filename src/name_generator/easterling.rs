use rand::seq::SliceRandom as _;

use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

pub struct EasterlingNameGenerator;

impl NameGenerator for EasterlingNameGenerator {
    fn get_name(&self, gender: Genders) -> String {
        let male = gender == Genders::Male;

        let l1: Vec<&str> = if male {
            vec!["", "b", "br", "d", "g", "k", "m", "n", "s", "y"]
        } else {
            vec!["", "", "d", "g", "k", "m", "n", "r", "t", "v", "y", "z"]
        };

        let l2: Vec<&str> = if male {
            vec!["ô", "ö", "o", "e", "i", "a", "u"]
        } else {
            vec!["a", "u", "î", "i", "a", "u", "î", "i", "e", "o"]
        };
        let l3: Vec<&str> = if male {
            vec![
                "b", "c", "dd", "dg", "kt", "l", "ld", "lf", "lt", "lw", "m", "mr", "r", "rg",
                "rl", "rth", "st", "z",
            ]
        } else {
            vec!["g", "gk", "l", "lk", "n", "r", "rk", "t", "z"]
        };
        let l4: Vec<&str> = if male {
            vec!["a", "i", "î", "ö", "o", "u", "ü"]
        } else {
            vec!["a", "e", "i", "u"]
        };
        let l5: Vec<&str> = if male {
            vec![
                "", "", "ç", "ch", "d", "g", "k", "l", "n", "nd", "ng", "r", "rth", "st", "z",
            ]
        } else {
            vec!["", "", "l", "n", "g", "ge", "ke", "le", "n", "ne", "z"]
        };

        let mut rng = rand::thread_rng();
        let str1 = *l1.choose(&mut rng).unwrap();
        let str2 = *l2.choose(&mut rng).unwrap();
        let str4 = *l4.choose(&mut rng).unwrap();
        let str5 = *l5.choose(&mut rng).unwrap();

        loop {
            let str3 = l3.choose(&mut rng).unwrap();
            if *str3 != str1 && *str3 != str5 {
                let name = str1.to_owned() + str2 + str3 + str4 + str5;
                return uppercase_first_letter(&name);
            }
        }
    }
}
