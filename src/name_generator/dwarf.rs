use rand::seq::SliceRandom as _;
use rand::Rng as _;

use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

pub struct DwarfNameGenerator;

impl NameGenerator for DwarfNameGenerator {
    fn get_name(&self, _: Genders) -> String {
        const L1: [&str; 21] = [
            "b", "br", "d", "dr", "dw", "f", "fl", "fr", "g", "gl", "gr", "k", "kh", "kr", "l",
            "m", "mh", "n", "t", "th", "thr",
        ];
        const L2: [&str; 5] = ["a", "e", "i", "o", "u"];
        const L3: [&str; 20] = [
            "b", "f", "fr", "l", "lb", "lr", "lv", "m", "mb", "ml", "mr", "n", "nd", "nr", "r",
            "rb", "rl", "rv", "s", "sr",
        ];
        const L4: [&str; 4] = ["k", "m", "n", "r"];
        const L5: [&str; 7] = ["a", "ai", "e", "i", "o", "oi", "u"];
        const L6: [&str; 9] = ["b", "d", "f", "g", "k", "l", "m", "n", "t"];
        const L7: [&str; 9] = ["a", "e", "i", "o", "u", "", "", "", ""];
        const LOW_PROB: f32 = 0.5;

        let mut rng = rand::thread_rng();
        let mut name = (*L1.choose(&mut rng).unwrap()).to_owned();
        let prob: f32 = rng.gen();
        if prob < LOW_PROB {
            name += L2.choose(&mut rng).unwrap();
            name += L3.choose(&mut rng).unwrap();
            name += L2.choose(&mut rng).unwrap();
            name += L4.choose(&mut rng).unwrap();
        } else {
            name += L5.choose(&mut rng).unwrap();
            name += L6.choose(&mut rng).unwrap();
            name += L7.choose(&mut rng).unwrap();
        }
        uppercase_first_letter(&name)
    }

    fn get_genders(&self) -> Vec<Genders> {
        vec![]
    }
}
