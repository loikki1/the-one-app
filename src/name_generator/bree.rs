use rand::seq::SliceRandom as _;
use serde::Deserialize;
use std::str::from_utf8;

use crate::assets::Assets;
use crate::name_generator::NameGenerator;

use super::Genders;

const FILENAME: &str = "name_generator/bree.json";

#[derive(Deserialize)]
struct Data {
    pub female: Vec<String>,
    pub male: Vec<String>,
}

pub struct BreeNameGenerator;

impl BreeNameGenerator {
    fn get_data() -> Data {
        let file = Assets::get(FILENAME).unwrap();
        let content = from_utf8(&file.data).unwrap();
        serde_json::from_str(content).unwrap()
    }
}

impl NameGenerator for BreeNameGenerator {
    fn get_name(&self, gender: Genders) -> String {
        let data = BreeNameGenerator::get_data();
        let mut rng = rand::thread_rng();
        if gender == Genders::Male {
            data.male.choose(&mut rng).unwrap().clone()
        } else {
            data.female.choose(&mut rng).unwrap().clone()
        }
    }
}
