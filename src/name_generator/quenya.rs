use std::collections::HashMap;
use std::str::from_utf8;

use rand::seq::{IteratorRandom as _, SliceRandom as _};
use rand::Rng as _;
use serde::Deserialize;
use serde_json;

use crate::assets::Assets;
use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

pub struct QuenyaNameGenerator;

const FILENAME: &str = "name_generator/quenya.json";

#[derive(Deserialize)]
struct Data {
    pub first: HashMap<String, String>,
    pub second: HashMap<String, String>,
}

struct Complement {
    pub extension: &'static str,
    pub description: &'static str,
    pub extension_with_liaison: &'static str,
}

impl Complement {
    pub fn new(
        extension: &'static str,
        description: &'static str,
        extension_with_liaison: &'static str,
    ) -> Self {
        Self {
            extension,
            description,
            extension_with_liaison,
        }
    }
}

impl QuenyaNameGenerator {
    fn get_data() -> Data {
        let file = Assets::get(FILENAME).unwrap();
        let content = from_utf8(&file.data).unwrap();
        serde_json::from_str(content).unwrap()
    }

    fn get_complement_second_female(key: &mut String) -> Vec<Complement> {
        if key.ends_with('a') {
            if key.ends_with("ya") {
                key.pop();
                key.pop();
                vec![
                    Complement::new("rë", "Woman who does", "yarë"),
                    Complement::new("indë", "Woman who does", "yinde"),
                    Complement::new("llë", "Woman who does", "yallë"),
                    Complement::new("më", "Woman who does", "yamë"),
                    Complement::new("ë", "Woman who does", "ë"),
                    Complement::new("issë", "Woman who does", "yisse"),
                ]
            } else {
                key.pop();
                vec![
                    Complement::new("rë", "Woman who does", "arë"),
                    Complement::new("indë", "Woman who does", "inde"),
                    Complement::new("llë", "Woman who does", "allë"),
                    Complement::new("më", "Woman who does", "amë"),
                    Complement::new("ë", "Woman who does", "ë"),
                    Complement::new("issë", "Woman who does", "isse"),
                ]
            }
        } else if key.ends_with('c')
            || key.ends_with('l')
            || key.ends_with('n')
            || key.ends_with('m')
            || key.ends_with('p')
            || key.ends_with("th")
            || key.ends_with('v')
        {
            vec![
                Complement::new("rë", "Woman who does", "rë"),
                Complement::new("indë", "Woman who does", "inde"),
                Complement::new("më", "Woman who does", "më"),
                Complement::new("ë", "Woman who does", "ë"),
                Complement::new("issë", "Woman who does", "isse"),
            ]
        } else if key.ends_with('r') {
            vec![
                Complement::new("rë", "Woman who does", "rë"),
                Complement::new("indë", "Woman who does", "inde"),
                Complement::new("më", "Woman who does", "më"),
                Complement::new("ë", "Woman who does", "të"),
                Complement::new("issë", "Woman who does", "isse"),
            ]
        } else if key.ends_with('t') || key.ends_with('h') {
            key.pop();
            vec![
                Complement::new("rë", "Woman who does", "rë"),
                Complement::new("indë", "Woman who does", "inde"),
                Complement::new("më", "Woman who does", "më"),
                Complement::new("ë", "Woman who does", "cë"),
                Complement::new("issë", "Woman who does", "isse"),
            ]
        } else {
            panic!("This should not happen");
        }
    }

    fn get_complement_second(key: &mut String, gender: Genders) -> Vec<Complement> {
        if gender == Genders::Female {
            QuenyaNameGenerator::get_complement_second_female(key)
        } else if gender == Genders::Male {
            if key.ends_with('a') {
                key.pop();
                vec![
                    Complement::new("ro", "Man who does", "aro"),
                    Complement::new("o", "Man who does", "o"),
                    Complement::new("no", "Man who does", "ano"),
                    Complement::new("mo", "Man who does", "amo"),
                ]
            } else if key.ends_with('c')
                || key.ends_with('m')
                || key.ends_with('p')
                || key.ends_with("th")
                || key.ends_with('v')
            {
                vec![
                    Complement::new("ro", "Man who does", "ro"),
                    Complement::new("o", "Man who does", "o"),
                    Complement::new("no", "Man who does", "no"),
                    Complement::new("mo", "Man who does", "mo"),
                ]
            } else if key.ends_with('c')
                || key.ends_with('l')
                || key.ends_with('n')
                || key.ends_with('r')
            {
                vec![
                    Complement::new("ro", "Man who does", "ro"),
                    Complement::new("o", "Man who does", "o"),
                    Complement::new("no", "Man who does", "do"),
                    Complement::new("mo", "Man who does", "mo"),
                ]
            } else if key.ends_with('t') {
                key.pop();
                vec![
                    Complement::new("ro", "Man who does", "tro"),
                    Complement::new("o", "Man who does", "co"),
                    Complement::new("no", "Man who does", "tno"),
                    Complement::new("mo", "Man who does", "tmo"),
                ]
            } else {
                panic!("This should not happen");
            }
        } else if key.ends_with('a') {
            vec![
                Complement::new("r", "Person who does", "r"),
                Complement::new("", "", ""),
            ]
        } else {
            vec![Complement::new("", "", "")]
        }
    }

    #[expect(clippy::too_many_lines)]
    fn get_complement_first_female(key: &mut String) -> Vec<Complement> {
        if key.ends_with('a') {
            if key.ends_with("ya") {
                key.pop();
                key.pop();
                vec![
                    Complement::new("më", "Woman who does", "yamë"),
                    Complement::new("ë", "Woman who does", "yë"),
                    Complement::new("ië", "Feminine", "ië"),
                    Complement::new("ien", "Feminine", "ien"),
                    Complement::new("issë", "Woman who does", "yissë"),
                    Complement::new("wendë", "Maiden", "yawen"),
                    Complement::new("níssë", "Female", "yanis"),
                    Complement::new("iel", "Daughter of", "iel"),
                ]
            } else {
                key.pop();
                vec![
                    Complement::new("më", "Woman who does", "amë"),
                    Complement::new("ë", "Woman who does", "ë"),
                    Complement::new("ië", "Feminine", "ië"),
                    Complement::new("ien", "Feminine", "ien"),
                    Complement::new("issë", "Woman who does", "issë"),
                    Complement::new("wendë", "Maiden", "awen"),
                    Complement::new("níssë", "Female", "anis"),
                    Complement::new("iel", "Daughter of", "iel"),
                ]
            }
        } else if key.ends_with('e') || key.ends_with('ë') {
            key.pop();
            vec![
                Complement::new("më", "Woman who does", "emë"),
                Complement::new("ë", "Woman who does", "ë"),
                Complement::new("ië", "Feminine", "ië"),
                Complement::new("ien", "Feminine", "ien"),
                Complement::new("issë", "Woman who does", "issë"),
                Complement::new("wendë", "Maiden", "ewen"),
                Complement::new("níssë", "Female", "enis"),
                Complement::new("iel", "Daughter of", "iel"),
            ]
        } else if key.ends_with('o') {
            key.pop();
            vec![
                Complement::new("më", "Woman who does", "omë"),
                Complement::new("ë", "Woman who does", "ë"),
                Complement::new("ië", "Feminine", "ië"),
                Complement::new("issë", "Woman who does", "issë"),
                Complement::new("wendë", "Maiden", "owen"),
                Complement::new("níssë", "Female", "onis"),
                Complement::new("iel", "Daughter of", "iel"),
            ]
        } else if key.ends_with('u') {
            key.pop();
            vec![
                Complement::new("më", "Woman who does", "umë"),
                Complement::new("ë", "Woman who does", "ë"),
                Complement::new("ië", "Feminine", "ië"),
                Complement::new("ien", "Feminine", "ien"),
                Complement::new("issë", "Woman who does", "issë"),
                Complement::new("wendë", "Maiden", "uwen"),
                Complement::new("níssë", "Female", "unis"),
                Complement::new("iel", "Daughter of", "iel"),
            ]
        } else if key.ends_with('l') || key.ends_with('n') {
            vec![
                Complement::new("më", "Woman who does", "më"),
                Complement::new("ë", "Woman who does", "ë"),
                Complement::new("ië", "Feminine", "ië"),
                Complement::new("ien", "Feminine", "ien"),
                Complement::new("issë", "Woman who does", "isse"),
                Complement::new("wendë", "Maiden", "wen"),
                Complement::new("níssë", "Female", "dis"),
                Complement::new("iel", "Daughter of", "iel"),
            ]
        } else if key.ends_with('r') {
            vec![
                Complement::new("më", "Woman who does", "më"),
                Complement::new("ë", "Woman who does", "të"),
                Complement::new("ië", "Feminine", "tië"),
                Complement::new("ien", "Feminine", "tien"),
                Complement::new("issë", "Woman who does", "isse"),
                Complement::new("wendë", "Maiden", "wen"),
                Complement::new("níssë", "Female", "dis"),
                Complement::new("iel", "Daughter of", "tiel"),
            ]
        } else if key.ends_with('s') {
            key.pop();
            vec![
                Complement::new("më", "Woman who does", "smë"),
                Complement::new("ë", "Woman who does", "rë"),
                Complement::new("ië", "Feminine", "rië"),
                Complement::new("ien", "Feminine", "rien"),
                Complement::new("issë", "Woman who does", "risse"),
                Complement::new("wendë", "Maiden", "swen"),
                Complement::new("níssë", "Female", "snis"),
                Complement::new("iel", "Daughter of", "riel"),
            ]
        } else if key.ends_with("th") || key.ends_with('v') {
            vec![
                Complement::new("më", "Woman who does", "më"),
                Complement::new("ë", "Woman who does", "ë"),
                Complement::new("ië", "Feminine", "ië"),
                Complement::new("ien", "Feminine", "ien"),
                Complement::new("issë", "Woman who does", "isse"),
                Complement::new("wendë", "Maiden", "wen"),
                Complement::new("níssë", "Female", "nis"),
                Complement::new("iel", "Daughter of", "iel"),
            ]
        } else if key.ends_with('t') {
            key.pop();
            vec![
                Complement::new("më", "Woman who does", "tmë"),
                Complement::new("ë", "Woman who does", "cë"),
                Complement::new("ië", "Feminine", "cië"),
                Complement::new("ien", "Feminine", "cien"),
                Complement::new("issë", "Woman who does", "cisse"),
                Complement::new("wendë", "Maiden", "twen"),
                Complement::new("níssë", "Female", "tnis"),
                Complement::new("iel", "Daughter of", "ciel"),
            ]
        } else {
            panic!("This should not happen");
        }
    }

    #[expect(clippy::too_many_lines)]
    fn get_complement_first_male(key: &mut String) -> Vec<Complement> {
        if key.ends_with('a') {
            if key.ends_with("ya") {
                key.pop();
                key.pop();
                vec![
                    Complement::new("o", "Man who does", "yo"),
                    Complement::new("on", "Masculine", "yon"),
                    Complement::new("no", "Man who does", "yano"),
                    Complement::new("mo", "Man who does", "yamo"),
                    Complement::new("nér", "Masculine", "yaner"),
                    Complement::new("ion", "Son of", "ion"),
                ]
            } else {
                key.pop();
                vec![
                    Complement::new("o", "Man who does", "o"),
                    Complement::new("on", "Masculine", "on"),
                    Complement::new("no", "Man who does", "ano"),
                    Complement::new("mo", "Man who does", "amo"),
                    Complement::new("nér", "Masculine", "aner"),
                    Complement::new("ion", "Son of", "ion"),
                ]
            }
        } else if key.ends_with('e') || key.ends_with('ë') {
            key.pop();
            vec![
                Complement::new("o", "Man who does", "o"),
                Complement::new("on", "Masculine", "on"),
                Complement::new("no", "Man who does", "eno"),
                Complement::new("mo", "Man who does", "emo"),
                Complement::new("nér", "Masculine", "ener"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("", "", ""),
                Complement::new("", "", ""),
                Complement::new("", "", ""),
            ]
        } else if key.ends_with('o') {
            key.pop();
            vec![
                Complement::new("o", "Man who does", "o"),
                Complement::new("on", "Masculine", "on"),
                Complement::new("no", "Man who does", "ono"),
                Complement::new("mo", "Man who does", "omo"),
                Complement::new("nér", "Masculine", "oner"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("", "", ""),
                Complement::new("", "", ""),
                Complement::new("", "", ""),
            ]
        } else if key.ends_with('u') {
            key.pop();
            vec![
                Complement::new("o", "Man who does", "o"),
                Complement::new("on", "Masculine", "on"),
                Complement::new("no", "Man who does", "uno"),
                Complement::new("mo", "Man who does", "umo"),
                Complement::new("nér", "Masculine", "uner"),
                Complement::new("ion", "Son of", "ion"),
            ]
        } else if key.ends_with('l') || key.ends_with('n') {
            vec![
                Complement::new("o", "Man who does", "o"),
                Complement::new("on", "Masculine", "on"),
                Complement::new("no", "Man who does", "do"),
                Complement::new("mo", "Man who does", "mo"),
                Complement::new("nér", "Masculine", "der"),
                Complement::new("ion", "Son of", "ion"),
            ]
        } else if key.ends_with('r') {
            vec![
                Complement::new("o", "Man who does", "no"),
                Complement::new("on", "Masculine", "non"),
                Complement::new("no", "Man who does", "do"),
                Complement::new("mo", "Man who does", "mo"),
                Complement::new("nér", "Masculine", "der"),
                Complement::new("ion", "Son of", "nion"),
            ]
        } else if key.ends_with('s') {
            key.pop();
            vec![
                Complement::new("o", "Man who does", "ro"),
                Complement::new("on", "Masculine", "ron"),
                Complement::new("no", "Man who does", "sno"),
                Complement::new("mo", "Man who does", "smo"),
                Complement::new("nér", "Masculine", "sner"),
                Complement::new("ion", "Son of", "rion"),
            ]
        } else if key.ends_with('t') {
            key.pop();
            vec![
                Complement::new("o", "Man who does", "co"),
                Complement::new("on", "Masculine", "con"),
                Complement::new("no", "Man who does", "tno"),
                Complement::new("mo", "Man who does", "tmo"),
                Complement::new("nér", "Masculine", "tner"),
                Complement::new("ion", "Son of", "cion"),
            ]
        } else if key.ends_with("th") || key.ends_with('v') {
            vec![
                Complement::new("o", "Man who does", "o"),
                Complement::new("on", "Masculine", "on"),
                Complement::new("no", "Man who does", "no"),
                Complement::new("mo", "Man who does", "mo"),
                Complement::new("nér", "Masculine", "ner"),
                Complement::new("ion", "Son of", "ion"),
            ]
        } else {
            panic!("This should not happen");
        }
    }

    fn get_complement_first(key: &mut String, gender: Genders) -> Vec<Complement> {
        if gender == Genders::Female {
            QuenyaNameGenerator::get_complement_first_female(key)
        } else if gender == Genders::Male {
            QuenyaNameGenerator::get_complement_first_male(key)
        } else {
            if key.ends_with('s') {
                key.pop();
            }
            if key.ends_with('ë') {
                vec![
                    Complement::new("dur", "Servant of", "edur"),
                    Complement::new("dil", "Friend of", "edil"),
                    Complement::new("nil", "Friend of", "enil"),
                    Complement::new("quen", "Person", "equen"),
                    Complement::new("wë", "Person", "ewë"),
                    Complement::new("", "", ""),
                ]
            } else {
                vec![
                    Complement::new("dur", "Servant of", "dur"),
                    Complement::new("dil", "Friend of", "dil"),
                    Complement::new("nil", "Friend of", "nil"),
                    Complement::new("quen", "Person", "quen"),
                    Complement::new("wë", "Person", "wë"),
                    Complement::new("", "", ""),
                ]
            }
        }
    }
}

impl NameGenerator for QuenyaNameGenerator {
    fn get_name(&self, gender: Genders) -> String {
        // Prepare random numbers
        const PROB: f32 = 0.5;

        let mut rng = rand::thread_rng();
        let prob: f32 = rng.gen();

        // Get data
        let data = QuenyaNameGenerator::get_data();
        let data = if prob < PROB { data.first } else { data.second };

        // Sample
        let mut key = data.keys().choose(&mut rng).unwrap().clone();
        let desc = &data[&key];
        let binding = if prob < PROB {
            QuenyaNameGenerator::get_complement_first(&mut key, gender)
        } else {
            QuenyaNameGenerator::get_complement_second(&mut key, gender)
        };
        let complement = binding.choose(&mut rng).unwrap();

        // Prepare output
        let name = key.clone() + complement.extension_with_liaison;
        let desc1 = key.clone() + " (" + desc;
        let desc2 = if complement.extension_with_liaison.is_empty() {
            ")".to_owned()
        } else {
            ") + ".to_owned() + complement.extension + " (" + complement.description + ")"
        };
        uppercase_first_letter(&name) + " = " + &desc1 + &desc2
    }

    fn get_genders(&self) -> Vec<Genders> {
        vec![Genders::Male, Genders::Female, Genders::Neutral]
    }
}
