use rand::seq::SliceRandom as _;
use rand::Rng as _;

use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

pub struct BalrogNameGenerator;

impl NameGenerator for BalrogNameGenerator {
    fn get_name(&self, _: Genders) -> String {
        const L1: [&str; 11] = ["b", "bh", "d", "dh", "g", "h", "kh", "n", "r", "v", "z"];
        const L2: [&str; 11] = ["a", "o", "u", "a", "o", "u", "a", "o", "u", "e", "i"];
        const L3: [&str; 32] = [
            "br", "bn", "dh", "dr", "dhr", "gn", "gr", "gd", "gz", "kn", "kg", "kv", "kz", "ld",
            "lg", "lm", "ln", "lv", "lz", "mz", "mg", "md", "nz", "ng", "nd", "thr", "thm", "tr",
            "zc", "zg", "zk", "zz",
        ];
        const L4: [&str; 9] = ["b", "d", "k", "l", "m", "n", "t", "v", "z"];
        const L5: [&str; 7] = ["c", "d", "g", "k", "l", "n", "r"];

        const LOW_PROB: f32 = 0.6;
        const HIGH_PROB: f32 = 0.8;

        let mut rng = rand::thread_rng();
        let prob: f32 = rng.gen();
        let output = if prob < LOW_PROB {
            let mut name = String::from(*L1.choose(&mut rng).unwrap());
            name += L2.choose(&mut rng).unwrap();
            name += L3.choose(&mut rng).unwrap();
            name += L2.choose(&mut rng).unwrap();
            name += L5.choose(&mut rng).unwrap();
            name
        } else if prob < HIGH_PROB {
            let mut name = String::from(*L1.choose(&mut rng).unwrap());
            name += L2.choose(&mut rng).unwrap();
            name += L3.choose(&mut rng).unwrap();
            name += L2.choose(&mut rng).unwrap();
            name += L4.choose(&mut rng).unwrap();
            name += L2.choose(&mut rng).unwrap();
            name += L5.choose(&mut rng).unwrap();
            name
        } else {
            let mut name = String::from(*L1.choose(&mut rng).unwrap());
            name += L2.choose(&mut rng).unwrap();
            name += L4.choose(&mut rng).unwrap();
            name += L2.choose(&mut rng).unwrap();
            name += L3.choose(&mut rng).unwrap();
            name += L2.choose(&mut rng).unwrap();
            name += L5.choose(&mut rng).unwrap();
            name
        };
        uppercase_first_letter(&output)
    }

    fn get_genders(&self) -> Vec<Genders> {
        vec![]
    }
}
