use rand::seq::SliceRandom as _;

use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;
const SKIP: usize = 6;

pub struct MaiarNameGenerator;

impl NameGenerator for MaiarNameGenerator {
    fn get_name(&self, gender: Genders) -> String {
        let male = gender == Genders::Male;

        let l1: Vec<&str> = if male {
            vec!["c", "k", "l", "m", "n", "p", "r", "s", "t", "th", "", ""]
        } else {
            vec!["f", "l", "m", "n", "ph", "s", "sh", "w", "y", "z", "", ""]
        };

        let l2: Vec<&str> = if male {
            vec![
                "a", "e", "o", "i", "u", "ó", "é", "ai", "eo", "io", "eö", "uo", "ua",
            ]
        } else {
            vec![
                "a", "e", "o", "i", "u", "ó", "é", "ie", "ui", "ia", "ea", "ae", "ua",
            ]
        };

        let l3: Vec<&str> = if male {
            vec![
                "l", "ll", "lm", "ln", "ls", "m", "md", "n", "nd", "nm", "nw", "r", "s", "ss", "t",
                "w",
            ]
        } else {
            vec![
                "l", "lm", "ln", "ls", "n", "nn", "ph", "r", "s", "sh", "ss", "th",
            ]
        };

        let l4: Vec<&str> = if male {
            vec!["l", "m", "n", "nd", "r", "s", "t", "th"]
        } else {
            vec!["r", "n", "s", "th", "l", "m"]
        };

        let l5: Vec<&str> = if male {
            vec!["o", "e", "ë", "ó", "", "", "", ""]
        } else {
            vec!["a", "e", "ë", "é", "ó", "", "", "", "", ""]
        };

        let mut rng = rand::thread_rng();

        let str1 = *l1.choose(&mut rng).unwrap();
        let str2 = *l2.choose(&mut rng).unwrap();
        let str3 = *l3.choose(&mut rng).unwrap();
        let str4 = *l4.choose(&mut rng).unwrap();
        let str5 = *l5.choose(&mut rng).unwrap();

        let mut str2_2 = l2.choose(&mut rng).unwrap();
        if l2.iter().position(|&x| x == str2).unwrap() > SKIP {
            while l2.iter().position(|&x| x == *str2_2).unwrap() > SKIP {
                str2_2 = l2.choose(&mut rng).unwrap();
            }
        }

        let name = str1.to_owned() + str2 + str3 + str2_2 + str4 + str5;
        uppercase_first_letter(&name)
    }
}
