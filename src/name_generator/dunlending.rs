use rand::seq::SliceRandom as _;
use rand::Rng as _;

use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

const LOW_PROB: f32 = 0.25;
const HIGH_PROB: f32 = 0.75;

pub struct DunlendingNameGenerator;

impl NameGenerator for DunlendingNameGenerator {
    fn get_name(&self, gender: Genders) -> String {
        let male = gender == Genders::Male;

        let l1: Vec<&str> = if male {
            vec![
                "br", "c", "d", "dr", "g", "gr", "h", "m", "r", "s", "v", "z",
            ]
        } else {
            vec!["b", "c", "d", "h", "m", "n", "s", "v", "w"]
        };

        let l2: Vec<&str> = if male {
            vec!["a", "e", "o"]
        } else {
            vec!["a", "e", "i", "o"]
        };

        let l3: Vec<&str> = if male {
            vec![
                "d", "dd", "dr", "g", "gr", "gd", "ld", "lm", "m", "mn", "n", "nn", "nr", "r",
                "rm", "rn", "rr", "rl", "rs",
            ]
        } else {
            vec![
                "c", "cr", "d", "dr", "g", "l", "m", "mn", "mr", "n", "nr", "r", "rh", "rl", "rth",
                "s", "t",
            ]
        };

        let l4: Vec<&str> = if male {
            vec!["a", "i", "o"]
        } else {
            vec!["a", "e", "i", "o"]
        };

        let l5: Vec<&str> = if male {
            vec!["c", "d", "g", "l", "ld", "lt", "n", "nd", "nt", "rn", "th"]
        } else {
            vec![
                "", "", "", "", "", "", "c", "d", "n", "s", "ld", "ft", "ss", "dh", "rf", "rd",
                "rn", "th",
            ]
        };

        let output;
        let mut rng = rand::thread_rng();
        let str1 = *l1.choose(&mut rng).unwrap();
        let str2 = *l2.choose(&mut rng).unwrap();
        let str5 = *l5.choose(&mut rng).unwrap();

        let prob: f32 = rng.gen();
        if prob < LOW_PROB {
            const SKIP: usize = 4;
            loop {
                let str5 = l5.choose(&mut rng).unwrap();
                if l5.iter().position(|&x| x == *str5).unwrap() >= SKIP {
                    output = str1.to_owned() + str2 + str5;
                    break;
                }
            }
        } else {
            let str4 = l4.choose(&mut rng).unwrap();
            loop {
                let str3 = l3.choose(&mut rng).unwrap();
                if *str3 != str1 && *str3 != str5 {
                    let prob: f32 = rng.gen();
                    if prob < HIGH_PROB {
                        output = str1.to_owned() + str2 + str3 + str4 + str5;
                        break;
                    }
                    output = str1.to_owned() + str2 + str3 + str4 + "doc";
                    break;
                }
            }
        }
        uppercase_first_letter(&output)
    }
}
