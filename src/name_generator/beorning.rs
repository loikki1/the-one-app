use rand::seq::SliceRandom as _;

use crate::name_generator::NameGenerator;

use super::Genders;

pub struct BeorningNameGenerator;

impl NameGenerator for BeorningNameGenerator {
    fn get_name(&self, gender: Genders) -> String {
        const L1: [&str; 83] = [
            "Ac", "Ag", "Ald", "Aln", "Aran", "Arn", "Ba", "Bar", "Bald", "Bear", "Beorn", "Beran",
            "Borg", "Both", "Brer", "Dag", "Darn", "Dreng", "Dug", "Eld", "Erad", "Eran", "Ern",
            "Fer", "Forn", "Frid", "Froth", "Gal", "Glum", "Gluth", "Grim", "Har", "Hart", "Heim",
            "Heor", "Hroth", "Ig", "Ingel", "Is", "Iw", "Jal", "Jar", "Jarn", "Jorn", "Log", "Lor",
            "Lyd", "Lyth", "Mag", "Mar", "Morn", "Moth", "Nard", "Ned", "Nef", "Nor", "Old", "Ord",
            "Ot", "Oth", "Rand", "Rath", "Raeg", "Ric", "Rod", "Sceot", "Sig", "Skal", "Skol",
            "Stig", "Tar", "Theod", "Thor", "Throt", "Treo", "Val", "Vald", "Vig", "Vul", "Wal",
            "Wald", "Wid", "Wul",
        ];
        const L2: [&str; 64] = [
            "ac", "ald", "angar", "ard", "aric", "bald", "bar", "beorn", "bert", "bold", "brand",
            "dac", "dar", "dhor", "dam", "dan", "fald", "fara", "fast", "forn", "gac", "geir",
            "gils", "grim", "hame", "har", "helm", "here", "hyrde", "kald", "kar", "karl", "kin",
            "mar", "mark", "moth", "mund", "ohd", "ond", "or", "oric", "rand", "rath", "rek",
            "ric", "rot", "sel", "sorn", "stin", "styr", "tan", "tar", "taric", "thorn", "torn",
            "treo", "var", "vat", "vir", "vith", "wald", "war", "wine", "wulf",
        ];
        const L3: [&str; 87] = [
            "Ac", "Aer", "Amal", "Arin", "Ava", "Bar", "Bear", "Beorn", "Bera", "Beran", "Birn",
            "Bog", "Brer", "Bruni", "Din", "Dis", "Dom", "Dyr", "Eir", "Esil", "Eth", "Ey", "Fast",
            "Faye", "Feor", "Fyn", "Gail", "Gel", "Gerth", "Gis", "Grim", "Gud", "Hall", "Har",
            "Hild", "Hrim", "Huld", "Hun", "Ilin", "Ingi", "Ior", "Is", "Jen", "Jern", "Jil",
            "Jor", "Kat", "Kath", "Kay", "Kyn", "Leot", "Lif", "Lin", "Lyn", "Maer", "Mag", "Mar",
            "Mel", "Nel", "Nor", "Nyr", "Od", "Ol", "Ovi", "Ovin", "Raeg", "Raen", "Ran", "Rhon",
            "Ril", "Sal", "Sig", "Sol", "Svan", "Treo", "Ul", "Ulin", "Ulvin", "Una", "Vel", "Ven",
            "Vil", "Vyl", "Waen", "Wen", "Win", "Wyl",
        ];
        const L4: [&str; 72] = [
            "a", "aen", "aeya", "anda", "ara", "ava", "aya", "bi", "bina", "bwyn", "byn", "da",
            "dira", "dis", "dora", "eith", "elde", "ena", "era", "eva", "ewyn", "fast", "firth",
            "frida", "fyn", "garth", "gifu", "ginny", "gun", "helda", "hena", "hera", "hild",
            "hild", "hild", "hild", "hild", "hild", "la", "laug", "lin", "loth", "nida", "nis",
            "nwyn", "ny", "olin", "ora", "otta", "owyn", "rin", "risa", "rlin", "run", "thrith",
            "tina", "tira", "tyn", "vera", "vild", "vor", "vyn", "wed", "wild", "winne", "wyn",
            "wyn", "wyn", "wyn", "wyn", "wyn", "wyn",
        ];

        let mut rng = rand::thread_rng();
        if gender == Genders::Male {
            let mut name = String::from(*L1.choose(&mut rng).unwrap());
            name += L2.choose(&mut rng).unwrap();
            name
        } else {
            let mut name = String::from(*L3.choose(&mut rng).unwrap());
            name += L4.choose(&mut rng).unwrap();
            name
        }
    }
}
