use std::collections::HashMap;

use rand::seq::{IteratorRandom as _, SliceRandom as _};
use rand::Rng as _;
use serde::Deserialize;
use serde_json;
use std::str::from_utf8;

use crate::assets::Assets;
use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

pub struct SindarinNameGenerator;

const FILENAME: &str = "name_generator/sindarin.json";

#[derive(Deserialize)]
struct Data {
    pub first: HashMap<String, String>,
    pub second: HashMap<String, String>,
}

struct Complement {
    pub extension: &'static str,
    pub description: &'static str,
    pub extension_with_liaison: &'static str,
}

impl Complement {
    pub fn new(
        extension: &'static str,
        description: &'static str,
        extension_with_liaison: &'static str,
    ) -> Self {
        Self {
            extension,
            description,
            extension_with_liaison,
        }
    }
}

impl SindarinNameGenerator {
    fn get_data() -> Data {
        let file = Assets::get(FILENAME).unwrap();
        let content = from_utf8(&file.data).unwrap();
        serde_json::from_str(content).unwrap()
    }

    fn get_complement_second_female(key: &mut String) -> Vec<Complement> {
        if key.ends_with('a') || key.ends_with('â') {
            key.pop();
            vec![
                Complement::new("ril", "Female", "ril"),
                Complement::new("dis", "Female", "adis"),
                Complement::new("iell", "Daughter of", "riel"),
                Complement::new("ien", "Daughter of", "rien"),
            ]
        } else if key.ends_with('b') || key.ends_with('h') || key.ends_with('w') {
            vec![
                Complement::new("ril", "Female", "ril"),
                Complement::new("dis", "Female", "edis"),
                Complement::new("iell", "Daughter of", "riel"),
                Complement::new("ien", "Daughter of", "rien"),
            ]
        } else if key.ends_with('d') {
            vec![
                Complement::new("ril", "Female", "ril"),
                Complement::new("dis", "Female", "is"),
                Complement::new("iell", "Daughter of", "issiel"),
                Complement::new("ien", "Daughter of", "issien"),
                Complement::new("iell", "Daughter of", "riel"),
                Complement::new("ien", "Daughter of", "rien"),
            ]
        } else if key.ends_with('f') {
            key.pop();
            vec![
                Complement::new("ril", "Female", "vril"),
                Complement::new("dis", "Female", "vedis"),
                Complement::new("iell", "Daughter of", "vriel"),
                Complement::new("ien", "Daughter of", "vrien"),
            ]
        } else if key.ends_with('g') {
            vec![
                Complement::new("ril", "Female", "ril"),
                Complement::new("dis", "Female", "nis"),
                Complement::new("iell", "Daughter of", "nissiel"),
                Complement::new("ien", "Daughter of", "nissien"),
                Complement::new("iell", "Daughter of", "riel"),
                Complement::new("ien", "Daughter of", "rien"),
            ]
        } else if key.ends_with('l') {
            vec![
                Complement::new("ril", "Female", "lil"),
                Complement::new("dis", "Female", "dis"),
                Complement::new("iell", "Daughter of", "liel"),
                Complement::new("ien", "Daughter of", "lien"),
                Complement::new("iell", "Daughter of", "dissiel"),
                Complement::new("ien", "Daughter of", "dissien"),
            ]
        } else if key.ends_with('n') {
            key.pop();
            vec![
                Complement::new("ril", "Female", "dhril"),
                Complement::new("dis", "Female", "ndis"),
                Complement::new("iell", "Daughter of", "ndissiel"),
                Complement::new("ien", "Daughter of", "ndissien"),
                Complement::new("iell", "Daughter of", "dhriel"),
                Complement::new("ien", "Daughter of", "dhrien"),
            ]
        } else if key.ends_with('r') {
            vec![
                Complement::new("ril", "Female", "il"),
                Complement::new("dis", "Female", "dis"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("ien", "Daughter of", "ien"),
            ]
        } else {
            panic!("Should not happen")
        }
    }

    fn get_complement_second_male(key: &mut String) -> Vec<Complement> {
        if key.ends_with('a') || key.ends_with('â') {
            key.pop();
            vec![
                Complement::new("on", "Male", "on"),
                Complement::new("dir", "Male", "edir"),
                Complement::new("ron", "Male", "on"),
                Complement::new("ion", "Son of", "ion"),
            ]
        } else if key.ends_with('b') || key.ends_with('h') || key.ends_with('w') {
            vec![
                Complement::new("on", "Male", "edon"),
                Complement::new("dir", "Male", "edir"),
                Complement::new("ron", "Male", "ron"),
                Complement::new("ion", "Son of", "rion"),
            ]
        } else if key.ends_with('d') {
            vec![
                Complement::new("on", "Male", "edon"),
                Complement::new("dir", "Male", "ir"),
                Complement::new("ron", "Male", "ron"),
                Complement::new("ion", "Son of", "rion"),
            ]
        } else if key.ends_with('f') {
            key.pop();
            vec![
                Complement::new("dir", "Male", "vedir"),
                Complement::new("ron", "Male", "vron"),
                Complement::new("ion", "Son of", "vrion"),
            ]
        } else if key.ends_with('g') {
            vec![
                Complement::new("ion", "Son of", "nirion"),
                Complement::new("dir", "Male", "nir"),
                Complement::new("ron", "Male", "ron"),
                Complement::new("ion", "Son of", "rion"),
            ]
        } else if key.ends_with('l') {
            vec![
                Complement::new("ion", "Son of", "lion"),
                Complement::new("dir", "Male", "dir"),
                Complement::new("ron", "Male", "lon"),
                Complement::new("ion", "Son of", "dirion"),
            ]
        } else if key.ends_with('n') {
            key.pop();
            vec![
                Complement::new("ion", "Son of", "dhrion"),
                Complement::new("dir", "Male", "ndir"),
                Complement::new("ron", "Male", "dhron"),
                Complement::new("ion", "Son of", "ndirion"),
            ]
        } else if key.ends_with('r') {
            vec![
                Complement::new("ion", "Son of", "ion"),
                Complement::new("dir", "Male", "dir"),
                Complement::new("ron", "Male", "on"),
                Complement::new("ion", "Son of", "dirion"),
            ]
        } else {
            panic!("Should not happen");
        }
    }

    fn get_complement_second(key: &mut String, gender: Genders) -> Vec<Complement> {
        if gender == Genders::Female {
            SindarinNameGenerator::get_complement_second_female(key)
        } else if gender == Genders::Male {
            SindarinNameGenerator::get_complement_second_male(key)
        } else if key.ends_with('f') {
            key.pop();
            vec![Complement::new("or", "Person", "vor")]
        } else {
            if key.ends_with('a') || key.ends_with('â') {
                key.pop();
            }
            vec![Complement::new("or", "Person", "or")]
        }
    }

    #[expect(clippy::too_many_lines)]
    fn get_complement_first_female(key: &mut String) -> Vec<Complement> {
        if key.ends_with('a') || key.ends_with('â') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("sell", "Girl", "ahel"),
                Complement::new("gwend", "Maiden", "awen"),
                Complement::new("neth", "Girl", "aneth"),
                Complement::new("dîs", "Bride", "anis"),
                Complement::new("dess", "Woman", "anes"),
                Complement::new("nîth", "Sister", "anith"),
                Complement::new("thêl", "Sister", "athel"),
                Complement::new("bess", "Wife", "aves"),
            ]
        } else if key.ends_with('e') || key.ends_with('ê') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("sell", "Girl", "ehel"),
                Complement::new("gwend", "Maiden", "ewen"),
                Complement::new("neth", "Girl", "eneth"),
                Complement::new("dîs", "Bride", "enis"),
                Complement::new("dess", "Woman", "enes"),
                Complement::new("nîth", "Sister", "enith"),
                Complement::new("thêl", "Sister", "ethel"),
                Complement::new("bess", "Wife", "eves"),
            ]
        } else if key.ends_with('i') || key.ends_with('í') || key.ends_with('î') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("sell", "Girl", "ihel"),
                Complement::new("gwend", "Maiden", "iwen"),
                Complement::new("neth", "Girl", "ineth"),
                Complement::new("dîs", "Bride", "inis"),
                Complement::new("dess", "Woman", "ines"),
                Complement::new("nîth", "Sister", "inith"),
                Complement::new("thêl", "Sister", "ithel"),
                Complement::new("bess", "Wife", "ives"),
            ]
        } else if key.ends_with('o') || key.ends_with('ô') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("sell", "Girl", "ohel"),
                Complement::new("gwend", "Maiden", "owen"),
                Complement::new("neth", "Girl", "oneth"),
                Complement::new("dîs", "Bride", "onis"),
                Complement::new("dess", "Woman", "ones"),
                Complement::new("nîth", "Sister", "onith"),
                Complement::new("thêl", "Sister", "othel"),
                Complement::new("bess", "Wife", "oves"),
            ]
        } else if key.ends_with('u') || key.ends_with('û') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("sell", "Girl", "uhel"),
                Complement::new("gwend", "Maiden", "uwen"),
                Complement::new("neth", "Girl", "uneth"),
                Complement::new("dîs", "Bride", "unis"),
                Complement::new("dess", "Woman", "unes"),
                Complement::new("nîth", "Sister", "unith"),
                Complement::new("thêl", "Sister", "uthel"),
                Complement::new("bess", "Wife", "uves"),
            ]
        } else if key.ends_with('b') {
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("bess", "Wife", "es"),
            ]
        } else if key.ends_with('c') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "geth"),
                Complement::new("el", "Female", "gel"),
                Complement::new("il", "Female", "gil"),
                Complement::new("ien", "Daughter of", "gien"),
                Complement::new("iell", "Daughter of", "giel"),
                Complement::new("gwend", "Maiden", "gwen"),
            ]
        } else if key.ends_with("nd") {
            key.pop();
            vec![
                Complement::new("eth", "Female", "neth"),
                Complement::new("el", "Female", "nel"),
                Complement::new("il", "Female", "nil"),
                Complement::new("ien", "Daughter of", "nien"),
                Complement::new("iell", "Daughter of", "niel"),
                Complement::new("sell", "Girl", "hel"),
                Complement::new("gwend", "Maiden", "gwen"),
                Complement::new("neth", "Girl", "neth"),
                Complement::new("dîs", "Bride", "dis"),
                Complement::new("dess", "Woman", "des"),
                Complement::new("nîth", "Sister", "nith"),
                Complement::new("thêl", "Sister", "thel"),
                Complement::new("bess", "Wife", "bes"),
            ]
        } else if key.ends_with('d') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "deth"),
                Complement::new("el", "Female", "del"),
                Complement::new("il", "Female", "dil"),
                Complement::new("ien", "Daughter of", "dien"),
                Complement::new("iell", "Daughter of", "diel"),
                Complement::new("sell", "Girl", "ssel"),
                Complement::new("gwend", "Maiden", "dwen"),
                Complement::new("dîs", "Bride", "dis"),
                Complement::new("dess", "Woman", "des"),
            ]
        } else if key.ends_with('f') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "veth"),
                Complement::new("el", "Female", "vel"),
                Complement::new("il", "Female", "vil"),
                Complement::new("ien", "Daughter of", "vien"),
                Complement::new("iell", "Daughter of", "viel"),
                Complement::new("bess", "Wife", "ves"),
            ]
        } else if key.ends_with('g') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "geth"),
                Complement::new("el", "Female", "gel"),
                Complement::new("il", "Female", "gil"),
                Complement::new("ien", "Daughter of", "gien"),
                Complement::new("iell", "Daughter of", "giel"),
                Complement::new("sell", "Girl", "gel"),
                Complement::new("gwend", "Maiden", "gwen"),
                Complement::new("neth", "Girl", "gneth"),
                Complement::new("dîs", "Bride", "gnis"),
                Complement::new("dess", "Woman", "gnes"),
                Complement::new("nîth", "Sister", "gnith"),
                Complement::new("thêl", "Sister", "cthel"),
            ]
        } else if key.ends_with("ch") {
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("sell", "Girl", "el"),
            ]
        } else if key.ends_with('h') {
            vec![
                Complement::new("eth", "Female", "es"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("sell", "Girl", "el"),
                Complement::new("thêl", "Sister", "el"),
            ]
        } else if key.ends_with("ll") || key.ends_with('l') {
            if key.ends_with("ll") {
                key.pop();
            }
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("sell", "Girl", "hel"),
                Complement::new("gwend", "Maiden", "wen"),
                Complement::new("neth", "Girl", "neth"),
                Complement::new("dîs", "Bride", "dis"),
                Complement::new("dess", "Woman", "des"),
                Complement::new("nîth", "Sister", "nith"),
                Complement::new("thêl", "Sister", "thel"),
                Complement::new("bess", "Wife", "bes"),
            ]
        } else if key.ends_with('m') {
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("dîs", "Bride", "dis"),
                Complement::new("dess", "Woman", "des"),
                Complement::new("bess", "Wife", "bes"),
            ]
        } else if key.ends_with('n') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "neth"),
                Complement::new("el", "Female", "nel"),
                Complement::new("il", "Female", "nil"),
                Complement::new("ien", "Daughter of", "nien"),
                Complement::new("iell", "Daughter of", "niel"),
                Complement::new("sell", "Girl", "ssel"),
                Complement::new("gwend", "Maiden", "ngwen"),
                Complement::new("neth", "Girl", "neth"),
                Complement::new("dîs", "Bride", "ndis"),
                Complement::new("dess", "Woman", "ndes"),
                Complement::new("nîth", "Sister", "nith"),
                Complement::new("thêl", "Sister", "nthel"),
                Complement::new("bess", "Wife", "mes"),
            ]
        } else if key.ends_with("mp") {
            key.pop();
            vec![
                Complement::new("eth", "Female", "meth"),
                Complement::new("el", "Female", "mel"),
                Complement::new("il", "Female", "mil"),
                Complement::new("ien", "Daughter of", "mien"),
                Complement::new("iell", "Daughter of", "miel"),
                Complement::new("sell", "Girl", "hel"),
                Complement::new("dîs", "Bride", "dis"),
                Complement::new("dess", "Woman", "des"),
                Complement::new("bess", "Wife", "mes"),
            ]
        } else if key.ends_with('p') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "beth"),
                Complement::new("el", "Female", "bel"),
                Complement::new("il", "Female", "bil"),
                Complement::new("ien", "Daughter of", "bien"),
                Complement::new("iell", "Daughter of", "biel"),
                Complement::new("bess", "Wife", "bes"),
            ]
        } else if key.ends_with('r') {
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("el", "Female", "el"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("sell", "Girl", "hel"),
                Complement::new("gwend", "Maiden", "wen"),
                Complement::new("neth", "Girl", "neth"),
                Complement::new("dîs", "Bride", "dis"),
                Complement::new("dess", "Woman", "des"),
                Complement::new("nîth", "Sister", "nith"),
                Complement::new("thêl", "Sister", "thel"),
                Complement::new("bess", "Wife", "bes"),
            ]
        } else if key.ends_with("ss") {
            key.pop();
            vec![
                Complement::new("eth", "Female", "seth"),
                Complement::new("el", "Female", "sel"),
                Complement::new("il", "Female", "sil"),
                Complement::new("ien", "Daughter of", "sien"),
                Complement::new("iell", "Daughter of", "siel"),
                Complement::new("sell", "Girl", "sel"),
                Complement::new("gwend", "Maiden", "sengwen"),
                Complement::new("neth", "Girl", "seneth"),
                Complement::new("dîs", "Bride", "sendis"),
                Complement::new("dess", "Woman", "sendes"),
                Complement::new("nîth", "Sister", "senith"),
                Complement::new("thêl", "Sister", "senthel"),
                Complement::new("bess", "Wife", "semes"),
            ]
        } else if key.ends_with('s') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "seth"),
                Complement::new("el", "Female", "sel"),
                Complement::new("il", "Female", "sil"),
                Complement::new("ien", "Daughter of", "sien"),
                Complement::new("iell", "Daughter of", "siel"),
                Complement::new("sell", "Girl", "sel"),
                Complement::new("dîs", "Bride", "dis"),
                Complement::new("dess", "Woman", "des"),
                Complement::new("bess", "Wife", "bes"),
            ]
        } else if key.ends_with("lt") {
            key.pop();
            vec![
                Complement::new("eth", "Female", "eth"),
                Complement::new("il", "Female", "il"),
                Complement::new("ien", "Daughter of", "ien"),
                Complement::new("iell", "Daughter of", "iel"),
                Complement::new("gwend", "Maiden", "wen"),
                Complement::new("neth", "Girl", "neth"),
                Complement::new("dîs", "Bride", "dis"),
                Complement::new("dess", "Woman", "des"),
                Complement::new("nîth", "Sister", "nith"),
                Complement::new("bess", "Wife", "ves"),
            ]
        } else if key.ends_with("nt") {
            key.pop();
            key.pop();
            vec![
                Complement::new("eth", "Female", "nneth"),
                Complement::new("el", "Female", "nnel"),
                Complement::new("il", "Female", "nnil"),
                Complement::new("ien", "Daughter of", "nnien"),
                Complement::new("iell", "Daughter of", "nniel"),
                Complement::new("sell", "Girl", "nthel"),
                Complement::new("gwend", "Maiden", "ngwen"),
                Complement::new("neth", "Girl", "nneth"),
                Complement::new("dîs", "Bride", "ndis"),
                Complement::new("dess", "Woman", "ndes"),
                Complement::new("nîth", "Sister", "nnith"),
                Complement::new("bess", "Wife", "mbes"),
            ]
        } else if key.ends_with('t') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "teth"),
                Complement::new("el", "Female", "tel"),
                Complement::new("il", "Female", "til"),
                Complement::new("ien", "Daughter of", "tien"),
                Complement::new("iell", "Daughter of", "tiel"),
                Complement::new("sell", "Girl", "sel"),
            ]
        } else if key.ends_with('w') {
            key.pop();
            vec![
                Complement::new("eth", "Female", "weth"),
                Complement::new("el", "Female", "wel"),
                Complement::new("il", "Female", "wil"),
                Complement::new("ien", "Daughter of", "wien"),
                Complement::new("iell", "Daughter of", "wiel"),
                Complement::new("sell", "Girl", "hel"),
                Complement::new("gwend", "Maiden", "wen"),
                Complement::new("neth", "Girl", "neth"),
                Complement::new("dîs", "Bride", "nis"),
                Complement::new("dess", "Woman", "nes"),
                Complement::new("nîth", "Sister", "nith"),
                Complement::new("thêl", "Sister", "thel"),
                Complement::new("bess", "Wife", "ves"),
            ]
        } else {
            panic!("This should not happen");
        }
    }

    #[expect(clippy::too_many_lines)]
    fn get_complement_first_male(key: &mut String) -> Vec<Complement> {
        if key.ends_with('a')
            || key.ends_with('â')
            || key.ends_with('e')
            || key.ends_with('ê')
            || key.ends_with('i')
            || key.ends_with('í')
            || key.ends_with('î')
            || key.ends_with('o')
            || key.ends_with('ô')
            || key.ends_with('u')
            || key.ends_with('û')
        {
            vec![
                Complement::new("daer", "Groom", "naer"),
                Complement::new("dir", "Man", "nir"),
                Complement::new("benn", "Husband", "ven"),
                Complement::new("tôr", "Brother", "dor"),
                Complement::new("hawn", "Brother", "chon"),
                Complement::new("hanar", "Brother", "chanar"),
            ]
        } else if key.ends_with('b') {
            vec![
                Complement::new("on", "Male", "on"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("benn", "Husband", "en"),
            ]
        } else if key.ends_with('c') {
            key.pop();
            vec![
                Complement::new("on", "Male", "gon"),
                Complement::new("ion", "Son of", "gion"),
            ]
        } else if key.ends_with("nd") || key.ends_with("nt") {
            key.pop();
            key.pop();
            vec![
                Complement::new("on", "Male", "nnor"),
                Complement::new("ion", "Son of", "nnion"),
                Complement::new("daer", "Groom", "ndaer"),
                Complement::new("dir", "Man", "ndir"),
                Complement::new("benn", "Husband", "mben"),
                Complement::new("tôr", "Brother", "ndor"),
            ]
        } else if key.ends_with('d') {
            vec![
                Complement::new("on", "Male", "on"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("daer", "Groom", "aer"),
                Complement::new("dir", "Man", "ir"),
                Complement::new("benn", "Husband", "ben"),
                Complement::new("tôr", "Brother", "or"),
            ]
        } else if key.ends_with('f') {
            key.pop();
            vec![
                Complement::new("on", "Male", "von"),
                Complement::new("ion", "Son of", "vion"),
                Complement::new("benn", "Husband", "ven"),
            ]
        } else if key.ends_with('g') || key.ends_with('h') {
            vec![
                Complement::new("on", "Male", "on"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("hawn", "Brother", "on"),
                Complement::new("hanar", "Brother", "anar"),
            ]
        } else if key.ends_with('l') {
            if key.ends_with("ll") {
                key.pop();
            }
            vec![
                Complement::new("on", "Male", "on"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("daer", "Groom", "aer"),
                Complement::new("dir", "Man", "ir"),
                Complement::new("benn", "Husband", "ben"),
                Complement::new("tôr", "Brother", "or"),
            ]
        } else if key.ends_with('m') {
            key.pop();
            vec![
                Complement::new("on", "Male", "on"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("daer", "Groom", "daer"),
                Complement::new("dir", "Man", "dir"),
                Complement::new("benn", "Husband", "ben"),
                Complement::new("tôr", "Brother", "dor"),
            ]
        } else if key.ends_with('n') {
            key.pop();
            vec![
                Complement::new("on", "Male", "non"),
                Complement::new("ion", "Son of", "nion"),
                Complement::new("daer", "Groom", "ndaer"),
                Complement::new("dir", "Man", "ndir"),
                Complement::new("benn", "Husband", "men"),
                Complement::new("tôr", "Brother", "thor"),
            ]
        } else if key.ends_with("mp") {
            key.pop();
            vec![
                Complement::new("on", "Male", "mon"),
                Complement::new("ion", "Son of", "mion"),
                Complement::new("daer", "Groom", "daer"),
                Complement::new("dir", "Man", "dir"),
                Complement::new("benn", "Husband", "ben"),
                Complement::new("tôr", "Brother", "dor"),
            ]
        } else if key.ends_with('p') {
            key.pop();
            vec![
                Complement::new("on", "Male", "bon"),
                Complement::new("ion", "Son of", "bion"),
                Complement::new("benn", "Husband", "ben"),
            ]
        } else if key.ends_with('r') {
            vec![
                Complement::new("on", "Male", "on"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("daer", "Groom", "daer"),
                Complement::new("dir", "Man", "dir"),
                Complement::new("benn", "Husband", "ben"),
                Complement::new("tôr", "Brother", "dor"),
                Complement::new("hawn", "Brother", "chon"),
                Complement::new("hanar", "Brother", "chanar"),
            ]
        } else if key.ends_with("ss") {
            key.pop();
            vec![
                Complement::new("on", "Male", "son"),
                Complement::new("ion", "Son of", "sion"),
                Complement::new("daer", "Groom", "sendaer"),
                Complement::new("dir", "Man", "sendir"),
                Complement::new("benn", "Husband", "semen"),
                Complement::new("tôr", "Brother", "tor"),
            ]
        } else if key.ends_with('s') {
            vec![
                Complement::new("on", "Male", "son"),
                Complement::new("ion", "Son of", "sion"),
                Complement::new("daer", "Groom", "daer"),
                Complement::new("dir", "Man", "dir"),
                Complement::new("benn", "Husband", "ben"),
                Complement::new("tôr", "Brother", "tor"),
            ]
        } else if key.ends_with("lt") {
            key.pop();
            vec![
                Complement::new("on", "Male", "on"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("daer", "Groom", "daer"),
                Complement::new("dir", "Man", "dir"),
                Complement::new("benn", "Husband", "ven"),
                Complement::new("tôr", "Brother", "dor"),
                Complement::new("hawn", "Brother", "chon"),
                Complement::new("hanar", "Brother", "chanar"),
            ]
        } else if key.ends_with('t') {
            vec![
                Complement::new("on", "Male", "on"),
                Complement::new("ion", "Son of", "ion"),
                Complement::new("tôr", "Brother", "or"),
            ]
        } else if key.ends_with('w') {
            key.pop();
            vec![
                Complement::new("on", "Male", "won"),
                Complement::new("ion", "Son of", "wion"),
                Complement::new("daer", "Groom", "naer"),
                Complement::new("dir", "Man", "nir"),
                Complement::new("benn", "Husband", "ven"),
                Complement::new("tôr", "Brother", "dor"),
                Complement::new("hawn", "Brother", "chon"),
                Complement::new("hanar", "Brother", "chanar"),
            ]
        } else {
            panic!("This should not happen");
        }
    }

    fn get_complement_first_neutral(key: &mut String) -> Vec<Complement> {
        if key.ends_with('b') || (key.ends_with('p') && !key.ends_with("mp")) {
            vec![
                Complement::new("pen", "Person", "en"),
                Complement::new("", "", ""),
            ]
        } else if key.ends_with("nd") || key.ends_with('n') || key.ends_with("nt") {
            key.pop();
            key.pop();
            vec![
                Complement::new("pen", "Person", "mben"),
                Complement::new("", "", "nd"),
            ]
        } else if key.ends_with('s') {
            if key.ends_with("ss") {
                key.pop();
            }
            vec![
                Complement::new("pen", "Person", "pen"),
                Complement::new("", "", ""),
            ]
        } else if key.ends_with('c')
            || key.ends_with('d')
            || key.ends_with('g')
            || key.ends_with("ch")
            || (key.ends_with('t') && !key.ends_with("lt"))
            || key.ends_with('w')
        {
            vec![Complement::new("", "", "")]
        } else if key.ends_with('f') || key.ends_with('r') {
            if key.ends_with('f') {
                key.pop();
            }
            vec![
                Complement::new("pen", "Person", "phen"),
                Complement::new("", "", ""),
            ]
        } else {
            if key.ends_with("ll") || key.ends_with("mp") || key.ends_with("lt") {
                key.pop();
            }
            vec![
                Complement::new("pen", "Person", "ben"),
                Complement::new("", "", ""),
            ]
        }
    }

    fn get_complement_first(key: &mut String, gender: Genders) -> Vec<Complement> {
        if gender == Genders::Female {
            SindarinNameGenerator::get_complement_first_female(key)
        } else if gender == Genders::Male {
            SindarinNameGenerator::get_complement_first_male(key)
        } else {
            SindarinNameGenerator::get_complement_first_neutral(key)
        }
    }
}

impl NameGenerator for SindarinNameGenerator {
    fn get_name(&self, gender: Genders) -> String {
        // Prepare random numbers
        const PROB: f32 = 0.5;

        let mut rng = rand::thread_rng();
        let prob: f32 = rng.gen();

        // Get data
        let data = SindarinNameGenerator::get_data();
        let data = if prob < PROB { data.first } else { data.second };

        // Sample
        let mut key = data.keys().choose(&mut rng).unwrap().clone();
        let desc = &data[&key];
        let binding = if prob < PROB {
            SindarinNameGenerator::get_complement_first(&mut key, gender)
        } else {
            SindarinNameGenerator::get_complement_second(&mut key, gender)
        };
        let complement = binding.choose(&mut rng).unwrap();

        // Prepare output
        let name = key.clone() + complement.extension_with_liaison;
        let desc1 = key.clone() + " (" + desc;
        let desc2 = if complement.extension_with_liaison.is_empty() {
            ")".to_owned()
        } else {
            ") + ".to_owned() + complement.extension + " (" + complement.description + ")"
        };
        uppercase_first_letter(&name) + " = " + &desc1 + &desc2
    }

    fn get_genders(&self) -> Vec<Genders> {
        vec![Genders::Male, Genders::Female, Genders::Neutral]
    }
}
