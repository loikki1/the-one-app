use rand::seq::SliceRandom as _;
use rand::Rng as _;

use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

const LOW_PROB: f32 = 0.25;
const HIGH_PROB: f32 = 0.5;

pub struct HaradrimNameGenerator;

impl NameGenerator for HaradrimNameGenerator {
    fn get_name(&self, gender: Genders) -> String {
        let male = gender == Genders::Male;

        let l1: Vec<&str> = if male {
            vec![
                "", "", "b", "d", "gh", "h", "j", "kh", "l", "m", "n", "q", "s", "t", "w", "y",
            ]
        } else {
            vec![
                "", "d", "f", "gh", "h", "kh", "m", "n", "r", "s", "t", "w", "y",
            ]
        };

        let l2: Vec<&str> = if male {
            vec![
                "aa", "a", "a", "i", "u", "u", "a", "a", "i", "u", "u", "a", "a", "i", "u", "u",
                "a", "a", "i", "u", "u",
            ]
        } else {
            vec!["a", "i", "u"]
        };

        let l3: Vec<&str> = if male {
            vec![
                "b", "br", "dh", "dn", "f", "kr", "ld", "'m", "mr", "'n", "n", "nn", "q", "qq",
                "r", "rw", "s", "s'", "sh", "tb", "th", "wf", "z",
            ]
        } else {
            vec![
                "d", "dh", "f", "h", "l", "ll", "lr", "ld", "m", "ml", "n", "nn", "nh", "r", "rr",
                "rl", "rh", "s", "sh", "sr", "w", "wd", "wl", "z", "zh",
            ]
        };

        let l4: Vec<&str> = if male {
            vec![
                "aa", "oo", "ay", "a", "i", "o", "a", "i", "o", "a", "i", "o", "a", "i", "o", "a",
                "a", "a", "o", "o",
            ]
        } else {
            vec![
                "ay", "y", "aa", "a", "a", "a", "a", "a", "i", "i", "i", "i", "i",
            ]
        };

        let l5: Vec<&str> = if male {
            vec!["d", "f", "m", "n", "nn", "r", "z"]
        } else {
            vec!["b", "dh", "h", "l", "m", "n", "r", "th", "y"]
        };

        let l6: Vec<&str> = if male {
            vec!["", "b", "d", "h", "kr", "l", "m", "n", "r", "s", "th"]
        } else {
            vec!["", "d", "h", "h", "h", "l", "n", "nd", "r"]
        };

        let mut rng = rand::thread_rng();
        let prob: f32 = rng.gen();

        let str1 = *l1.choose(&mut rng).unwrap();
        let str2 = *l2.choose(&mut rng).unwrap();
        let str4 = *l4.choose(&mut rng).unwrap();
        let str6 = *l6.choose(&mut rng).unwrap();

        let mut str3 = str1;
        while str3 == str1 || str3 == str6 {
            str3 = l3.choose(&mut rng).unwrap();
        }

        let name;
        if male && prob < LOW_PROB || !male && prob < HIGH_PROB {
            name = str1.to_owned() + str2 + str3 + str4 + str6;
        } else if male && prob < HIGH_PROB {
            name = "Al".to_owned() + str1 + str2 + str3 + str4 + str6;
        } else {
            let str2_2 = l2.choose(&mut rng).unwrap();
            let mut str5 = str3;
            while str5 == str6 || str5 == str3 {
                str5 = l5.choose(&mut rng).unwrap();
            }
            name = str1.to_owned() + str2 + str3 + str2_2 + str5 + str4 + str6;
        }
        uppercase_first_letter(&name)
    }
}
