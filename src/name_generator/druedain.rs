use rand::seq::SliceRandom as _;

use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

pub struct DruedainNameGenerator;

impl NameGenerator for DruedainNameGenerator {
    fn get_name(&self, _: Genders) -> String {
        const L1: [&str; 11] = [
            "bh", "dh", "gh", "kh", "oh", "rh", "th", "vh", "wh", "xh", "zh",
        ];
        const L2: [&str; 3] = ["â", "ô", "û"];
        const L3: [&str; 9] = ["b", "d", "g", "h", "m", "n", "r", "v", "z"];
        const L4: [&str; 4] = ["a", "e", "o", "u"];
        const L5: [&str; 6] = ["d", "m", "n", "r", "v", "z"];
        const L6: [&str; 4] = ["a", "i", "o", "u"];

        let mut rng = rand::thread_rng();

        let str1 = L1.choose(&mut rng).unwrap();
        let str2 = L2.choose(&mut rng).unwrap();
        let str3 = L3.choose(&mut rng).unwrap();
        let str4 = L4.choose(&mut rng).unwrap();
        let str6 = L6.choose(&mut rng).unwrap();
        loop {
            let str5 = L5.choose(&mut rng).unwrap();
            if str5 != str3 {
                let name = uppercase_first_letter(str1)
                    + str2
                    + "n-"
                    + str3
                    + str4
                    + str5
                    + str6
                    + "-"
                    + str1
                    + str2
                    + "n";
                return name;
            }
        }
    }

    fn get_genders(&self) -> Vec<Genders> {
        vec![]
    }
}
