use rand::seq::SliceRandom as _;
use rand::Rng as _;

use crate::name_generator::NameGenerator;
use crate::utils::uppercase_first_letter;

use super::Genders;

pub struct OrcNameGenerator;

impl NameGenerator for OrcNameGenerator {
    fn get_name(&self, _: Genders) -> String {
        const L1: [&str; 17] = [
            "b", "br", "c", "cr", "d", "dr", "g", "gh", "gr", "k", "kr", "l", "m", "r", "s", "sh",
            "sr",
        ];
        const L2: [&str; 11] = ["a", "e", "i", "o", "u", "a", "e", "i", "o", "u", "au"];
        const L3: [&str; 31] = [
            "cb", "cd", "cr", "db", "dd", "fd", "fth", "g", "gb", "gd", "gg", "gl", "gr", "gz",
            "h", "lcm", "ld", "lf", "lg", "rb", "rc", "rd", "rg", "rz", "shn", "thr", "z", "zb",
            "zg", "zr", "zz",
        ];
        const L4: [&str; 16] = [
            "c", "d", "dh", "f", "g", "gh", "kh", "l", "r", "rg", "sh", "t", "th", "", "", "",
        ];
        const L5: [&str; 4] = ["a", "o", "u", "au"];

        const PROB: f32 = 0.5;

        let mut rng = rand::thread_rng();
        let prob: f32 = rng.gen();

        let output = if prob < PROB {
            let mut name = (*L1.choose(&mut rng).unwrap()).to_owned();
            name += L2.choose(&mut rng).unwrap();
            name += L3.choose(&mut rng).unwrap();
            name += L2.choose(&mut rng).unwrap();
            name += L4.choose(&mut rng).unwrap();
            uppercase_first_letter(&name)
        } else {
            let mut name = (*L5.choose(&mut rng).unwrap()).to_owned();
            name += L3.choose(&mut rng).unwrap();
            name += L2.choose(&mut rng).unwrap();
            name += L4.choose(&mut rng).unwrap();
            name
        };
        uppercase_first_letter(&output)
    }

    fn get_genders(&self) -> Vec<Genders> {
        vec![]
    }
}
