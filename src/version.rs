use serde::{Deserialize, Serialize};
use ts_rs::TS;

#[derive(Serialize, Deserialize, TS)]
#[ts(export)]
pub struct Version {
    pub major: u32,
    pub minor: u32,
    pub patch: u32,
}

impl Default for Version {
    fn default() -> Self {
        Self {
            major: env!("CARGO_PKG_VERSION_MAJOR")
                .parse()
                .expect("Failed to extract major version"),
            minor: env!("CARGO_PKG_VERSION_MINOR")
                .parse()
                .expect("Failed to extract minor version"),
            patch: env!("CARGO_PKG_VERSION_PATCH")
                .parse()
                .expect("Failed to extract patch version"),
        }
    }
}

impl Version {
    pub fn new() -> Self {
        Self::default()
    }
}
