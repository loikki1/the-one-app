use crate::character::items::Items;
use crate::character::reward::Reward;
use crate::character::virtue::Virtue;
use crate::new_character::improvements::Improvements;
use enums::{
    calling::CallingEnum, combat_skill::CombatSkillEnum, culture::CultureEnum, skill::SkillEnum,
};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

#[derive(Serialize, Deserialize, TS)]
#[ts(export)]
pub struct Choices {
    pub character_name: String,
    pub culture: CultureEnum,
    pub calling: CallingEnum,
    pub attributes: usize,
    pub culture_skill: Option<SkillEnum>,
    pub calling_skills: Vec<SkillEnum>,
    pub distinctive_features: Vec<String>,
    pub all_combat_skill: Option<CombatSkillEnum>,
    pub available_combat_skill: Option<CombatSkillEnum>,
    pub items: Items,
    pub reward: Reward,
    pub virtue: Virtue,
    pub previous_experience: u32,
    pub improvements: Improvements,
    pub one_shot_rules: bool,
    pub strider_mode: bool,
}

impl Default for Choices {
    fn default() -> Self {
        Self::new()
    }
}

impl Choices {
    pub fn new() -> Self {
        Self {
            character_name: String::new(),
            culture: CultureEnum::Barding,
            calling: CallingEnum::Captain,
            attributes: 0,
            culture_skill: None,
            calling_skills: vec![],
            distinctive_features: vec![],
            all_combat_skill: None,
            available_combat_skill: None,
            items: Items::new(),
            previous_experience: 10,
            reward: Reward::default(),
            virtue: Virtue::default(),
            improvements: Improvements::new(),
            one_shot_rules: false,
            strider_mode: false,
        }
    }
}
