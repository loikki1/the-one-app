use anyhow::{bail, Result};
use enums::{combat_skill::CombatSkillEnum, skill::SkillEnum};
use strum::{EnumCount as _, IntoEnumIterator as _};

use crate::character::combat_skills::CombatSkills;

use super::{choices::Choices, culture::Culture};

pub struct PreviousExperience;

impl PreviousExperience {
    pub fn get_skill_cost(new_level: u32) -> u32 {
        match new_level {
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 5,
            _ => u32::MAX,
        }
    }

    pub fn get_skill_cost_from_previous_level(previous_level: u32, new_level: u32) -> i32 {
        let mut cost = 0;
        if previous_level > new_level {
            for i in previous_level..new_level {
                cost -= Self::get_skill_cost(i) as i32;
            }
        } else {
            for i in previous_level..new_level {
                cost += Self::get_skill_cost(i + 1) as i32;
            }
        }
        cost
    }

    pub fn get_combat_skill_cost(new_level: u32) -> u32 {
        match new_level {
            1 => 2,
            2 => 4,
            3 => 6,
            _ => u32::MAX,
        }
    }

    pub fn get_combat_skill_cost_from_previous_level(previous_level: u32, new_level: u32) -> i32 {
        let mut cost = 0;
        if previous_level > new_level {
            for i in previous_level..new_level {
                cost -= Self::get_combat_skill_cost(i) as i32;
            }
        } else {
            for i in previous_level..new_level {
                cost += Self::get_combat_skill_cost(i + 1) as i32;
            }
        }
        cost
    }

    pub fn get_previous_experience_spent(choices: &Choices) -> Result<i32> {
        let initial_skills = Culture::new(choices.culture.clone()).skills;
        let initial_combat = CombatSkills::new(choices, false)?;

        let mut previous_experience: i32 = 0;
        // Skills
        for improvement in &choices.improvements.skills {
            let new_value = improvement.1;
            let init = initial_skills[improvement.0.clone() as usize];
            previous_experience += Self::get_skill_cost_from_previous_level(init, *new_value);
        }

        // Combat
        for improvement in &choices.improvements.combat {
            let new_value = improvement.1;
            let init = initial_combat.get_skill(improvement.0);
            previous_experience +=
                Self::get_combat_skill_cost_from_previous_level(init, *new_value);
        }

        Ok(previous_experience)
    }

    pub fn check_previous_experience(choices: &Choices) -> Result<()> {
        let previous_experience = PreviousExperience::get_previous_experience_spent(choices)?;

        if (choices.strider_mode && previous_experience == 15)
            || (!choices.strider_mode && previous_experience == 10)
        {
            Ok(())
        } else {
            bail!(format!(
                "The previous experience does not match the expected value {previous_experience}",
            ))
        }
    }

    pub fn get_can_upgrade_skills(
        previous_experience: u32,
        skills: &[u32; SkillEnum::COUNT],
    ) -> Vec<SkillEnum> {
        let mut out = vec![];
        for skill in SkillEnum::iter() {
            let current = skills[skill.clone() as usize];
            if Self::get_skill_cost(current + 1) <= previous_experience {
                out.push(skill.clone());
            }
        }
        out
    }

    pub fn get_can_upgrade_combat_skills(
        previous_experience: u32,
        skills: &[u32; CombatSkillEnum::COUNT],
    ) -> Vec<CombatSkillEnum> {
        let mut out = vec![];
        for skill in CombatSkillEnum::iter() {
            let current = skills[skill.clone() as usize];
            if Self::get_combat_skill_cost(current + 1) <= previous_experience {
                out.push(skill.clone());
            }
        }
        out
    }
}
