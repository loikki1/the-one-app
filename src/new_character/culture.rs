use crate::assets::read_enum_from_assets;

use crate::character::virtue::VirtueEnum;
use enums::{combat_skill::CombatSkillEnum, culture::CultureEnum, skill::SkillEnum};
use serde::{Deserialize, Serialize};
use strum::EnumCount as _;
use ts_rs::TS;

const DIRECTORY: &str = "new_character/culture";

#[derive(Serialize, Deserialize, Clone, TS)]
#[ts(export)]
pub struct DerivedStats {
    pub endurance: u32,
    pub hope: u32,
    pub parry: u32,
}

#[derive(Serialize, Deserialize, Clone, TS)]
#[ts(export)]
pub struct Culture {
    pub attributes: Vec<[u32; 3]>,
    pub treasures: u32,
    #[serde(alias = "culture_blessings")]
    pub blessings: Vec<VirtueEnum>,
    pub derived_stats: DerivedStats,
    pub favoured_skills: Vec<SkillEnum>,
    pub skills: [u32; SkillEnum::COUNT],
    pub combat_skills: Vec<CombatSkillEnum>,
    pub distinctive_features: Vec<String>,
}

impl Culture {
    pub fn new(culture: CultureEnum) -> Self {
        read_enum_from_assets(&culture, DIRECTORY)
    }
}

#[cfg(test)]
mod test {
    use enums::culture::CultureEnum;
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{assets::get_number_files, new_character::culture::DIRECTORY};

    use super::Culture;

    #[test]
    fn check_culture_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(CultureEnum::COUNT == number_files);

        for culture in CultureEnum::iter() {
            eprintln!("Testing {culture:?}");
            Culture::new(culture);
        }
    }
}
