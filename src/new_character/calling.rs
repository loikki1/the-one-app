use crate::assets::read_enum_from_assets;
use enums::{calling::CallingEnum, skill::SkillEnum};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

const DIRECTORY: &str = "new_character/calling";

#[derive(Serialize, Deserialize, Clone, TS)]
#[ts(export)]
pub struct Calling {
    pub favoured_skills: Vec<SkillEnum>,
    pub distinctive_feature: String,
    pub shadow_path: String,
}

impl Calling {
    pub fn new(calling: CallingEnum) -> Self {
        read_enum_from_assets(&calling, DIRECTORY)
    }
}

#[cfg(test)]
mod test {
    use enums::calling::CallingEnum;
    use strum::{EnumCount as _, IntoEnumIterator as _};

    use crate::{assets::get_number_files, new_character::calling::DIRECTORY};

    use super::Calling;

    #[test]
    fn check_calling_data() {
        let number_files = get_number_files(DIRECTORY);
        assert!(CallingEnum::COUNT == number_files);

        for calling in CallingEnum::iter() {
            eprintln!("Testing {calling:?}");
            Calling::new(calling);
        }
    }
}
