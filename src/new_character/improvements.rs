use enums::{combat_skill::CombatSkillEnum, skill::SkillEnum};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use ts_rs::TS;

#[derive(Serialize, Deserialize, TS)]
#[ts(export)]
pub struct Improvements {
    pub skills: HashMap<SkillEnum, u32>,
    pub combat: HashMap<CombatSkillEnum, u32>,
}

impl Default for Improvements {
    fn default() -> Self {
        Self::new()
    }
}

impl Improvements {
    pub fn new() -> Self {
        Self {
            skills: HashMap::new(),
            combat: HashMap::new(),
        }
    }
}
