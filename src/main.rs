// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

#[cfg(feature = "server")]
use rocket::launch;

#[cfg(feature = "server")]
#[launch]
async fn rocket() -> rocket::Rocket<rocket::Build> {
    use rocket::fs::FileServer;
    use std::path::PathBuf;
    use the_one_app_lib::{backend, config::CONFIG};

    // Get directories
    let current_dir = PathBuf::from(".");

    let mut config = CONFIG.lock().await;
    config.configure(&current_dir);

    let frontend_path = config.frontend_path().clone();
    drop(config);

    let rocket = backend::get_rocket().await;
    if let Ok(rocket) = rocket {
        rocket.mount("/", FileServer::from(frontend_path))
    } else {
        panic!("Failed to start rocket {rocket:?}");
    }
}

#[cfg(feature = "tauri")]
pub fn main() {
    use the_one_app_lib::start_with_tauri;

    start_with_tauri();
}
