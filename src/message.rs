use anyhow::{anyhow, bail, Result};
use chrono::NaiveDateTime;
use sea_orm::ActiveValue::NotSet;
use sea_orm::DatabaseConnection;
use sea_orm::EntityTrait as _;
use sea_orm::QueryFilter as _;
use sea_orm::QueryOrder as _;
use sea_orm::{ActiveModelTrait as _, ColumnTrait as _, Set};
use serde::Deserialize;
use serde::Serialize;
use ts_rs::TS;

use crate::check_ownership_none_is_ok;
use crate::entity::fellowship as fellowship_entity;
use crate::entity::message as message_entity;
use crate::users::User;

#[derive(Serialize, Deserialize, TS, Debug)]
#[ts(export)]
pub struct Message {
    text: String,
    to_loremaster: bool,
    player: String,
    date: NaiveDateTime,
}

impl Message {
    pub fn get_to_loremaster(&self) -> bool {
        self.to_loremaster
    }

    pub fn get_player(&self) -> &str {
        &self.player
    }
    pub async fn get(
        db: &DatabaseConnection,
        user: &User,
        fellowship_id: i32,
        player: Option<&str>,
    ) -> Result<Vec<Self>> {
        let messages = message_entity::Entity::find()
            .filter(message_entity::Column::FellowshipId.eq(fellowship_id))
            .order_by_desc(message_entity::Column::Date);

        let messages = if let Some(player) = player {
            messages.filter(message_entity::Column::Player.eq(player))
        } else {
            messages
        }
        .all(db)
        .await?;

        let mut out = Vec::with_capacity(messages.len());
        for mess in messages {
            out.push(Self::from_db_model(db, user, &mess).await?);
        }

        Ok(out)
    }

    pub async fn from_db_model(
        db: &DatabaseConnection,
        user: &User,
        value: &message_entity::Model,
    ) -> Result<Self> {
        check_ownership_none_is_ok!(db, user, Some(value.fellowship_id), fellowship_entity)?;

        Ok(Self {
            text: value.text.clone(),
            to_loremaster: value.to_loremaster,
            player: value.player.clone(),
            date: value.date,
        })
    }

    pub async fn save_to_db(
        &self,
        db: &DatabaseConnection,
        user: &User,
        fellowship_id: i32,
    ) -> Result<()> {
        check_ownership_none_is_ok!(db, user, Some(fellowship_id), fellowship_entity)?;

        message_entity::ActiveModel {
            id: NotSet,
            fellowship_id: Set(fellowship_id),
            text: Set(self.text.clone()),
            to_loremaster: Set(self.to_loremaster),
            player: Set(self.player.clone()),
            date: Set(self.date),
        }
        .save(db)
        .await?;
        Ok(())
    }
}
