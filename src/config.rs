use lazy_static::lazy_static;
use rocket::tokio::sync::Mutex;
use serde::{Deserialize, Serialize};
use std::{
    fs::File,
    path::{Path, PathBuf},
};

lazy_static! {
    pub static ref CONFIG: Mutex<Config> = Mutex::new(Config::default());
}

const CONFIG_FILE: &str = "config.json";

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Config {
    expose_rocket: bool,
    frontend_path: String,
    #[serde(default)]
    directory: PathBuf,
    #[serde(default)]
    configured: bool,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            frontend_path: String::from("frontend/build"),
            expose_rocket: false,
            directory: PathBuf::new(),
            configured: false,
        }
    }
}

impl Config {
    pub fn configure(&mut self, config_dir: &Path) {
        eprintln!("config: {config_dir:?}");
        self.directory = config_dir.to_path_buf();
        self.configured = true;

        let config_file = config_dir.join(CONFIG_FILE);
        let Ok(file) = File::open(config_file) else {
            return;
        };
        let config = match serde_json::from_reader::<File, Config>(file) {
            Err(error) => {
                eprintln!("Failed to read config file: {error}");
                self.clone()
            }
            Ok(mut config) => {
                eprintln!("Successfully read configuration file");
                if cfg!(target_os = "android") {
                    config.directory = config_dir.to_path_buf();
                }
                config
            }
        };
        self.clone_from(&config);
        self.configured = true;
    }

    pub fn is_configured(&self) -> bool {
        self.configured
    }

    pub(crate) fn expose_rocket(&self) -> bool {
        self.expose_rocket
    }

    pub fn frontend_path(&self) -> &String {
        &self.frontend_path
    }

    pub fn get_directory(&self) -> &PathBuf {
        &self.directory
    }
}
