use rocket::request::FromParam;
use serde::Deserialize;

#[derive(Deserialize, PartialEq)]
pub enum NameOrId {
    Name(String),
    Id(i32),
}

impl<'a> FromParam<'a> for NameOrId {
    type Error = &'a str;

    fn from_param(param: &'a str) -> Result<Self, Self::Error> {
        Ok(match param.parse::<i32>() {
            Ok(x) => NameOrId::Id(x),
            Err(_) => NameOrId::Name(param.to_owned()),
        })
    }
}

#[cfg(test)]
mod test {
    use rocket::request::FromParam as _;

    use super::NameOrId;

    #[test]
    fn check_conversion() {
        assert!(NameOrId::from_param("a").expect("Can parse") == NameOrId::Name(String::from("a")));
        assert!(NameOrId::from_param("1").expect("Can parse") == NameOrId::Id(1));
    }
}
