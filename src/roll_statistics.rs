use serde::Serialize;
use std::ops::AddAssign;
use ts_rs::TS;

use crate::roll::{FeatResult, Roll, TENGWAR};

#[derive(Serialize, TS)]
#[ts(export)]
pub struct RollStatistics {
    average_tengwar: f32,
    average_value: f32,
    #[serde(skip)]
    number_normal_rolls: u32,
    #[serde(skip)]
    number_rolls: u32,
    number_gandalf: u32,
    probabilities: Vec<f32>,
}

impl Default for RollStatistics {
    fn default() -> Self {
        Self::new()
    }
}

impl RollStatistics {
    pub fn new() -> Self {
        let mut vec = Vec::new();
        vec.resize(30, 0.0);
        Self {
            average_tengwar: 0.0,
            average_value: 0.0,
            number_normal_rolls: 0,
            number_rolls: 0,
            number_gandalf: 0,
            probabilities: vec,
        }
    }

    pub fn finalize(&mut self) {
        self.average_tengwar /= self.number_rolls as f32;
        self.average_value /= self.number_normal_rolls as f32;

        let mut previous = self.number_gandalf as f32;
        for x in self.probabilities.iter_mut().rev() {
            let current = previous;
            previous += *x;
            *x = 100.0 * (*x + current) / self.number_rolls as f32;
        }

        let mut prev = 0.0;
        for x in &mut self.probabilities {
            if *x == 0.0 {
                *x = prev;
            } else {
                prev = *x;
            }
        }
    }
}

impl AddAssign<Roll> for RollStatistics {
    fn add_assign(&mut self, rhs: Roll) {
        self.average_tengwar += rhs
            .success_dices
            .iter()
            .copied()
            .reduce(|acc, elem| acc + u32::from(elem == TENGWAR))
            .unwrap_or(0) as f32;
        let total = rhs.total.unwrap_or(0);
        self.average_value += total as f32;
        self.number_normal_rolls += if let FeatResult::Normal(_) = rhs.feat_dice {
            1
        } else {
            0
        };
        self.number_rolls += 1;
        self.number_gandalf += if let FeatResult::AutomaticSuccess = rhs.feat_dice {
            1
        } else {
            0
        };

        // Increase the size of the array if needed
        if self.probabilities.len() <= total as usize {
            self.probabilities.resize(1 + total as usize, 0.0);
        }

        // Avoid counting double gandalf
        if let FeatResult::AutomaticSuccess = rhs.feat_dice {
            return;
        }

        self.probabilities[total as usize] += 1.0;
    }
}
