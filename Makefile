.ONESHELL:
SHELL = /bin/bash
.SHELLFLAGS = -ec

ANDROID_NATIVE_API_LEVEL = 25
NDK_VERSION ?= 25.1.8937393
NDK_HOME ?= /opt/android-sdk/ndk/$(NDK_VERSION)
ANDROID_ARCH ?=
ANDROID_OPTIONS ?= --features tauri
DESKTOP_OPTIONS ?= --features tauri
SERVER_OPTIONS ?= --features server
CLOUD_OPTIONS ?= --features server,cloud
RANLIB ?= $(NDK_HOME)/toolchains/llvm/prebuilt/linux-x86_64/bin/llvm-ranlib
TS_RS_EXPORT_DIR ?= frontend/src/lib/bindings
CLIPPY_ARGS ?= --features server # -- -W clippy::pedantic
# -W clippy::restriction -W clippy::cargo

init:
	export NDK_HOME=$(NDK_HOME)
	cargo tauri android init
	echo "Don't foget to set your jvm version in ~/.gradle/gradle.properties (/usr/lib/jvm/...)"
	cargo tauri icon icon.png

# Dev
run:
	cargo tauri dev $(DESKTOP_OPTIONS) --no-watch

format:
	cd frontend
	npm run format
	npm run check
	npm run eslint
	cd ..
	cargo fmt
	for file in `find data -name '*.json'`; do \
      python3 -m json.tool --sort-keys $$file $$file; \
	done;
	cargo clippy $(CLIPPY_ARGS)

check-dependencies:
	cargo +nightly udeps --features tauri
	cargo deny check -A rejected -A unlicensed

clippy:
	cargo clippy $(CLIPPY_ARGS)

check-format:
	cargo fmt --all -- --check
	cargo clippy $(CLIPPY_ARGS)

test:
	TS_RS_EXPORT_DIR=$(TS_RS_EXPORT_DIR) cargo test --features cloud
	TS_RS_EXPORT_DIR=../$(TS_RS_EXPORT_DIR) cargo test --workspace

# Frontend
frontend-install-dep:
	cd frontend
	npm install

frontend-build:
	cd frontend
	npm run build
	cd ..
	python3	scripts/add-index.py

frontend-run:
	cd frontend
	npm run dev

# Desktop
desktop-build-debug:
	cargo tauri build --debug $(DESKTOP_OPTIONS)

desktop-build:
	cargo tauri build $(DESKTOP_OPTIONS)

desktop-test-ubuntu: desktop-build
	docker run --net=host --env DISPLAY=$$DISPLAY --device /dev/kfd --device /dev/dri --group-add video -v $$PWD:$$PWD ubuntu:22.04 /bin/bash -c "dpkg -i $$PWD/target/x86_64-unknown-linux-gnu/release/bundle/deb/the-one-app_0.0.0_amd64.deb || apt update && apt install -y -f && the-one-app"

# Server
server-build:
	cargo build --release $(SERVER_OPTIONS)

server-build-cloud:
	cargo build --release $(CLOUD_OPTIONS)

server-run:
	cargo run $(SERVER_OPTIONS)

server-run-cloud:
	cargo run $(CLOUD_OPTIONS)

server-run-cloud-dev:
	cargo watch -c -w data -w src -w migration -w enums -x run $(CLOUD_OPTIONS)

# Android
android-dev: .android-prepare
	adb uninstall io.gitlab.the-one-app | true
	export NDK_HOME=$(NDK_HOME)
	export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig/:$PKG_CONFIG_PATH
	export PKG_CONFIG_SYSROOT_DIR=/
	cargo tauri android dev $(ANDROID_OPTIONS)

.android-prepare:
	./copy-android

android-build-release: .android-prepare
	export RANLIB=$(RANLIB)
	export NDK_HOME=$(NDK_HOME)
	cargo tauri android build --apk $(ANDROID_OPTIONS) $(ANDROID_ARCH)
	cp gen/android/app/build/outputs/apk/universal/release/app-universal-release-unsigned.apk the-one-app.apk

android-build: .android-prepare
	export RANLIB=$(RANLIB)
	export NDK_HOME=$(NDK_HOME)
	cargo tauri android build --debug --apk $(ANDROID_OPTIONS) $(ANDROID_ARCH)
	cp gen/android/app/build/outputs/apk/universal/debug/app-universal-debug.apk the-one-app.apk

android-generate-key:
	keytool -genkey -v -keystore upload-keystore.jks -keyalg RSA -keysize 2048 -validity 10000 -alias upload

android-deploy:
	adb uninstall io.gitlab.the-one-app | true
	adb install the-one-app.apk

android-sign:
	/opt/android-sdk/build-tools/30.0.3/apksigner sign --ks-pass file:/home/loikki/.password --ks ~/android.keystore the-one-app.apk

android-logs:
	pid=`adb shell pidof io.gitlab.the-one-app`
	adb logcat --pid=$$pid
	echo $$pid

android-logs-clear:
	adb logcat -c

android-install-sdk:
	sudo sdkmanager --install "ndk;$(NDK_VERSION)"
	sudo sdkmanager --install "build-tools;34.0.0"
	sudo sdkmanager --install "platforms;android-$(ANDROID_NATIVE_API_LEVEL)"
	sudo sdkmanager --install "platform-tools"
	sudo sdkmanager --licenses

android-add-target:
	rustup target add aarch64-linux-android armv7-linux-androideabi i686-linux-android x86_64-linux-android

android-emulator:
	/opt/android-sdk/tools/emulator -avd Nexus6P

android-create-emulator:
	/opt/android-sdk/cmdline-tools/latest/bin/avdmanager create avd --force --name "Nexus 6P" --abi google_apis/x86_64 --package 'system-images;android-33;google_apis;x86_64' --device "Nexus 6P"

db-generate:
	sea-orm-cli generate entity -o src/entity
	echo Do not forget to revert all the unwanted changes in src/entity

db-migrate:
	sea-orm-cli migrate
