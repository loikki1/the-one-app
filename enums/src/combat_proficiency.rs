use sea_orm::DeriveIden;
use serde::{Deserialize, Serialize};
use strum::EnumIter;
use ts_rs::TS;

#[derive(Serialize, Deserialize, Default, Clone, EnumIter, TS, DeriveIden, Debug)]
#[ts(export)]
pub enum CombatProficiency {
    #[default]
    Brawling = 0,
    Swords = 1,
    Spears = 2,
    Axes = 3,
    Bows = 4,
}
