use rocket::{request::FromParam, FromFormField};
use sea_orm::DeriveIden;
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter};
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

#[derive(
    Serialize,
    DeriveIden,
    Deserialize,
    Clone,
    Debug,
    FromParamEnum,
    Hash,
    Eq,
    PartialEq,
    EnumIter,
    EnumCount,
    FromFormField,
    TS,
)]
#[ts(export)]
pub enum CultureEnum {
    Barding,
    Beorning,
    DwarfOfDurinsFolk,
    ElfOfLindon,
    ElfOfMirkwood,
    HobbitOfBree,
    HobbitOfTheShire,
    ManOfBree,
    RangerOfTheNorth,
    HighElfOfRivendell,
    DwarfOfNogrodOrBelegost,
    WoodmanOfWilderland,
}

impl CultureEnum {
    pub fn get_starting_eye(&self) -> u32 {
        match self {
            Self::Barding
            | Self::Beorning
            | Self::HobbitOfBree
            | Self::HobbitOfTheShire
            | Self::ManOfBree
            | Self::WoodmanOfWilderland => 0,
            Self::DwarfOfDurinsFolk | Self::DwarfOfNogrodOrBelegost => 1,
            Self::ElfOfLindon | Self::ElfOfMirkwood => 2,
            Self::RangerOfTheNorth | Self::HighElfOfRivendell => 3,
        }
    }
}
