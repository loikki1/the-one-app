use serde::Serialize;

#[derive(Serialize, Eq, PartialEq, Hash)]
pub enum AttributeEnum {
    Strength = 0,
    Heart = 1,
    Wits = 2,
}
