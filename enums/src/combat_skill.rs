use rocket::request::FromParam;
use sea_orm::DeriveIden;
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter};
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

#[derive(
    Clone,
    Serialize,
    Deserialize,
    Eq,
    PartialEq,
    Hash,
    FromParamEnum,
    EnumIter,
    EnumCount,
    TS,
    DeriveIden,
)]
pub enum CombatSkillEnum {
    Axes = 0,
    Bows = 1,
    Spears = 2,
    Swords = 3,
}
