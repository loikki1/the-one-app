use sea_orm::DeriveIden;
use serde::{Deserialize, Serialize};
use strum::EnumIter;
use ts_rs::TS;

#[derive(Serialize, Deserialize, Default, TS, DeriveIden, EnumIter, Debug, Clone)]
#[ts(export)]
pub enum HateEnum {
    #[default]
    Hate,
    Resolve,
}
