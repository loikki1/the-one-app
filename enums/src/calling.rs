use rocket::request::FromParam;
use sea_orm::DeriveIden;
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter};
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

#[derive(
    Debug, DeriveIden, Serialize, Deserialize, Clone, FromParamEnum, EnumIter, EnumCount, TS,
)]
#[ts(export)]
pub enum CallingEnum {
    Captain,
    Champion,
    Messenger,
    Scholar,
    TreasureHunter,
    Warden,
}
