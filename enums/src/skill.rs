use rocket::request::FromParam;
use sea_orm_migration::sea_orm::DeriveIden;
use serde::{Deserialize, Serialize};
use strum::{EnumCount, EnumIter};
use the_one_app_macro::FromParamEnum;
use ts_rs::TS;

use crate::attribute::AttributeEnum;

#[derive(
    Clone,
    Debug,
    DeriveIden,
    PartialOrd,
    PartialEq,
    Serialize,
    Deserialize,
    Eq,
    Hash,
    FromParamEnum,
    EnumIter,
    EnumCount,
    TS,
)]
pub enum SkillEnum {
    Awe = 0,
    Athletics = 1,
    Awareness = 2,
    Hunting = 3,
    Song = 4,
    Craft = 5,
    Enhearten = 6,
    Travel = 7,
    Insight = 8,
    Healing = 9,
    Courtesy = 10,
    Battle = 11,
    Persuade = 12,
    Stealth = 13,
    Scan = 14,
    Explore = 15,
    Riddle = 16,
    Lore = 17,
}

impl SkillEnum {
    pub fn get_attribute(&self) -> AttributeEnum {
        if *self <= SkillEnum::Craft {
            AttributeEnum::Strength
        } else if *self <= SkillEnum::Battle {
            AttributeEnum::Heart
        } else {
            AttributeEnum::Wits
        }
    }
}
