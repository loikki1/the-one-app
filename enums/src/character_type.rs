use sea_orm::{DeriveIden, EnumIter};
use serde::{Deserialize, Serialize};

#[derive(
    DeriveIden,
    EnumIter,
    Default,
    Serialize,
    Deserialize,
    Debug,
    Clone,
    Copy,
    PartialEq,
    PartialOrd,
    ts_rs::TS,
)]
pub enum CharacterType {
    #[default]
    Internal,
    External,
}
