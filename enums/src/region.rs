use sea_orm::DeriveIden;
use serde::{Deserialize, Serialize};
use strum::EnumIter;
use ts_rs::TS;

#[derive(Serialize, Deserialize, TS, DeriveIden, EnumIter, Debug)]
#[ts(export)]
pub enum Region {
    BorderLand,
    WildLand,
    DarkLand,
}

impl Region {
    pub fn hunt_threshold(eye: u32) -> Option<Self> {
        if eye >= 18 {
            Some(Self::BorderLand)
        } else if eye >= 16 {
            Some(Self::WildLand)
        } else if eye >= 14 {
            Some(Self::DarkLand)
        } else {
            None
        }
    }
}
