import requests
from copy import deepcopy

character = "<zd<zd"
server = "http://localhost:8000"

def get_character() -> dict:
    response = requests.get(f'{server}/character/{character}')
    return response.json()


def filter_fields(field) -> bool:
    ok = field != "derived"
    ok = ok and field != "standard_living"
    ok = ok and field != "skills"
    return ok

def process_object(obj, parents=[]) -> list:
    output = []
    for element in obj:
        if not filter_fields(element):
            continue

        value = obj[element]
        new_parents = deepcopy(parents)
        new_parents.append(element)

        # Treat array of dict as a dict => manual intervention
        if isinstance(value, list) and len(value) > 0 and isinstance(value[0], dict):
            print(f"Need to be manually edited: {element}")
            new_parents.append(0)
            value = value[0]

        if isinstance(value, dict):
            output.extend(process_object(value, new_parents))
        else:
            output.append(new_parents)
    return output

def get_field_type(obj, value) -> str:
    if isinstance(value, list) or value is None:
        print(f"Check {obj}")
        return "text"
    if isinstance(value, str):
        return "text"
    elif isinstance(value, int):
        return "number"
    else:
        raise Exception(f"Unknown type: {value} - {type(value)}")

def write_code(char, objects) -> str:
    output = ""
    for obj in objects:
        value = char
        for field in obj:
            value = value[field]

        field_type = get_field_type(obj, value)
        js_fields = ".".join(filter(lambda x: isinstance(x, str), obj))
        output += f"<Edit field_type={{field_types.{field_type}}} bind:value={{character.{js_fields}}} label=\"{obj[-1]}\"/>\n"

    return output

if (__name__ == "__main__"):
    char = get_character()

    processed = process_object(char)

    code = write_code(char, processed)

    print(code)
