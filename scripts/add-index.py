import os
from os.path import islink

def manage_directory(directory):
    elements = os.listdir(directory)
    for el in elements:
        if el == "index.html":
            continue

        full = f"{directory}/{el}"
        if os.path.isdir(full):
            manage_directory(full)
            continue

        basename, ext = os.path.splitext(full)
        if ext != ".html":
            continue

        print(f"Linking {el}")
        os.makedirs(basename, exist_ok=True)
        index = f"{basename}/index.html"

        if os.path.islink(index):
            os.remove(index)
        os.symlink(f"../{el}", index)


if (__name__ == "__main__"):
    manage_directory("frontend/build")
