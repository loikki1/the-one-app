* [ ] Generate translations
* [ ] Update the typescript classes with `make test`
* [ ] Create a tag (e.g. 1.0.0)
* [ ] Update tauri cli in `versions`
* [ ] Write a changelog in `fastlane`
* [ ] Update version in `android-files/AndroidManifest.xml`
* [ ] Update version in `android-files/build.gradle.kts`
* [ ] Update version in `tauri.conf.json`
* [ ] Update version in `Cargo.toml`
* [ ] Update F.A.Q.
