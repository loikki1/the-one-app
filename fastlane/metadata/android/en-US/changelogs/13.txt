Fix issues with the sockets (connection to loremaster)
Make the default url for the websocket the official website
Add new screenshots from the loremaster side
Hide login page with android
Add notification when saving / reseting characters in edit
Give access to the tools to the players
Automatically close the menu on phones
Cleanup code with clippy
