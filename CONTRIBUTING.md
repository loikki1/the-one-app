# Contributing

All contributions are welcomed (including comments / suggestions). I see manly 3 categories (by simplest to most complex) which are described below.

## Comments, suggestions, feature requests or anything similar

Please either contact me on discord or create an [issue](https://gitlab.com/loikki/the-one-app/-/issues) on gitlab.

## Adding / Updating data (culture, calling, virtue, objects)

All the data are written in json in the directory `data`. Each sub directory correspond to one class of data (e.g. culture, calling, ...). If you are not able to find them you can contact me.

Once the data are written, you can either open a merge request (requires a bit of knowledge of git) or send me the files directly.

To make the new data available, the code needs a tiny modification (adding the filename to an enum). If you don't want to loose time looking for it (or don't know how to do it), I am happy to take care of it.

## Code contribution (backend, frontend, pipelines, ...)

The code might not be easy to start with (e.g. lack of comments or documentation). This point was clearly not my focus when I started to implement the one app. I will be happy to onboard you and write more documentation based on your questions.

As a first step, I would recommend trying to run locally (see README.md).

# Technical Information

## Adding / Modifying a Class
The typescript bindings are automatically generated from rust. You just need to run `make test` to generate some new ones.
