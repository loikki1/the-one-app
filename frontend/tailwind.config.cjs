const config = {
	content: [
		'./src/**/*.{html,js,svelte,ts}',
		'./node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'
	],

	plugins: [require('flowbite/plugin')],

	darkMode: 'class',

	theme: {
		extend: {
			colors: {
				primary: {
					50: '#fef2f2',
					100: '#fee2e2',
					200: '#fecaca',
					300: '#fca5a5',
					400: '#f87171',
					500: '#ef4444',
					600: '#AC4739',
					700: '#b91c1c',
					800: '#991b1b',
					900: '#7f1d1d'
				},
				red: '#AC4739',
				white: '#FFFEEE',
				green: '#4F7942',
				brown: '#EBE6D3',
				black: '#5C503C'
			}
		}
	}
};

module.exports = config;
