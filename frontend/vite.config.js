import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vitest/config';

export default defineConfig(async () => {
	/** @type {import('vite').UserConfig} */
	const config = {
		server: {
			host: '0.0.0.0', // listen on all addresses
			port: 1420,
			strictPort: true,
			hmr: {
				protocol: 'ws',
				host: 'localhost',
				port: 5180
			}
		},
		plugins: [sveltekit()],
		test: {
			include: ['src/**/*.{test,spec}.{js,ts}']
		}
	};

	return config;
});
