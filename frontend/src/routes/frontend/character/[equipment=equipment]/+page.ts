import { ItemTypes } from '$lib/enum';

type Item = { equipment: ItemTypes };

export function entries(): Item[] {
	return [
		{ equipment: ItemTypes.Weapon },
		{ equipment: ItemTypes.Armor },
		{ equipment: ItemTypes.Shield },
		{ equipment: ItemTypes.Equipment }
	];
}

export const load = ({ params }: { params: Item }) => {
	return {
		equipment: params.equipment
	};
};
