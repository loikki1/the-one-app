import { Attributes } from '$lib/character/utils';

type Item = { attribute: Attributes };

export function entries() {
	return [
		{ attribute: Attributes.Strength },
		{ attribute: Attributes.Heart },
		{ attribute: Attributes.Wits }
	];
}
export const load = ({ params }: { params: Item }) => {
	return {
		attribute: params.attribute
	};
};
