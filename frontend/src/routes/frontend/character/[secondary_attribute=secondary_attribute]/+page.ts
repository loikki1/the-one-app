import { SecondaryAttributeEnum } from '$lib/enum';

type Item = { secondary_attribute: SecondaryAttributeEnum };

export function entries(): Item[] {
	return [
		{ secondary_attribute: SecondaryAttributeEnum.Valor },
		{ secondary_attribute: SecondaryAttributeEnum.Wisdom }
	];
}

export const load = ({ params }: { params: Item }) => {
	return {
		secondary_attribute: params.secondary_attribute
	};
};
