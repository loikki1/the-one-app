import { print_enum } from './utils';

export class SelectItem {
	value: number;
	name: string;

	constructor(value: number, name: string) {
		this.value = value;
		this.name = name;
	}
}

export function convert_to_select(array: string[] | null | undefined): SelectItem[] {
	if (array === null || array === undefined) {
		return [];
	}
	return array.map((x, i) => {
		return new SelectItem(i, print_enum(x));
	});
}
