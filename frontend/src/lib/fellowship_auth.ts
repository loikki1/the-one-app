import { store_set, store_get, store_remove } from './store';
import { writable, get } from 'svelte/store';
import type { Writable } from 'svelte/store';
import type { Fellowship } from './bindings/Fellowship';
import { get_fellowship, get_player_fellowship } from './queries';
import { toast } from '@zerodevx/svelte-toast';
import { is_player } from './utils';

export const fellowship_auth: Writable<null | FellowshipAuth> = writable(null);
export const fellowship: Writable<null | Fellowship> = writable(null);
export const fellowship_id: Writable<null | number> = writable(null);

export type FellowshipAuth = {
	fellowship: string;
	loremaster: string;
	player: string;
	server: URL;
};
export async function save_fellowship_auth(auth: FellowshipAuth) {
	await store_set('fellowship_auth', JSON.stringify(auth));
}

export function get_players(fellowship: Fellowship | null): string[] {
	if (fellowship == null) {
		return [];
	}
	return fellowship.characters.map((x) => x.player_name);
}

export function get_loremaster(): string {
	const f = get(fellowship_auth);
	if (f == null) {
		throw 'No fellowship available';
	}
	return f.loremaster;
}

export async function init_fellowship() {
	if (get(is_player)) {
		const local_data = await store_get('fellowship_auth');
		if (local_data == null) {
			return;
		} else {
			const local_auth: FellowshipAuth = JSON.parse(local_data);
			fellowship_auth.set(local_auth);
		}
	} else {
		const json_id = await store_get('fellowship_id');
		if (json_id == null) {
			return;
		} else {
			const id = JSON.parse(json_id) as number;
			fellowship_id.set(id);
		}
	}

	await update_fellowship();
}

export async function update_fellowship() {
	try {
		if (get(is_player)) {
			fellowship.set((await get_player_fellowship()).expect('Failed to get fellowship'));
		} else {
			const id = get(fellowship_id);
			if (id != null) {
				fellowship.set((await get_fellowship(id)).expect('Failed to get fellowship'));
			}
		}
	} catch (err) {
		await store_remove('fellowship_id');
		throw err;
	}
}

export async function save_fellowship_id(id: number) {
	await store_set('fellowship_id', JSON.stringify(id));
}

export function get_fellowship_id(): number {
	const f = get(fellowship);
	if (f == null) {
		throw 'No fellowship available';
	}
	if (f.id == null) {
		toast.push('No fellowship id available. This is a bug, please contact me');
		throw 'Fellowship id is null, should not happen';
	}
	return f.id;
}
