export enum FieldTypes {
	Number,
	Text,
	Select,
	TextArea,
	Bool,
	List
}

export enum ItemTypes {
	Shield = 'Shield',
	Armor = 'Armor',
	Weapon = 'Weapon',
	Equipment = 'Equipment'
}

export enum SecondaryAttributeEnum {
	Wisdom = 'Wisdom',
	Valor = 'Valor'
}

export enum Colors {
	Red = '#AC4739',
	White = '#FFFEEE',
	Green = '#4F7942',
	Brown = '#EBE6D3',
	Black = '#000000',
	Grey = '#4B5563',
	Orange = '#FF8C69'
}
