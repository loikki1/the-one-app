// This file was generated by [ts-rs](https://github.com/Aleph-Alpha/ts-rs). Do not edit this file manually.
import type { AttributeField } from './AttributeField';
import type { Shadow } from './Shadow';
import type { StateCap } from './StateCap';

export type Attributes = {
	strength: AttributeField;
	heart: AttributeField;
	wits: AttributeField;
	endurance: StateCap;
	hope: StateCap;
	parry: number;
	shadow: Shadow;
	fatigue: number;
};
