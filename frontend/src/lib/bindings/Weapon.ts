// This file was generated by [ts-rs](https://github.com/Aleph-Alpha/ts-rs). Do not edit this file manually.
import type { CombatProficiency } from './CombatProficiency';

export type Weapon = {
	id: number | null;
	name: string;
	damage: number;
	injury: number;
	two_hand_injury: number | null;
	load: number;
	combat_proficiency: CombatProficiency;
	wearing: boolean;
	notes: string;
};
