// This file was generated by [ts-rs](https://github.com/Aleph-Alpha/ts-rs). Do not edit this file manually.
import type { Character } from './Character';
import type { Version } from './Version';

export type Event =
	| 'UpdateMessage'
	| 'UpdateFellowship'
	| { Version: Version }
	| { Character: Character };
