// This file was generated by [ts-rs](https://github.com/Aleph-Alpha/ts-rs). Do not edit this file manually.
import type { Roll } from './Roll';

export type CharacterRoll = { roll: Roll; success: boolean | null };
