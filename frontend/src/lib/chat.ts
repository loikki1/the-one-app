import { writable, get, type Writable } from 'svelte/store';
import type { Message } from './bindings/Message';
import { get_messages, send_message } from './queries';
import { is_player } from './utils';
import { fellowship_auth, get_fellowship_id } from './fellowship_auth';

export const messages: Writable<Message[]> = writable([]);

function convert_date_to_rfc_3339(date: Date): string {
	return date.toISOString().replace('Z', '');
}

function get_player(player: null | string): string | null {
	if (get(is_player)) {
		const f = get(fellowship_auth);
		if (f == null) {
			console.log('fellowship is null => skipping load messages');
			return null;
		}
		return f.player;
	} else {
		return player;
	}
}

export async function load_messages(player: null | string = null) {
	const resolved_player = get_player(player);
	const fellowship_id = get_fellowship_id();
	messages.set(
		(await get_messages(fellowship_id, resolved_player)).expect('Failed to load messages')
	);
	console.log(get(messages));
}

export async function add_message(player: string, text: string) {
	const message: Message = {
		player: player,
		to_loremaster: get(is_player),
		text: text,
		date: convert_date_to_rfc_3339(new Date())
	};
	const fellowship_id = get_fellowship_id();
	(await send_message(fellowship_id, message)).expect('Failed to send message');
	await load_messages();
}
