import { writeText, readText } from '@tauri-apps/plugin-clipboard-manager';
import { wait_tauri, tauri } from './tauri';
import { get } from 'svelte/store';

export async function to_clipboard(text: string): Promise<void> {
	await wait_tauri();
	if (get(tauri)) {
		await writeText(text);
	} else {
		navigator.clipboard.writeText(text);
	}
}

export async function get_clipboard(): Promise<string> {
	await wait_tauri();
	if (get(tauri)) {
		return await readText();
	} else {
		return await navigator.clipboard.readText();
	}
}
