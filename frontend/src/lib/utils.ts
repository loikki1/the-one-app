console.log('Loading utils');
import { writable, type Writable } from 'svelte/store';
import { Tolgee, DevTools, FormatSimple, LanguageDetector } from '@tolgee/svelte';
import * as local_fr from '$lib/i18n/fr.json';
import * as local_en from '$lib/i18n/en.json';
import * as local_es from '$lib/i18n/es.json';
import type { Character } from './bindings/Character';
import { store_set, store_get } from './store';
import { toast } from '@zerodevx/svelte-toast';
import type { RequestOrigin } from '$lib/bindings/RequestOrigin';

export const is_player: Writable<boolean> = writable(true);
export const locked: Writable<boolean> = writable(true);
export const character_id: Writable<null | number> = writable(null);

// this is made to share character between layout / page
// avoid using it for anything else
export const character: Writable<null | Character> = writable(null);
export const enhanced_accessibility: Writable<boolean> = writable(false);

export const label_class = 'text-center content-center align-middle m-2';
export const button_class =
	'text-gray-500 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-700 focus:outline-none rounded-lg text-sm p-2.5 text-center';
export const accessibility_font_size = 'text-4xl leading-10';

export const tolgee = Tolgee()
	.use(DevTools())
	.use(FormatSimple())
	.use(LanguageDetector())
	.init({
		language: 'en',
		availableLanguages: ['en', 'fr', 'es'],
		fallbackLanguage: 'en',
		apiUrl: import.meta.env.VITE_TOLGEE_API_URL,
		apiKey: import.meta.env.VITE_TOLGEE_API_KEY,
		staticData: {
			en: local_en,
			fr: local_fr,
			es: local_es
		}
	});

export async function init_language(): Promise<void> {
	const lang = await store_get('language');
	if (lang != null) {
		tolgee.changeLanguage(lang);
	}
}

export function raise_error(text: string): never {
	toast.push(text);
	throw text;
}

export function is_dark(): boolean {
	return localStorage.getItem('color-theme') === 'dark';
}

export async function sleep(ms: number): Promise<void> {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

export function set_character(id: number) {
	store_set('character_id', id.toString());
	character_id.set(id);
}

export function print_enum(value: string): string {
	if (value === null) {
		return value;
	}
	return value.split(/(?=[A-Z])/).join(' ');
}

export function to_enum(value: string): string {
	return to_title_case(value.replaceAll('-', ' ')).replaceAll(' ', '');
}

function to_title_case(str: string): string {
	return str.replace(
		/\w\S*/g,
		(text) => text.charAt(0).toUpperCase() + text.substring(1).toLowerCase()
	);
}

export function capitalizeFirstLetter(str: string): string {
	return str.charAt(0).toUpperCase() + str.slice(1);
}

export function is_unique(array: string[]): boolean {
	const unique = array.filter((v, i, a) => {
		return a.indexOf(v) == i;
	});
	return unique.length == array.length;
}

export function is_android(): boolean {
	return /android/i.test(navigator.userAgent);
}

export function is_wounded(character: Character): boolean {
	return character.injury.duration > 0;
}

export function get_origin(url: URL): RequestOrigin {
	return url.host == window.location.host ? 'Local' : 'Extern';
}
