import { call_backend, own_fetch } from './main';
import { toast } from '@zerodevx/svelte-toast';
import type { Version } from '../bindings/Version';
import type { Try } from '$lib/error';
import { Ok } from 'ts-results';

export async function get_version(): Promise<Version> {
	const resp = await own_fetch(null, '/version');
	if (!resp.ok) {
		const text = await resp.text();
		toast.push(`Failed to get version: ${text}`);
	}
	return resp.json();
}

export async function requires_authentication(): Promise<Try<boolean>> {
	const out = await call_backend<string>(
		'GET',
		'/requires-authentication',
		null,
		'Failed to check if requires authentication'
	);
	if (out.err) {
		return out;
	}
	return Ok(out.unwrap() === 'true');
}
