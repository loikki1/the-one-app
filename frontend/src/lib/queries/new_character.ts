import { call_backend } from './main';
import type { Choices } from '../bindings/Choices.ts';
import type { Culture } from '../bindings/Culture.ts';
import type { CultureEnum } from '../bindings/CultureEnum.ts';
import type { Calling } from '../bindings/Calling.ts';
import type { CallingEnum } from '../bindings/CallingEnum.ts';
import type { CombatSkills } from '../bindings/CombatSkills.ts';
import type { SkillEnum } from '../bindings/SkillEnum.ts';
import type { CombatSkillEnum } from '../bindings/CombatSkillEnum.ts';
import type { Try } from '$lib/error';
import type { Character } from '$lib/bindings/Character';

export async function get_choices(): Promise<Try<Choices>> {
	// Get empty character
	return call_backend('GET', '/new-character/empty', null, 'Failed to get empty character');
}

export async function get_culture_options(culture: CultureEnum): Promise<Try<Culture>> {
	return call_backend(
		'GET',
		`/new-character/culture/${culture}`,
		null,
		'Failed to update the cultural options'
	);
}

export async function get_calling_options(calling: CallingEnum): Promise<Try<Calling>> {
	return call_backend(
		'GET',
		`/new-character/calling/${calling}`,
		null,
		'Failed to update the calling options'
	);
}

export async function get_new_character_combat_skills(
	choices: Choices
): Promise<Try<CombatSkills>> {
	return call_backend(
		'POST',
		'/new-character/combat-skills',
		choices,
		'Failed to get the inital combat skills'
	);
}

export async function can_upgrade_skill(
	previous_experience: number,
	skills: number[]
): Promise<Try<SkillEnum[]>> {
	return call_backend(
		'POST',
		`/new-character/skills/can-upgrade/${previous_experience}`,
		skills,
		'Failed to get list of upgradable skills'
	);
}

export async function can_upgrade_combat_skill(
	previous_experience: number,
	combat_skills: number[]
): Promise<Try<CombatSkillEnum[]>> {
	return call_backend(
		'POST',
		`/new-character/combat-skills/can-upgrade/${previous_experience}`,
		combat_skills,
		'Failed to get list of upgradable skills'
	);
}

export async function get_initial_skill_cost(new_value: number): Promise<Try<number>> {
	return call_backend(
		'GET',
		`/new-character/skill-cost/${new_value}`,
		null,
		'Failed to get the cost of the upgrade'
	);
}

// TODO merge with previous
export async function get_initial_combat_skill_cost(new_value: number): Promise<Try<number>> {
	return call_backend(
		'GET',
		`/new-character/combat-skill-cost/${new_value}`,
		null,
		'Failed to get the cost of the upgrade'
	);
}

export async function create_character(choices: Choices): Promise<Try<Character>> {
	return await call_backend('PUT', '/new-character/create', choices, 'Failed to create character');
}
