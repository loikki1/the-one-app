import { call_backend } from './main';
import type { Character } from '../bindings/Character';
import type { SkillEnum } from '../bindings/SkillEnum';
import type { SkillType as SkillTypeFromBackend } from '../bindings/SkillType';
import type { CombatSkillEnum } from '../bindings/CombatSkillEnum';
import type { SecondaryAttributeEnum } from '../enum';
import type { Try } from '$lib/error';
import type { Advantage, AdvantageList } from '$lib/advantage';
import type { RollType } from '$lib/bindings/RollType';
import type { CharacterRoll } from '$lib/bindings/CharacterRoll';
import { send_message_to_socket, SocketMessage } from './socket';

export type SkillType = 'skill' | 'combat-skill';
export type AnySkill = SkillEnum | CombatSkillEnum;

export async function get_character(character_id: number): Promise<Try<Character>> {
	const character = await call_backend<Character>(
		'GET',
		`/character/${character_id}`,
		null,
		`Failed to fetch ${character_id}`
	);

	// Convert fields for frontend
	return character;
}

export async function delete_character(character_id: number) {
	return call_backend(
		'DELETE',
		`/character/${character_id}`,
		null,
		`Failed to delete character ${character_id}`,
		true
	);
}

export async function set_character(character: Character): Promise<Try<number>> {
	const out = await call_backend<number>(
		'PUT',
		`/character/`,
		character,
		`Failed to update ${character.name}`,
		true
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function get_list_characters(): Promise<Try<number[]>> {
	return call_backend('GET', '/character', null, 'Failed to get the list of characters');
}

export async function get_notes(character_id: number): Promise<Try<string>> {
	return call_backend('GET', `/character/${character_id}/notes/`, null, 'Failed to get notes');
}

export async function set_notes(character_id: number, notes: string): Promise<Try<void>> {
	return call_backend(
		'POST',
		`/character/${character_id}/notes/`,
		notes,
		'Failed to update notes',
		true,
		false
	);
}

export async function get_upgrade_skills(character_id: number): Promise<Try<SkillEnum[]>> {
	return call_backend('GET', `/character/${character_id}/skills/can-upgrade`);
}

export async function get_upgrade_combat_skills(
	character_id: number
): Promise<Try<CombatSkillEnum[]>> {
	return call_backend('GET', `/character/${character_id}/combat-skills/can-upgrade`);
}

export async function get_upgrade_secondary_attributes(
	character_id: number
): Promise<Try<SecondaryAttributeEnum[]>> {
	return call_backend('GET', `/character/${character_id}/secondary-attributes/can-upgrade`);
}

export async function update_skill(
	character_id: number,
	skill_type: SkillType,
	delta: number,
	label: AnySkill
): Promise<Try<void>> {
	const out = await call_backend<void>(
		'POST',
		`/character/${character_id}/increase-${skill_type}/${label}/${delta}`,
		label,
		`Failed to increase ${label}`
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function increase_secondary_attribute(
	character_id: number,
	label: SecondaryAttributeEnum,
	advantage: Advantage,
	delta: number,
	free_change = false
): Promise<Try<void>> {
	const free = free_change ? '?free' : '';
	const out = await call_backend<void>(
		'POST',
		`/character/${character_id}/increase-secondary-attribute/${label}/${delta}${free}`,
		advantage,
		`Failed to increase ${label}`
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function loose(
	character_id: number,
	type: 'endurance' | 'hope',
	amount: number
): Promise<Try<number>> {
	const resp = await call_backend<number>(
		'POST',
		`/character/${character_id}/loose-${type}/${amount}`
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return resp;
}

export async function get_current_skill_points(character_id: number): Promise<Try<number>> {
	return call_backend(
		'GET',
		`/character/${character_id}/current-skill-points`,
		null,
		'Failed to get current skill points'
	);
}

export async function get_current_adventure_points(character_id: number): Promise<Try<number>> {
	return call_backend(
		'GET',
		`/character/${character_id}/current-adventure-points`,
		null,
		'Failed to get current adventure points'
	);
}

export async function add_advantage(
	character_id: number,
	adv_type: SecondaryAttributeEnum,
	advantage: Advantage
): Promise<Try<AdvantageList>> {
	const out = await call_backend<AdvantageList>(
		'POST',
		`/character/${character_id}/${adv_type}/advantage`,
		advantage,
		`Failed to add advantage`
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function rest(character_id: number, long: boolean): Promise<Try<void>> {
	const desc = long ? 'long' : 'short';
	const out = await call_backend<void>(
		'POST',
		`/character/${character_id}/${desc}-rest`,
		null,
		`Failed to take a long rest`,
		true
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function take_fellowship_phase(
	character_id: number,
	yule: boolean
): Promise<Try<void>> {
	const out = await call_backend<void>(
		'POST',
		`/character/${character_id}/fellowship-phase/${yule}`,
		null,
		`Failed to take a fellowship phase`,
		true
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function reward_xp(
	character_id: number,
	skill: number,
	adventure: number
): Promise<Try<void>> {
	const out = await call_backend<void>(
		'POST',
		`/character/${character_id}/reward-xp/skill/${skill}/adventure/${adventure}`,
		null,
		`Failed to take reward xp`,
		true
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function convert_shadow(character_id: number): Promise<Try<void>> {
	const out = await call_backend<void>(
		'POST',
		`/character/${character_id}/convert-shadow`,
		null,
		`Failed to convert shadow`,
		true
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function roll(
	character_id: number,
	skill: SkillTypeFromBackend,
	bonus_dices: number,
	roll_type: RollType | null
): Promise<Try<CharacterRoll>> {
	const opts: string[][] = [];
	if (roll_type != null) {
		opts.push(['roll_type', roll_type]);
	}
	opts.push(['bonus_dices', String(bonus_dices)]);
	const params = new URLSearchParams(opts).toString();

	return call_backend('POST', `/character/${character_id}/roll?${params}`, skill, 'Failed to roll');
}
