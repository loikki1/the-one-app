import { is_player, sleep, get_origin, raise_error, character_id } from '$lib/utils';
import { get, writable } from 'svelte/store';
import { get_server, auth } from './main';
import { toast } from '@zerodevx/svelte-toast';
import type { Writable } from 'svelte/store';
import { fellowship_auth, update_fellowship } from '$lib/fellowship_auth';
import { get_version } from './general';
import { load_messages } from '$lib/chat';
import { get_character } from './character';

export enum SocketMessage {
	Version = 'Version',
	UpdateFellowship = 'UpdateFellowship',
	UpdateMessage = 'UpdateMessage'
}

const socket: Writable<null | WebSocket> = writable(null);

// Initialize the two sockets
export async function setup_sockets(): Promise<boolean> {
	console.log('Setting up socket');
	await setup_socket();
	console.log('Socket are set up');
	return true;
}

// // Process a received message from a socket
// function process_socket<Type>(
// 	event: MessageEvent,
// 	data_type: SocketMessage,
// 	func: (_: Type) => void
// ) {
// 	try {
// 		if (event.data.includes('{')) {
// 			const data = JSON.parse(event.data);
// 			if (data_type in data) {
// 				func(data[data_type]);
// 			}
// 		} else {
// 			const data = event.data.replaceAll('"', '');
// 			console.log('Process socket', data, data_type);
// 			if (data == data_type) {
// 				func(data);
// 			}
// 		}
// 	} catch (error) {
// 		console.log('Socket error', error);
// 		console.log('data: ', event.data);
// 		toast.push(`Failed to communicate on the websocket: ${event.data}`);
// 	}
// }

// Process a received message from socket (async)
async function process_async_socket<T>(
	event: MessageEvent,
	data_type: SocketMessage,
	func: (_: T) => Promise<void>
): Promise<void> {
	try {
		if (event.data.includes('{')) {
			const data = JSON.parse(event.data);
			if (data_type in data) {
				await func(data[data_type]);
			}
		} else {
			const data = event.data.replaceAll('"', '');
			console.log('Process socket async', data, data_type);
			if (data == data_type) {
				await func(data);
			}
		}
	} catch (error) {
		console.log('Socket error', error);
		console.log('data: ', event.data);
		toast.push(`Failed to communicate on the websocket: ${event.data}`);
	}
}

// Setup the socket and enable its connection for players
// if credentials are in cache
async function setup_socket() {
	let server = null;
	if (get(is_player)) {
		const auth = get(fellowship_auth);
		if (auth == null) {
			console.log('Fellowship auth is not set => no socket');
			return;
		}
		server = auth.server;
	} else {
		server = get_server();
	}
	server = server.toString().replace('http', 'ws');
	const ws = new WebSocket(server + '/socket/fellowship');

	// Authenticate request
	const wait = 100;
	while (get(auth) == null) {
		await sleep(wait);
	}
	ws.addEventListener('open', async () => {
		const version = await get_version();
		// Check version
		ws.send(JSON.stringify(version));
		if (get(is_player)) {
			const fellowship = get(fellowship_auth);
			if (fellowship == null) {
				toast.push('fellowship is null, this is a bug please contact me');
				throw 'logic error';
			}
			const auth = {
				player: fellowship.player,
				loremaster: fellowship.loremaster,
				fellowship: fellowship.fellowship
			};
			ws.send(JSON.stringify({ Player: auth }));
		} else {
			ws.send(JSON.stringify({ Loremaster: get(auth) }));
		}
		console.log('Socket connection has been established');
	});
	socket.set(ws);

	// Listen to incoming messages
	ws.addEventListener('message', async (event: MessageEvent) => {
		await process_async_socket(event, SocketMessage.UpdateFellowship, async () => {
			await update_fellowship();
		});

		await process_async_socket(event, SocketMessage.UpdateMessage, async () => {
			toast.push('New message');
			await load_messages();
		});

		// Not parsable case
		process_socket_error(event);
	});
}

export async function send_message_to_socket(message: SocketMessage): Promise<void> {
	const s = get(socket);
	if (s == null) {
		return;
	}
	if (message == SocketMessage.UpdateFellowship) {
		const auth = get(fellowship_auth);
		if (auth == null) {
			return;
		}
		if (get_origin(auth.server) == 'Extern') {
			const id = get(character_id);
			if (id == null) {
				raise_error('No character id found, cannot update fellowship');
			}
			const c = (await get_character(id)).expect('Failed to get character');
			s.send(JSON.stringify({ Character: c }));
		}
	}
	s.send(JSON.stringify(message));
}

// Simple error process with a toast
function process_socket_error(event: MessageEvent) {
	if (event.data.includes('error') || event.data.includes('Error')) {
		toast.push(`Error: ${event.data}`);
	}
}
