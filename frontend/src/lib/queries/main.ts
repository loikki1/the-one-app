import { get, writable } from 'svelte/store';
import { toast } from '@zerodevx/svelte-toast';
import { fetch as tauri_fetch } from '@tauri-apps/plugin-http';
import { tauri, set_running_with_tauri, wait_tauri } from '$lib/tauri';
import type { Writable } from 'svelte/store';
import type { UserInClear } from '$lib/bindings/UserInClear';
import { Err, Ok, type Result } from 'ts-results';
import { MyError } from '$lib/error';
import { fellowship_auth } from '$lib/fellowship_auth';
import { is_player } from '$lib/utils';

export const auth: Writable<null | UserInClear> = writable(null);
export const use_auth: Writable<boolean> = writable(true);

function is_dev(): boolean {
	return import.meta.env.DEV;
}

export async function own_fetch(server: URL | null, path: string, ...args: unknown[]) {
	await wait_tauri();
	const url = (server == null ? get_server() : server) + path;
	if (get(tauri)) {
		// @ts-expect-error just want my own fetch
		return tauri_fetch(url, ...args);
	} else {
		// @ts-expect-error just want my own fetch
		return fetch(url, ...args);
	}
}

export async function init_backend() {
	console.log('Init backend');
	await set_running_with_tauri();
	console.log('Backend has been initialized');
}

export function get_server(): URL {
	if (get(tauri)) {
		return new URL('http://localhost:8000');
	} else if (is_dev()) {
		return new URL('http://localhost:8000');
	} else {
		return new URL(window.location.origin);
	}
}

export async function call_backend<Type>(
	method: string,
	path: string,
	body: object | string | null = null,
	error_message: string = 'Failed to call backend',
	no_answer: boolean = false,
	json: boolean = true,
	server: URL | null = null
): Promise<Result<Type, MyError>> {
	if (body !== null && json) {
		body = JSON.stringify(body);
	}
	try {
		const headers: { [id: string]: string } = {};
		if (server == null) {
			const local_auth = get(auth);
			if (local_auth != null) {
				headers['username'] = local_auth.username;
				headers['password'] = local_auth.password;
			}
		} else {
			const local_auth = get(fellowship_auth);
			if (local_auth != null) {
				headers['fellowship'] = local_auth.fellowship;
				headers['loremaster'] = local_auth.loremaster;
			}
		}
		const response = await own_fetch(server, path, {
			method: method,
			body: body,
			headers: headers
		});
		if (!response.ok) {
			const text = await response.text();
			toast.push(`${error_message}: ${text}`);
			return Err(new MyError(text));
		}
		if (no_answer) {
			return Ok(null as Type);
		}
		return Ok(await response.json());
	} catch (error) {
		console.log(error);
		toast.push(`Failed to send a request to the backend: ${error}`);
		return Err(new MyError(String(error)));
	}
}

export async function call_fellowship_backend<Type>(
	method: string,
	path: string,
	body: object | string | null = null,
	error_message: string = 'Failed to call backend',
	no_answer: boolean = false,
	json: boolean = true
): Promise<Result<Type, MyError>> {
	const auth = get(fellowship_auth);
	const server = auth == null ? null : auth.server;
	return call_backend(method, path, body, error_message, no_answer, json, server);
}

export async function call_role_dependend_backend<Type>(
	method: string,
	path: string,
	body: object | string | null = null,
	error_message: string = 'Failed to call backend',
	no_answer: boolean = false,
	json: boolean = true
): Promise<Result<Type, MyError>> {
	if (get(is_player)) {
		return call_fellowship_backend(method, path, body, error_message, no_answer, json);
	} else {
		return call_backend(method, path, body, error_message, no_answer, json);
	}
}

export async function check_authentication(username: string, password: string): Promise<boolean> {
	const headers = {
		username: username,
		password: password
	};

	const response = await own_fetch(null, '/check-auth', {
		headers: headers
	});

	return response.ok;
}

export async function create_user(username: string, password: string): Promise<boolean> {
	try {
		const response = await own_fetch(null, `/create-user/${username}`, {
			method: 'POST',
			body: password
		});
		if (!response.ok) {
			const text = await response.text();
			toast.push(`Failed to create account: ${text}`);
		}
		return response.ok;
	} catch (error) {
		console.log(error);
		toast.push(`Failed to create account: ${error}`);
		return false;
	}
}
