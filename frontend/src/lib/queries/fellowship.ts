import { call_backend, call_fellowship_backend, call_role_dependend_backend } from './main';
import type { UndertakingEnum } from '../bindings/UndertakingEnum';
import type { Undertaking } from '../bindings/Undertaking';
import type { Try } from '$lib/error';
import type { Fellowship } from '$lib/bindings/Fellowship';
import type { Patron } from '$lib/bindings/Patron';
import type { AddToFellowship } from '$lib/bindings/AddToFellowship';
import type { Adversary } from '$lib/bindings/Adversary';

export async function get_undertaking(undertaking: UndertakingEnum): Promise<Try<Undertaking>> {
	return call_backend(
		'GET',
		`/fellowship/undertaking/${undertaking}`,
		null,
		`Failed to fetch an undertaking ${undertaking}`
	);
}

export async function get_fellowships(): Promise<Try<Fellowship[]>> {
	return call_backend('GET', '/fellowship', null, 'Failed to fetch fellowships');
}

export async function get_fellowship(id: number): Promise<Try<Fellowship>> {
	return call_backend('GET', `/fellowship/${id}`, null, `Failed to fetch fellowship ${id}`);
}

export async function set_fellowship(fellowship: Fellowship): Promise<Try<number>> {
	return call_backend(
		'POST',
		'/fellowship',
		fellowship,
		`Failed to set fellowship ${fellowship.name}`
	);
}
export async function delete_fellowship(id: number): Promise<Try<Patron>> {
	return call_backend(
		'DELETE',
		`/fellowship/${id}`,
		null,
		`Failed to delete fellowship ${id}`,
		true
	);
}

export async function get_patrons(): Promise<Try<Patron[]>> {
	return call_backend('GET', '/fellowship/patron', null, 'Failed to fetch patrons');
}

export async function get_patron(id: number | string): Promise<Try<Patron>> {
	return call_backend('GET', `/fellowship/patron/${id}`, null, `Failed to fetch patron ${id}`);
}

export async function delete_patron(id: number): Promise<Try<void>> {
	return call_backend(
		'DELETE',
		`/fellowship/patron/${id}`,
		null,
		`Failed to delete patron ${id}`,
		true
	);
}

export async function add_character_to_fellowship(
	server: URL,
	data: AddToFellowship
): Promise<Try<void>> {
	return call_backend(
		'POST',
		`/fellowship/character`,
		data,
		`Failed to add to fellowship`,
		true,
		true,
		server
	);
}

export async function add_adversary_to_fellowship(
	fellowship_id: number,
	adversary: Adversary
): Promise<Try<void>> {
	return call_backend(
		'POST',
		`/fellowship/${fellowship_id}/adversary`,
		adversary,
		'Failed to add adversary to fellowship',
		true
	);
}

export async function delete_adversary_from_fellowship(
	fellowship_id: number,
	adversary_id: number
): Promise<Try<void>> {
	return call_backend(
		'DELETE',
		`/fellowship/${fellowship_id}/adversary/${adversary_id}`,
		null,
		`Failed to remove adversary ${adversary_id} to fellowship`,
		true
	);
}

export async function delete_character_from_fellowship(
	fellowship_id: number,
	character_id: number
): Promise<Try<void>> {
	return call_backend(
		'DELETE',
		`/fellowship/${fellowship_id}/character/${character_id}`,
		null,
		`Failed to remove character ${character_id} to fellowship`,
		true
	);
}

export async function get_player_fellowship(): Promise<Try<Fellowship>> {
	return call_fellowship_backend('GET', '/fellowship/player', null, 'Failed to get fellowship');
}

export async function increase_eye(fellowship_id: number, inc: number): Promise<Try<number>> {
	return call_backend(
		'POST',
		`/fellowship/${fellowship_id}/increase-eye/${inc}`,
		null,
		'Failed to increase eye'
	);
}

export async function reset_fellowship_points(fellowship_id: number): Promise<Try<number>> {
	return call_backend(
		'POST',
		`/fellowship/${fellowship_id}/reset-points`,
		null,
		'Failed to reset points'
	);
}

export async function reset_eye(fellowship_id: number): Promise<Try<number>> {
	return call_backend(
		'POST',
		`/fellowship/${fellowship_id}/reset-eye`,
		null,
		'Failed to reset eye'
	);
}

export async function increase_points(fellowship_id: number, inc: number): Promise<Try<number>> {
	return call_role_dependend_backend(
		'POST',
		`/fellowship/${fellowship_id}/increase-points/${inc}`,
		null,
		'Failed to increase eye'
	);
}
