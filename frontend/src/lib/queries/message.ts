import { call_role_dependend_backend } from './main';
import type { Try } from '$lib/error';
import type { Message } from '$lib/bindings/Message';

export async function send_message(fellowship_id: number, message: Message): Promise<Try<void>> {
	return call_role_dependend_backend(
		'POST',
		`/message/${fellowship_id}`,
		message,
		`Failed to send message`,
		true
	);
}

export async function get_messages(
	fellowship_id: number,
	player: string | null
): Promise<Try<Message[]>> {
	let path = '';
	if (player == null) {
		path = `/message/${fellowship_id}`;
	} else {
		path = encodeURI(`/message/${fellowship_id}?player=${player}`);
	}
	return call_role_dependend_backend('GET', path, null, 'Failed to fetch messages');
}
