import { call_backend } from './main';
import type { AnyItem } from './enum';
import { ItemTypes } from '$lib/enum';
import type { Weapon } from '$lib/bindings/Weapon';
import { send_message_to_socket, SocketMessage } from './socket';
import type { Try } from '$lib/error';

export async function set_item(
	character_id: number,
	item_type: ItemTypes,
	item: AnyItem
): Promise<Try<void>> {
	if (item_type == ItemTypes.Weapon && (item as Weapon).two_hand_injury == 0) {
		(item as Weapon).two_hand_injury = null;
	}
	const out = await call_backend<void>(
		'PUT',
		`/equipment/${character_id}/${item_type}`,
		item,
		`Failed to add a ${item.name}`,
		true
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function delete_item(
	character_id: number,
	item_type: ItemTypes,
	id: number
): Promise<Try<void>> {
	const out = await call_backend<void>(
		'DELETE',
		`/equipment/${character_id}/${item_type}/${id}`,
		null,
		`Failed to delete ${id}`,
		true
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}

export async function update_wearing(
	character_id: number,
	item_type: ItemTypes,
	id: number,
	value: boolean
): Promise<Try<void>> {
	const out = await call_backend<void>(
		'PUT',
		`/equipment/${character_id}/${item_type}/wearing/${id}/${value}`,
		null,
		`Failed to update wearing for ${id}`,
		true
	);
	await send_message_to_socket(SocketMessage.UpdateFellowship);
	return out;
}
