import { call_backend } from './main';
import type { NameGenerators } from '../bindings/NameGenerators';
import type { Genders } from '../bindings/Genders';
import type { RollType } from '../bindings/RollType';
import type { RollStatistics } from '../bindings/RollStatistics';
import type { Try } from '$lib/error';

export async function get_genders(race: NameGenerators): Promise<Try<Genders[]>> {
	return call_backend(
		'GET',
		`/name-generator/${race}/genders`,
		null,
		`Failed to fetch the genders for ${race}`
	);
}

export async function roll_name_generator(
	n_rolls: number,
	race: NameGenerators,
	gender: Genders
): Promise<Try<string[]>> {
	return call_backend(
		'POST',
		`/name-generator/roll/${n_rolls}/${race}/${gender}`,
		null,
		`Failed to roll for the name generator`
	);
}

export async function roll_random_features(
	n_rolls: number,
	positive: boolean
): Promise<Try<string[]>> {
	return call_backend(
		'POST',
		`/random-features/roll/${n_rolls}/${positive}`,
		null,
		`Failed to roll for the random features`
	);
}

export async function get_roll_statistics(
	n_rolls: number,
	number_dices: number,
	roll_type: RollType,
	miserable: boolean,
	weary: boolean
): Promise<Try<RollStatistics>> {
	return call_backend(
		'GET',
		`/roll-statistics/${n_rolls}/${number_dices}/${roll_type}/${miserable}/${weary}`,
		null,
		`Failed to get roll statistics`
	);
}
