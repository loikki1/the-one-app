import type { Adversary } from '$lib/bindings/Adversary';
import type { HateEnum } from '$lib/bindings/HateEnum';
import type { Try } from '$lib/error';
import { call_backend } from './main';

export async function get_empty_adversary(): Promise<Try<Adversary>> {
	return call_backend('GET', `/adversary/empty`, null, `Failed to fetch empty adversary`);
}

export async function get_list_adversaries(): Promise<Try<Adversary[]>> {
	return call_backend('GET', `/adversary`, null, `Failed to fetch list of adversaries`);
}

export async function get_adversary(name_or_id: string | number): Promise<Try<Adversary>> {
	return call_backend('GET', `/adversary/${name_or_id}`, null, `Failed to fetch the adversary`);
}

export async function delete_adversary(id: number): Promise<Try<void>> {
	return call_backend('DELETE', `/adversary/${id}`, null, `Failed to delete adversary ${id}`, true);
}

export async function set_adversary(adv: Adversary): Promise<Try<number>> {
	return call_backend('PUT', `/adversary`, adv, `Failed to update ${adv.name}`, true);
}

export async function adversary_loose(
	adversary_id: number,
	type: 'Endurance' | HateEnum,
	amount: number
): Promise<Try<void>> {
	return await call_backend(
		'POST',
		`/adversary/${adversary_id}/loose-${type}/${amount}`,
		null,
		`Failed to remove adversary ${type}`,
		true
	);
}
