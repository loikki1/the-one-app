import { call_backend } from './main';
import type { CultureEnum } from '../bindings/CultureEnum.ts';
import type { CallingEnum } from '../bindings/CallingEnum.ts';
import type { CombatSkillEnum } from '../bindings/CombatSkillEnum.ts';
import type { CombatProficiency } from '../bindings/CombatProficiency.ts';
import type { SkillEnum } from '../bindings/SkillEnum.ts';
import type { UndertakingEnum } from '../bindings/UndertakingEnum.ts';
import type { SecondaryAttributeEnum } from '../enum';
import type { StandardOfLiving } from '../bindings/StandardOfLiving.ts';
import type { EquipmentType } from '../bindings/EquipmentType.ts';
import type { Weapon } from '../bindings/Weapon.ts';
import type { Equipment } from '../bindings/Equipment.ts';
import type { Armor } from '../bindings/Armor.ts';
import type { Shield } from '../bindings/Shield.ts';
import type { NameGenerators } from '../bindings/NameGenerators.ts';
import type { RollType } from '../bindings/RollType.ts';
import type { WeaponEnum } from '../bindings/WeaponEnum.ts';
import type { ArmorEnum } from '../bindings/ArmorEnum.ts';
import type { ShieldEnum } from '../bindings/ShieldEnum.ts';
import type { Region } from '../bindings/Region.ts';
import type { Try } from '$lib/error';
import type { Advantage, AdvantageEnum, AdvantageMap, AdvantageType } from '$lib/advantage';
import { Ok } from 'ts-results';

export type AnyItem = Weapon | Armor | Shield | Equipment;
export type AnyListItem = Weapon[] | Armor[] | Shield[] | Equipment[];
export type AnyItemEnum = WeaponEnum | ArmorEnum | ShieldEnum;
export type AnyListItemEnum = WeaponEnum[] | ArmorEnum[] | ShieldEnum[];

export async function get_cultures(): Promise<Try<CultureEnum[]>> {
	return await call_backend('GET', '/cultures', null, 'Failed to fetch the list of cultures');
}

export async function get_callings(): Promise<Try<CallingEnum[]>> {
	return call_backend('GET', '/callings', null, 'Failed to fetch the list of callings');
}

export async function get_combat_skills(): Promise<Try<CombatSkillEnum[]>> {
	return call_backend('GET', '/combat-skills', null, 'Failed to fetch the list of combat skills');
}

export async function get_combat_proficiencies(): Promise<Try<CombatProficiency[]>> {
	return call_backend(
		'GET',
		'/combat-proficiencies',
		null,
		'Failed to fetch the list of combat proficiencies'
	);
}

export async function get_skills(): Promise<Try<SkillEnum[]>> {
	return call_backend('GET', '/skills', null, 'Failed to fetch the list of skills');
}
export async function get_undertakings(): Promise<Try<UndertakingEnum[]>> {
	return call_backend('GET', '/undertaking', null, 'Failed to fetch the list of undertaking');
}

export async function get_secondary_attributes(): Promise<Try<SecondaryAttributeEnum[]>> {
	return call_backend(
		'GET',
		'/secondary-attributes',
		null,
		'Failed to fetch the list of secondary skills'
	);
}

export async function get_standard_living(): Promise<Try<StandardOfLiving[]>> {
	return call_backend(
		'GET',
		'/standard-living',
		null,
		'Failed to fetch the list of standard of living'
	);
}

export async function get_empty_item(type: EquipmentType): Promise<Try<AnyItem>> {
	return call_backend('GET', `/empty/${type}`, null, `Failed to get an empty ${type}`);
}

export async function get_advantage(type: SecondaryAttributeEnum): Promise<Try<Advantage>> {
	return call_backend('GET', `/empty/${type}`, null, `Failed to get an empty ${type}`);
}

export async function get_name_generator_race(): Promise<Try<NameGenerators[]>> {
	return call_backend(
		'GET',
		'/name-generator/races',
		null,
		'Failed to fetch the list of races for the name generator'
	);
}

export async function get_roll_type(): Promise<Try<RollType[]>> {
	return call_backend('GET', '/roll-type', null, 'Failed to fetch the roll types');
}

export async function get_item(
	item_type: EquipmentType,
	item_name: AnyItemEnum
): Promise<Try<AnyItem>> {
	return call_backend('GET', `/${item_type}/${item_name}`, null, 'Failed to fetch item');
}

export async function get_list_items(item_type: EquipmentType): Promise<Try<AnyListItemEnum>> {
	return call_backend('GET', `/item/${item_type}`, null, 'Failed to fetch the list of items');
}

export async function get_standard_advantages(
	type: AdvantageType,
	culture: CultureEnum,
	at_creation: boolean
): Promise<Try<AdvantageMap>> {
	const list_params = [];
	if (culture != null) {
		list_params.push(`culture=${culture}`);
	}
	if (at_creation != null) {
		list_params.push(`creation=${at_creation}`);
	}
	const params = list_params.length == 0 ? '' : '?' + list_params.join('&');
	const out = await call_backend('GET', `/${type}${params}`);
	if (!out.ok) {
		return out;
	}
	// @ts-expect-error	Weird behavior from linter
	return Ok(new Map(Object.entries(out.unwrap())) as AdvantageMap);
}

export async function get_standard_advantage(
	type: AdvantageType,
	name: AdvantageEnum
): Promise<Try<Advantage>> {
	return call_backend('GET', `/${type}/${name}`);
}

export async function get_eye_threshold(eye: number): Promise<Try<Region>> {
	return call_backend('GET', `/eye-threshold/${eye}`, null, 'Failed to get eye threshold');
}
