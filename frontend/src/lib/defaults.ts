import type { Choices } from './bindings/Choices';
import type { Character } from './bindings/Character';
import type { CombatSkills } from './bindings/CombatSkills';
import type { Culture } from './bindings/Culture';
import type { DerivedStats } from './bindings/DerivedStats';
import type { Improvements } from './bindings/Improvements';
import type { Items } from './bindings/Items';
import type { ItemsDerived } from './bindings/ItemsDerived';
import type { Reward } from './bindings/Reward';
import type { Roll } from './bindings/Roll';
import type { RollStatistics } from './bindings/RollStatistics';
import type { Treasure } from './bindings/Treasure';
import type { TreasureDerived } from './bindings/TreasureDerived';
import type { Virtue } from './bindings/Virtue';
import type { Patron } from './bindings/Patron';
import type { Fellowship } from './bindings/Fellowship';
import type { AddToFellowship } from '$lib/bindings/AddToFellowship';
import { auth } from './queries';
import { get } from 'svelte/store';

export function get_default_items_derived(): ItemsDerived {
	return {
		protection: 0,
		parry: 0,
		load: 0
	};
}

export function get_default_treasure_derived(): TreasureDerived {
	return {
		standard_of_living: 'Frugal'
	};
}

export function get_default_treasure(): Treasure {
	return {
		total: 0,
		carrying: 0,
		derived: get_default_treasure_derived()
	};
}

export function get_default_items(): Items {
	return {
		weapons: [],
		armors: [],
		shields: [],
		equipments: [],
		treasure: get_default_treasure(),
		derived: get_default_items_derived()
	};
}

export function get_default_reward(): Reward {
	return {
		id: 0,
		title: '',
		description: '',
		available_at_creation: true
	};
}

export function get_default_virtue(): Virtue {
	return {
		id: 0,
		title: '',
		description: '',
		text_input: '',
		available: true,
		available_at_creation: true,
		restricted_to: null,
		implemented: false
	};
}

export function get_default_improvements(): Improvements {
	return {
		skills: {},
		combat: {}
	};
}

export function get_default_choices(): Choices {
	return {
		character_name: '',
		culture: 'Barding',
		calling: 'Warden',
		attributes: 0,
		culture_skill: null,
		calling_skills: [],
		distinctive_features: [],
		all_combat_skill: null,
		available_combat_skill: null,
		items: get_default_items(),
		reward: get_default_reward(),
		virtue: get_default_virtue(),
		previous_experience: 0,
		improvements: get_default_improvements(),
		one_shot_rules: false,
		strider_mode: false
	};
}

export function get_default_combat_skills(): CombatSkills {
	return {
		skills: [0, 0, 0, 0]
	};
}

export function get_default_derived_stats(): DerivedStats {
	return {
		endurance: 0,
		hope: 0,
		parry: 0
	};
}

export function get_default_culture(): Culture {
	return {
		attributes: [],
		treasures: 0,
		blessings: [],
		derived_stats: get_default_derived_stats(),
		favoured_skills: [],
		skills: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		combat_skills: [],
		distinctive_features: []
	};
}

export function get_default_roll_statistics(): RollStatistics {
	return {
		average_value: 0,
		average_tengwar: 0,
		number_gandalf: 0,
		probabilities: []
	};
}

export function get_default_patron(): Patron {
	return {
		id: null,
		name: '',
		fellowship_points: 0,
		advantage: ''
	};
}

export function get_default_fellowship(): Fellowship {
	return {
		id: null,
		patron: get_default_patron(),
		characters: [],
		adversaries: [],
		name: '',
		points: 0,
		eye: 0
	};
}

export function get_default_roll(): Roll {
	return {
		success_dices: [],
		feat_dice: { Normal: 0 },
		total: null
	};
}

export function get_default_add_to_fellowship(character: Character): AddToFellowship {
	const authentication = get(auth);
	return {
		character: character,
		fellowship: '',
		loremaster: '',
		user: authentication == null ? '' : authentication.username,
		origin: 'Local'
	};
}
