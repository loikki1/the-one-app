import { getTauriVersion } from '@tauri-apps/api/app';
import { writable, get, type Writable } from 'svelte/store';
import { sleep, character_id, locked, is_player } from './utils';
import { store_get, store_init } from './store';

export const tauri: Writable<null | boolean> = writable(null);

export async function wait_tauri(): Promise<void> {
	let time = 0;
	const wait = 100;
	const max_wait = 2000;
	while (get(tauri) == null && time < max_wait) {
		await sleep(wait);
		time += wait;
	}
	if (get(tauri) == null) {
		throw new Error('Tauri is still not discovered');
	}
}

export async function set_running_with_tauri(): Promise<void> {
	console.log('Init tauri');
	try {
		const version = await getTauriVersion();
		console.log('With tauri', version);
		await store_init();
		tauri.set(true);
	} catch (error) {
		console.log(`Without tauri: ${error}`);
		tauri.set(false);
	}
	console.log('Setting variables');
	const id = await store_get('character_id', null);
	character_id.set(id == null ? id : Number(id));
	locked.set((await store_get('locked', 'true')) == 'true');
	is_player.set((await store_get('is_player', 'true')) == 'true');
	console.log('Variables set');
}
