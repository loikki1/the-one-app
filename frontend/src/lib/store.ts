import { Store, load } from '@tauri-apps/plugin-store';
import { tauri, wait_tauri } from './tauri';
import { get } from 'svelte/store';
import { toast } from '@zerodevx/svelte-toast';

let tauri_store: Store | null = null;

export async function store_init() {
	try {
		tauri_store = await load('.settings.dat');
		console.log('Store has been initialized');
	} catch (error) {
		console.log(`Store init failed ${error}`);
	}
}

export async function store_get(
	key: string,
	default_value: null | string = null
): Promise<string | null> {
	console.log(`Reading ${key}`);
	await wait_tauri();
	if (get(tauri)) {
		if (tauri_store == null) {
			toast.push('Tauri store is null, this is a bug');
			throw 'tauri store is null';
		}
		const val = await tauri_store.get<string>(key);
		return val === undefined ? default_value : val;
	} else {
		const val = localStorage.getItem(key);
		return val == null ? default_value : val;
	}
}

export async function store_set(key: string, value: string): Promise<void> {
	console.log(`Saving ${key}`);
	await wait_tauri();
	if (get(tauri)) {
		if (tauri_store == null) {
			toast.push('Tauri store is null, this is a bug');
			throw 'tauri store is null';
		}
		localStorage.setItem(key, value);
		await tauri_store.set(key, value);
		await tauri_store.save();
	} else {
		localStorage.setItem(key, value);
	}
}

export async function store_remove(key: string): Promise<void> {
	console.log(`Removing ${key}`);
	await wait_tauri();
	if (get(tauri)) {
		if (tauri_store == null) {
			toast.push('Tauri store is null, this is a bug');
			throw 'tauri store is null';
		}
		tauri_store.delete(key);
	} else {
		localStorage.removeItem(key);
	}
}
