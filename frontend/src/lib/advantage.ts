import type { Reward } from './bindings/Reward';
import type { RewardEnum } from './bindings/RewardEnum';
import type { Virtue } from './bindings/Virtue';
import type { VirtueEnum } from './bindings/VirtueEnum';

export enum AdvantageType {
	Virtue = 'virtue',
	Reward = 'reward'
}

export type AdvantageEnum = RewardEnum | VirtueEnum;

export type Advantage = Reward | Virtue;
export type AdvantageList = Reward[] | Virtue[];
export type AdvantageMap = Map<AdvantageEnum, Advantage>;

export function advantage_get_implemented(adv: Advantage): undefined | boolean {
	if ('implemented' in adv) {
		return adv.implemented;
	} else {
		return undefined;
	}
}
