export enum NavPages {
	Description,
	Notes,
	Strength,
	Heart,
	Wits,
	Combat,
	Rewards,
	Virtues,
	Weapon,
	Armor,
	Shield,
	Equipment
}

export enum Attributes {
	Strength = 'strength',
	Heart = 'heart',
	Wits = 'wits'
}

export enum AttributeSecondaryLabel {
	Hope = 'hope',
	Endurance = 'endurance',
	Parry = 'parry'
}
