export function match(param) {
	return param === 'strength' || param === 'heart' || param === 'wits';
}
