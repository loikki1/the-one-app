export function match(param) {
	return param === 'Equipment' || param === 'Shield' || param === 'Armor' || param === 'Weapon';
}
