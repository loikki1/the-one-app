extern crate proc_macro;
use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, Data, DeriveInput};

// Example of expansion
// impl<'a> FromParam<'a> for CallingEnum {
//     type Error = &'a str;

//     fn from_param(param: &'a str) -> Result<Self, Self::Error> {
//         match param {
//             "Captain" => Ok(CallingEnum::Captain),
//             "Champion" => Ok(CallingEnum::Champion),
//             "Messenger" => Ok(CallingEnum::Messenger),
//             "Scholar" => Ok(CallingEnum::Scholar),
//             "TreasureHunter" => Ok(CallingEnum::TreasureHunter),
//             "Warden" => Ok(CallingEnum::Warden),
//             _ => Err("Failed to find enum"),
//         }
//     }
// }
#[proc_macro_derive(FromParamEnum)]
pub fn derive_macro_param(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;

    let mut matches = quote!();

    match input.data {
        Data::Enum(ref data) => {
            for field in data.variants.iter() {
                let field_name = &field.ident;
                matches.extend(quote!(
                    stringify!(#field_name) => Ok(#name::#field_name),
                ))
            }
        }
        _ => panic!("Work only on enums"),
    };

    let expanded = quote! {
        impl<'a> FromParam<'a> for #name {
            type Error = &'a str;

            fn from_param(param: &'a str) -> std::result::Result<Self, Self::Error> {
                match param {
                    #matches
                    _ => Err("Failed to find enum")
                }
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}
