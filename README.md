# The One App
[![discord](https://img.shields.io/badge/Discord-7289DA)](https://discord.gg/ZFstbnfppZ)
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="40">](https://f-droid.org/en/packages/io.theoneapp/)

Application for managing characters in the RPG The One Ring for both the players and the loremaster.
It works on Linux, Android, Windows and MacOS. Due to Apple, I will not release an iOS application until they open their store but I provide a [cloud version](https://the-one-app.loikki.ch). 
The application is developed in order to be compatible with iOS. 

Contributions, comments, feature requests are welcomed. A contribution guide is available in gitlab. If you don't know how to proceed, please contact me on [discord](https://discord.gg/ZFstbnfppZ).

This software is totally independent from Free League Publishing and totally free (as a beer and freedom). If Free League Publishing has any legal issue with this application, please contact me directly and I will make the required changes. 

## Advices

- Many elements in the user interface are interactive (stats diamonds, endurance / hope bar, injuries, valor / wisdom stat, ...), click everywhere if you are looking for something.

## Cloud Version

I am providing the cloud version without any guarantees. I will do my best to guarantee a good service quality but it can be removed at any time, the data can be lost and any other unfortunate events.
I am doing automatic weekly backups and keep them for 3 weeks to prevent the risk of loosing data.

### User Accounts

Feel free to create as many accounts as you wish but be careful because I do not provide any way of self recovering the accounts. I decided against asking for an email address (which could be used for recovery) in order to ensure total privacy.

## For Loremaster

The app is configured by default for players. If you wish to use it as a loremaster, you will need to change the mode in the `settings` tab.

## Sharing Data Between the Loremaster and the Players

The setup for sharing data between the loremaster and the players is using the loremaster application as the central hub.
So the loremaster needs to have the app exposed to internet. The easiest solution is to use my [cloud version](https://the-one-app.loikki.ch). Once the account created, you can switch to the loremaster mode in `settings`, setup a password and enable the connection in the same page. Your browser will save the connection password so if you quit the app, next time the connection will be automatically setup.


# Screenshots


## Player
![home page](fastlane/metadata/android/en-US/images/phoneScreenshots/home.png){width=25% height=25%}
![character creation](fastlane/metadata/android/en-US/images/phoneScreenshots/character-creation.png){width=25% height=25%}
![statistics](fastlane/metadata/android/en-US/images/phoneScreenshots/strength.png){width=25% height=25%}
![valor](fastlane/metadata/android/en-US/images/phoneScreenshots/valor.png){width=25% height=25%}
![weapon](fastlane/metadata/android/en-US/images/phoneScreenshots/weapon.png){width=25% height=25%}

## Loremaster
![adversary](fastlane/metadata/android/en-US/images/phoneScreenshots/loremaster-adversary.png){width=25% height=25%}
![dashboard](fastlane/metadata/android/en-US/images/phoneScreenshots/loremaster-dashboard.png){width=25% height=25%}

## Tools
![name generator](fastlane/metadata/android/en-US/images/phoneScreenshots/name-generator.png){width=25% height=25%}
![random features](fastlane/metadata/android/en-US/images/phoneScreenshots/random-features.png){width=25% height=25%}
![roll analysis](fastlane/metadata/android/en-US/images/phoneScreenshots/roll-analysis.png){width=25% height=25%}

# For Developers

## Running Locally

First, you will need to install [rust](https://www.rust-lang.org/learn/get-started) and npm (I recommend using [nvm](https://github.com/nvm-sh/nvm)). Once it is done, you can run `make frontend-install-dep` to install the required dependencies and `make frontend-build` to ensure that the backend is finding the required files.

If you wish to run The One App as a tauri, you will need also tauri-cli 2.0.0: `cargo install tauri-cli@2.0.0-beta.18 --locked` (see `.gitlab-ci.yml` for the exact version),

For developing and testing, I run the backend and the frontend independently with `make run-server` and `make frontend-run`. You can then open your browser on [localhost](http://localhost:1420).


### Note for MacOS Users

Make on macos is extremely old so you might have some issues with the instruction `.ONESHELL`. I would suggest to [upgrade it](https://stackoverflow.com/a/65393811)

You might have a compilation error. If it is the case, please try to compile with [CI=true](https://github.com/tauri-apps/tauri/issues/3055#issuecomment-1661286422)
