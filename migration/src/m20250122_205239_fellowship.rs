use crate::m20220101_000001_create_table::{Adversary, Character, User};
use enums::character_type::CharacterType;
use sea_orm::Iterable;
use sea_orm_migration::{prelude::*, schema::*};

#[derive(DeriveMigrationName)]
pub struct Migration;

const COLUMN_CHARACTER_TYPE: &str = "char_type";
const COLUMN_PLAYER_NAME: &str = "player_name";

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Character
        manager
            .alter_table(
                Table::alter()
                    .table(Character::Table)
                    .add_column(
                        ColumnDef::new(Alias::new(COLUMN_CHARACTER_TYPE))
                            .enumeration(Alias::new(COLUMN_CHARACTER_TYPE), CharacterType::iter())
                            .not_null()
                            .default(format!("\"{}\"", CharacterType::Internal.to_string()))
                            .to_owned(),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .alter_table(
                Table::alter()
                    .table(Character::Table)
                    .add_column(
                        ColumnDef::new(Alias::new(COLUMN_PLAYER_NAME))
                            .string()
                            .not_null()
                            .default("".to_string())
                            .to_owned(),
                    )
                    .to_owned(),
            )
            .await?;

        // Patron
        manager
            .create_table(
                Table::create()
                    .table(Patron::Table)
                    .if_not_exists()
                    .col(pk_auto(Patron::Id))
                    .col(string(Patron::Name))
                    .col(string(Patron::Advantage))
                    .col(unsigned(Patron::FellowshipPoints))
                    .col(integer(Patron::OwnerId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("owner")
                            .from(Patron::Table, Patron::OwnerId)
                            .to(User::Table, User::Id),
                    )
                    .to_owned(),
            )
            .await?;

        // Fellowship
        manager
            .create_table(
                Table::create()
                    .table(Fellowship::Table)
                    .if_not_exists()
                    .col(pk_auto(Fellowship::Id))
                    .col(string(Fellowship::Name))
                    .col(unsigned(Fellowship::Eye))
                    .col(unsigned(Fellowship::Points))
                    .col(integer_null(Fellowship::PatronId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("patron")
                            .from(Fellowship::Table, Fellowship::PatronId)
                            .to(Patron::Table, Patron::Id),
                    )
                    .col(integer(Fellowship::OwnerId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("owner")
                            .from(Fellowship::Table, Fellowship::OwnerId)
                            .to(User::Table, User::Id),
                    )
                    .to_owned(),
            )
            .await?;

        // Fellowship characteres
        manager
            .create_table(
                Table::create()
                    .table(FellowshipCharacter::Table)
                    .if_not_exists()
                    .primary_key(
                        Index::create()
                            .col(FellowshipCharacter::CharacterId)
                            .col(FellowshipCharacter::FellowshipId),
                    )
                    .col(integer(FellowshipCharacter::CharacterId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("character")
                            .from(FellowshipCharacter::Table, FellowshipCharacter::CharacterId)
                            .to(Character::Table, Character::Id),
                    )
                    .col(integer(FellowshipCharacter::FellowshipId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fellowship")
                            .from(
                                FellowshipCharacter::Table,
                                FellowshipCharacter::FellowshipId,
                            )
                            .to(Fellowship::Table, Fellowship::Id),
                    )
                    .to_owned(),
            )
            .await?;

        // Fellowship adversaries
        manager
            .create_table(
                Table::create()
                    .table(FellowshipAdversary::Table)
                    .if_not_exists()
                    .primary_key(
                        Index::create()
                            .col(FellowshipAdversary::AdversaryId)
                            .col(FellowshipAdversary::FellowshipId),
                    )
                    .col(integer(FellowshipAdversary::AdversaryId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("adversary")
                            .from(FellowshipAdversary::Table, FellowshipAdversary::AdversaryId)
                            .to(Adversary::Table, Adversary::Id),
                    )
                    .col(integer(FellowshipAdversary::FellowshipId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fellowship")
                            .from(
                                FellowshipAdversary::Table,
                                FellowshipAdversary::FellowshipId,
                            )
                            .to(Fellowship::Table, Fellowship::Id),
                    )
                    .to_owned(),
            )
            .await?;

        // Message
        manager
            .create_table(
                Table::create()
                    .table(Message::Table)
                    .if_not_exists()
                    .col(pk_auto(Message::Id))
                    .col(text(Message::Text))
                    .col(integer(Message::FellowshipId))
                    .col(boolean(Message::ToLoremaster))
                    .col(text(Message::Player))
                    .col(date_time(Message::Date))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fellowship")
                            .from(Message::Table, Message::FellowshipId)
                            .to(Fellowship::Table, Fellowship::Id),
                    )
                    .to_owned(),
            )
            .await?;
        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_fellowship")
                    .table(Message::Table)
                    .col(Message::FellowshipId)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_index(Index::drop().name("idx_fellowship").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Message::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(FellowshipAdversary::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(FellowshipCharacter::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Fellowship::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Patron::Table).to_owned())
            .await?;
        manager
            .alter_table(
                Table::alter()
                    .table(Character::Table)
                    .drop_column(Alias::new(COLUMN_PLAYER_NAME))
                    .to_owned(),
            )
            .await?;
        manager
            .alter_table(
                Table::alter()
                    .table(Character::Table)
                    .drop_column(Alias::new(COLUMN_CHARACTER_TYPE))
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
enum Fellowship {
    Table,
    Id,
    Name,
    Points,
    PatronId,
    Eye,
    OwnerId,
}

#[derive(DeriveIden)]
enum FellowshipCharacter {
    Table,
    CharacterId,
    FellowshipId,
}

#[derive(DeriveIden)]
enum Patron {
    Table,
    Id,
    Name,
    FellowshipPoints,
    Advantage,
    OwnerId,
}

#[derive(DeriveIden)]
enum FellowshipAdversary {
    Table,
    FellowshipId,
    AdversaryId,
}

#[derive(DeriveIden)]
enum Message {
    Table,
    Id,
    FellowshipId,
    Text,
    ToLoremaster,
    Player,
    Date,
}
