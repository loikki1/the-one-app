use enums::{
    calling::CallingEnum, combat_proficiency::CombatProficiency, combat_skill::CombatSkillEnum,
    culture::CultureEnum, hate::HateEnum, skill::SkillEnum,
};
use sea_orm::strum::EnumCount;
use sea_orm_migration::{prelude::*, schema::*, sea_orm::Iterable};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // User
        manager
            .create_table(
                Table::create()
                    .table(User::Table)
                    .if_not_exists()
                    .col(pk_auto(User::Id))
                    .col(string_uniq(User::Name))
                    .col(big_unsigned(User::HashedPassword))
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_user_name")
                    .table(User::Table)
                    .col(User::Name)
                    .to_owned(),
            )
            .await?;

        // Character
        manager
            .create_table(
                Table::create()
                    .table(Character::Table)
                    .if_not_exists()
                    .col(pk_auto(Character::Id))
                    .col(string(Character::Name))
                    .col(unsigned(Character::Age))
                    .col(enumeration(
                        Character::Culture,
                        Alias::new("culture"),
                        CultureEnum::iter(),
                    ))
                    .col(enumeration(
                        Character::Calling,
                        Alias::new("calling"),
                        CallingEnum::iter(),
                    ))
                    .col(string(Character::DistinctiveFeatures))
                    .col(string(Character::Flaws))
                    .col(unsigned(Character::ItemsTreasureTotal))
                    .col(unsigned(Character::ItemsTreasureCarrying))
                    .col(binary_len(
                        Character::CharacteristicsSkills,
                        (4 * SkillEnum::COUNT).try_into().unwrap(),
                    ))
                    .col(binary_len(
                        Character::CharacteristicsCombatSkills,
                        (4 * CombatSkillEnum::COUNT).try_into().unwrap(),
                    ))
                    .col(unsigned(Character::CharacteristicsAttributesStrength))
                    .col(unsigned(Character::CharacteristicsAttributesHeart))
                    .col(unsigned(Character::CharacteristicsAttributesWits))
                    .col(unsigned(
                        Character::CharacteristicsAttributesEnduranceCurrent,
                    ))
                    .col(unsigned(Character::CharacteristicsAttributesEnduranceMax))
                    .col(unsigned(Character::CharacteristicsAttributesHopeCurrent))
                    .col(unsigned(Character::CharacteristicsAttributesHopeMax))
                    .col(unsigned(Character::CharacteristicsAttributesParry))
                    .col(unsigned(Character::CharacteristicsAttributesShadowCurrent))
                    .col(unsigned(Character::CharacteristicsAttributesShadowScars))
                    .col(string(Character::CharacteristicsAttributesShadowPath))
                    .col(unsigned(Character::CharacteristicsAttributesFatigue))
                    .col(unsigned(
                        Character::CharacteristicsAdventureExperienceCurrent,
                    ))
                    .col(unsigned(Character::CharacteristicsAdventureExperienceTotal))
                    .col(unsigned(Character::CharacteristicsSkillExperienceCurrent))
                    .col(unsigned(Character::CharacteristicsSkillExperienceTotal))
                    .col(unsigned(Character::CharacteristicsValor))
                    .col(unsigned(Character::CharacteristicsWisdom))
                    .col(unsigned(Character::InjuryDuration))
                    .col(text(Character::InjuryDescription))
                    .col(text(Character::Notes))
                    .col(boolean(Character::OneShotRules))
                    .col(boolean(Character::StriderMode))
                    .col(integer(Character::OwnerId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("character_owner")
                            .from(Character::Table, Character::OwnerId)
                            .to(User::Table, User::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_character_owner")
                    .table(Character::Table)
                    .col(Character::OwnerId)
                    .to_owned(),
            )
            .await?;

        // Favoured Skills
        manager
            .create_table(
                Table::create()
                    .table(FavouredSkill::Table)
                    .if_not_exists()
                    .col(pk_auto(FavouredSkill::Id))
                    .col(enumeration(
                        FavouredSkill::Skill,
                        Alias::new("skill"),
                        SkillEnum::iter(),
                    ))
                    .col(integer(FavouredSkill::CharacterId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("favoured_skills_character")
                            .from(FavouredSkill::Table, FavouredSkill::CharacterId)
                            .to(Character::Table, Character::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_favoured_skill_character")
                    .table(FavouredSkill::Table)
                    .col(FavouredSkill::CharacterId)
                    .to_owned(),
            )
            .await?;

        // Reward
        manager
            .create_table(
                Table::create()
                    .table(Reward::Table)
                    .if_not_exists()
                    .col(pk_auto(Reward::Id))
                    .col(string(Reward::Title))
                    .col(text(Reward::Description))
                    .col(integer(Reward::CharacterId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("reward_character")
                            .from(Reward::Table, Reward::CharacterId)
                            .to(Character::Table, Character::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_reward_character")
                    .table(Reward::Table)
                    .col(Reward::CharacterId)
                    .to_owned(),
            )
            .await?;

        // Virtue
        manager
            .create_table(
                Table::create()
                    .table(Virtue::Table)
                    .if_not_exists()
                    .col(pk_auto(Virtue::Id))
                    .col(string(Virtue::Title))
                    .col(text(Virtue::Description))
                    .col(text(Virtue::TextInput))
                    .col(integer(Virtue::CharacterId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("virtue_character")
                            .from(Virtue::Table, Virtue::CharacterId)
                            .to(Character::Table, Character::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_virtue_character")
                    .table(Virtue::Table)
                    .col(Virtue::CharacterId)
                    .to_owned(),
            )
            .await?;

        // Weapon
        manager
            .create_table(
                Table::create()
                    .table(Weapon::Table)
                    .if_not_exists()
                    .col(pk_auto(Weapon::Id))
                    .col(string(Weapon::Name))
                    .col(unsigned(Weapon::Damage))
                    .col(unsigned(Weapon::Injury))
                    .col(unsigned_null(Weapon::TwoHandInjury))
                    .col(unsigned(Weapon::Load))
                    .col(enumeration(
                        Weapon::CombatProficiency,
                        Alias::new("combat_proficiency"),
                        CombatProficiency::iter(),
                    ))
                    .col(boolean(Weapon::Wearing))
                    .col(text(Weapon::Notes))
                    .col(integer(Weapon::CharacterId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("weapon_character")
                            .from(Weapon::Table, Weapon::CharacterId)
                            .to(Character::Table, Character::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_weapon_character")
                    .table(Weapon::Table)
                    .col(Weapon::CharacterId)
                    .to_owned(),
            )
            .await?;

        // Armor
        manager
            .create_table(
                Table::create()
                    .table(Armor::Table)
                    .if_not_exists()
                    .col(pk_auto(Armor::Id))
                    .col(string(Armor::Name))
                    .col(unsigned(Armor::Protection))
                    .col(unsigned(Armor::Load))
                    .col(boolean(Armor::Wearing))
                    .col(text(Armor::Notes))
                    .col(integer(Armor::CharacterId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("armor_character")
                            .from(Armor::Table, Armor::CharacterId)
                            .to(Character::Table, Character::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_armor_character")
                    .table(Armor::Table)
                    .col(Armor::CharacterId)
                    .to_owned(),
            )
            .await?;

        // Shield
        manager
            .create_table(
                Table::create()
                    .table(Shield::Table)
                    .if_not_exists()
                    .col(pk_auto(Shield::Id))
                    .col(string(Shield::Name))
                    .col(unsigned(Shield::Parry))
                    .col(unsigned(Shield::Load))
                    .col(boolean(Shield::Wearing))
                    .col(text(Shield::Notes))
                    .col(integer(Shield::CharacterId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("shield_character")
                            .from(Shield::Table, Shield::CharacterId)
                            .to(Character::Table, Character::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_shield_character")
                    .table(Shield::Table)
                    .col(Shield::CharacterId)
                    .to_owned(),
            )
            .await?;

        // Equipment
        manager
            .create_table(
                Table::create()
                    .table(Equipment::Table)
                    .if_not_exists()
                    .col(pk_auto(Equipment::Id))
                    .col(string(Equipment::Name))
                    .col(unsigned(Equipment::Load))
                    .col(boolean(Equipment::Wearing))
                    .col(text(Equipment::Notes))
                    .col(integer(Equipment::CharacterId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("equipment_character")
                            .from(Equipment::Table, Equipment::CharacterId)
                            .to(Character::Table, Character::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_equipment_character")
                    .table(Equipment::Table)
                    .col(Equipment::CharacterId)
                    .to_owned(),
            )
            .await?;

        // Adversary
        manager
            .create_table(
                Table::create()
                    .table(Adversary::Table)
                    .if_not_exists()
                    .col(pk_auto(Adversary::Id))
                    .col(string(Adversary::Name))
                    .col(string(Adversary::Features))
                    .col(unsigned(Adversary::AttributeLevel))
                    .col(unsigned(Adversary::CurrentEndurance))
                    .col(unsigned(Adversary::MaxEndurance))
                    .col(unsigned(Adversary::Might))
                    .col(unsigned(Adversary::CurrentHate))
                    .col(unsigned(Adversary::MaxHate))
                    .col(enumeration(
                        Adversary::HateType,
                        Alias::new("hate_type"),
                        HateEnum::iter(),
                    ))
                    .col(unsigned(Adversary::Parry))
                    .col(unsigned(Adversary::Armour))
                    .col(integer(Adversary::OwnerId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("adversary_owner")
                            .from(Adversary::Table, Adversary::OwnerId)
                            .to(User::Table, User::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_adversary_owner")
                    .table(Adversary::Table)
                    .col(Adversary::OwnerId)
                    .to_owned(),
            )
            .await?;

        // Adversary Combat Proficiency
        manager
            .create_table(
                Table::create()
                    .table(AdversaryCombatProficiency::Table)
                    .if_not_exists()
                    .col(pk_auto(AdversaryCombatProficiency::Id))
                    .col(string(AdversaryCombatProficiency::Weapon))
                    .col(unsigned(AdversaryCombatProficiency::Rating))
                    .col(unsigned(AdversaryCombatProficiency::Damage))
                    .col(unsigned(AdversaryCombatProficiency::Injury))
                    .col(string(AdversaryCombatProficiency::SpecialDamages))
                    .col(integer(AdversaryCombatProficiency::AdversaryId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("adversary_combat_proficiency_adversary")
                            .from(
                                AdversaryCombatProficiency::Table,
                                AdversaryCombatProficiency::AdversaryId,
                            )
                            .to(Adversary::Table, Adversary::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_adversary_combat_proficiency_adversary")
                    .table(AdversaryCombatProficiency::Table)
                    .col(AdversaryCombatProficiency::AdversaryId)
                    .to_owned(),
            )
            .await?;

        // Fell Ability
        manager
            .create_table(
                Table::create()
                    .table(FellAbility::Table)
                    .if_not_exists()
                    .col(pk_auto(FellAbility::Id))
                    .col(string(FellAbility::Title))
                    .col(text(FellAbility::Description))
                    .col(integer(FellAbility::AdversaryId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fell_ability_adversary")
                            .from(FellAbility::Table, FellAbility::AdversaryId)
                            .to(Adversary::Table, Adversary::Id),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_fell_ability_adversary")
                    .table(FellAbility::Table)
                    .col(FellAbility::AdversaryId)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_index(Index::drop().name("idx_fell_ability_adversary").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(FellAbility::Table).to_owned())
            .await?;
        manager
            .drop_index(
                Index::drop()
                    .name("idx_adversary_combat_proficiency_adversary")
                    .to_owned(),
            )
            .await?;
        manager
            .drop_table(
                Table::drop()
                    .table(AdversaryCombatProficiency::Table)
                    .to_owned(),
            )
            .await?;
        manager
            .drop_index(Index::drop().name("idx_adversary_owner").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Adversary::Table).to_owned())
            .await?;
        manager
            .drop_index(Index::drop().name("idx_equipment_character").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Equipment::Table).to_owned())
            .await?;
        manager
            .drop_index(Index::drop().name("idx_shield_character").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Shield::Table).to_owned())
            .await?;
        manager
            .drop_index(Index::drop().name("idx_armor_character").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Armor::Table).to_owned())
            .await?;
        manager
            .drop_index(Index::drop().name("idx_weapon_character").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Weapon::Table).to_owned())
            .await?;
        manager
            .drop_index(Index::drop().name("idx_virtue_character").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Virtue::Table).to_owned())
            .await?;
        manager
            .drop_index(Index::drop().name("idx_reward_character").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Reward::Table).to_owned())
            .await?;
        manager
            .drop_index(
                Index::drop()
                    .name("idx_favoured_skill_character")
                    .to_owned(),
            )
            .await?;
        manager
            .drop_table(Table::drop().table(FavouredSkill::Table).to_owned())
            .await?;
        manager
            .drop_index(Index::drop().name("idx_character_owner").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Character::Table).to_owned())
            .await?;
        manager
            .drop_index(Index::drop().name("idx_user_name").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(User::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
pub enum User {
    Table,
    Id,
    Name,
    HashedPassword,
}

#[derive(DeriveIden)]
pub enum Adversary {
    Table,
    Id,
    Name,
    Features, // coma separated string
    AttributeLevel,
    CurrentEndurance,
    MaxEndurance,
    Might,
    CurrentHate,
    MaxHate,
    HateType,
    Parry,
    Armour,
    // CombatProficiencies,
    // Abilities,
    OwnerId,
}

#[derive(DeriveIden)]
enum AdversaryCombatProficiency {
    Table,
    Id,
    Weapon,
    Rating,
    Damage,
    Injury,
    SpecialDamages, // Comma separated string
    AdversaryId,
}

#[derive(DeriveIden)]
enum FellAbility {
    Table,
    Id,
    Title,
    Description,
    AdversaryId,
}

#[derive(DeriveIden)]
pub enum Character {
    Table,
    Id,
    Name,
    Age,
    Culture,
    Calling,
    DistinctiveFeatures, // Comma separated string
    Flaws,               // Comma separated string
    ItemsTreasureTotal,
    ItemsTreasureCarrying,
    CharacteristicsSkills,
    // CharacteristicsFavouredSkills,
    CharacteristicsCombatSkills,
    CharacteristicsAttributesStrength,
    CharacteristicsAttributesHeart,
    CharacteristicsAttributesWits,
    CharacteristicsAttributesEnduranceCurrent,
    CharacteristicsAttributesEnduranceMax,
    CharacteristicsAttributesHopeCurrent,
    CharacteristicsAttributesHopeMax,
    CharacteristicsAttributesParry,
    CharacteristicsAttributesShadowCurrent,
    CharacteristicsAttributesShadowScars,
    CharacteristicsAttributesShadowPath,
    CharacteristicsAttributesFatigue,
    CharacteristicsAdventureExperienceCurrent,
    CharacteristicsAdventureExperienceTotal,
    CharacteristicsSkillExperienceCurrent,
    CharacteristicsSkillExperienceTotal,
    CharacteristicsValor,
    // CharacteristicsRewards,
    CharacteristicsWisdom,
    // CharacteristicsVirtues,
    InjuryDuration,
    InjuryDescription,
    Notes,
    OneShotRules,
    StriderMode,
    OwnerId,
}

#[derive(DeriveIden)]
enum FavouredSkill {
    Table,
    Id,
    CharacterId,
    Skill,
}

#[derive(DeriveIden)]
enum Reward {
    Table,
    Id,
    Title,
    Description,
    CharacterId,
}

#[derive(DeriveIden)]
enum Virtue {
    Table,
    Id,
    Title,
    Description,
    TextInput,
    // Implemented,
    CharacterId,
}

#[derive(DeriveIden)]
enum Weapon {
    Table,
    Id,
    Name,
    Damage,
    Injury,
    TwoHandInjury,
    Load,
    CombatProficiency,
    Wearing,
    Notes,
    CharacterId,
}

#[derive(DeriveIden)]
enum Armor {
    Table,
    Id,
    Name,
    Protection,
    Load,
    Wearing,
    Notes,
    CharacterId,
}

#[derive(DeriveIden)]
enum Shield {
    Table,
    Id,
    Name,
    Parry,
    Load,
    Wearing,
    Notes,
    CharacterId,
}

#[derive(DeriveIden)]
enum Equipment {
    Table,
    Id,
    Name,
    Load,
    Wearing,
    Notes,
    CharacterId,
}
